#ifndef COLORPICKER_H
#define COLORPICKER_H

#include <QColor>
#include <QWidget>

QT_BEGIN_NAMESPACE
class QColorDialog;
class QLabel;
class QLineEdit;
class QPushButton;
class QSpinBox;
QT_END_NAMESPACE

QT_USE_NAMESPACE

class ColorPicker : public QWidget
{
    Q_OBJECT
public:
    explicit ColorPicker( QWidget* parent = 0 );
    virtual ~ColorPicker();

    QColor color() const;

    void retranslateUi();

public slots:
    void setColor( const QColor& aColor );

signals:
    void valueChanged();
    void valueEdited();

private:
    static QString colorToHexString( const QColor& aColor );

    void setColorToComponentsEditor( const QColor& aColor );
    QColor colorFromComponentsEditor() const;

    void setColorToHexEditor( const QColor& aColor );
    QColor colorFromHexEditor() const;

    void setColorToPicker( const QColor& aColor );
    QColor colorFromPicker() const;

    void colorToUi( const QColor& aColor );

    void updateColorDisplay();

private slots:
    void pickColor();
    void onColorUpdatedFromComponentsEditor();
    void onColorUpdatedFromHexEditor();
    void onColorUpdatedFromPicker();

private:
    enum ColorComponentIndex
    {
        ColorComponentIndexInvalid = -1,
        ColorComponentIndexRed     = 0,
        ColorComponentIndexGreen   = 1,
        ColorComponentIndexBlue    = 2,
        ColorComponentsCount       = 3
    };

private:
    QColor        m_color;
    QLineEdit*    m_colorHexEditor;

    QLabel*       m_colorComponentsLabels[ ColorComponentsCount ];
    QSpinBox*     m_colorComponentsEditors[ ColorComponentsCount ];

    QLabel*       m_colorDisplay;
    QPushButton*  m_openColorEditorButton;
    QColorDialog* m_colorPickerDialog;
};

#endif // COLORPICKER_H
