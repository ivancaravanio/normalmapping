#ifndef OGL_H
#define OGL_H

#ifdef USE_GLCOREARB
    #include <GL/glcorearb.h>
#endif
#include <GL/glew.h>
#include <GL/GL.h>

#endif // OGL_H
