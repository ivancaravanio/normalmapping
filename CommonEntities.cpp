#include "CommonEntities.h"

#include "Utilities/ImageUtilities.h"

#include <glm/glm.hpp>

#include <QApplication>
#include <QDir>
#include <QStringBuilder>
#include <QtGlobal>

#include <limits>

using namespace Utilities;

const GLint CommonEntities::INVALID_UNIFORM_VAR_LOCATION = -1;
const GraphicsData CommonEntities::DEFAULT_GRAPHICS_DATA(
        GraphicsData::EffectTypePhongIlluminationAveragedSurfaceNormals,
        GraphicsData::textureDefaultFilePath( GraphicsData::PartTypeIndexFloor ),
        GraphicsData::textureDefaultFilePath( GraphicsData::PartTypeIndexWalls ),
        GraphicsData::textureDefaultFilePath( GraphicsData::PartTypeIndexRoofSides ),
        GraphicsData::DEFAULT_BUMP_SIZE,
        ImageUtilities::intFromRealColor( glm::vec3( 0.2f, 0.2f, 0.2f ) ),
        ImageUtilities::intFromRealColor( glm::vec3( 1.0f, 1.0f, 1.0f ) ),
        glm::vec3( -1.0f, 1.0f, 1.0f ),
        glm::vec3( 0.0f, 0.0f, 1.0f ),
        GraphicsData::MAX_MATERIAL_SHININESS_LEVEL,
        5.0 );

bool CommonEntities::isTextureIndexValid( const CommonEntities::TextureIndex textureIndex )
{
    return textureIndex == TextureIndexWallsColor
           || textureIndex == TextureIndexWallsNormalMap
           || textureIndex == TextureIndexFloorColor
           || textureIndex == TextureIndexFloorNormalMap
           || textureIndex == TextureIndexRoofSidesColor
           || textureIndex == TextureIndexRoofSidesNormalMap;
}

bool CommonEntities::isTextureUnitIndexValid( const CommonEntities::TextureUnitIndex textureUnitIndex )
{
    return textureUnitIndex == TextureUnitIndexColor
           || textureUnitIndex == TextureUnitIndexNormalMap;
}

QString CommonEntities::defaultTextureFileNameFromTextureIndex( const CommonEntities::TextureIndex textureIndex )
{
#ifdef USE_SINGLE_UNWRAPPED_TEXTURE
    Q_UNUSED( textureIndex )

    return "HouseDiffuseMap.png";
#else
    static const QString textureFileNames[ TextureIndicesCount ] =
    {
        "brick_wall.jpg",
        "brick_wall_nm.png",
        "stone_wall_mosaic.jpg",
        "stone_wall_mosaic_nm.png",
        "roof.jpg",
        "roof_nm.png"
    };

    static const int textureFileNamesCount = sizeof( textureFileNames ) / sizeof( QString );

    return 0 <= textureIndex && textureIndex < textureFileNamesCount
           ? textureFileNames[ textureIndex ]
           : QString();
#endif
}

QString CommonEntities::defaultTextureFilePathFromTextureIndex( const CommonEntities::TextureIndex textureIndex )
{
    QApplication* const app = qApp;
    return QDir::toNativeSeparators( app == 0
                                     ? QDir::currentPath()
                                     : app->applicationDirPath() ) % QDir::separator()
           % "textures" % QDir::separator()
           % CommonEntities::defaultTextureFileNameFromTextureIndex( textureIndex );
}

bool CommonEntities::isUniformVarLocationValid( const GLint uniformVarLocation )
{
    return uniformVarLocation >= 0;
}

CommonEntities::TextureUnitIndex CommonEntities::textureUnitIndexFromTextureIndex( const CommonEntities::TextureIndex textureIndex )
{
    switch ( textureIndex )
    {
        case TextureIndexWallsColor: // don't break
        case TextureIndexFloorColor: // don't break
        case TextureIndexRoofSidesColor:
            return TextureUnitIndexColor;

        case TextureIndexWallsNormalMap: // don't break
        case TextureIndexFloorNormalMap: // don't break
        case TextureIndexRoofSidesNormalMap:
            return TextureUnitIndexNormalMap;

        default:
            break;
    }

    return TextureUnitIndexInvalid;
}

GLenum CommonEntities::textureUnitDescriptorFromTextureUnitIndex( const CommonEntities::TextureUnitIndex textureUnitIndex )
{
    switch ( textureUnitIndex )
    {
        case TextureUnitIndexColor:
            return GL_TEXTURE0;
        case TextureUnitIndexNormalMap:
            return GL_TEXTURE1;
        default:
            break;
    }

    return GL_TEXTURE0;
}

QString CommonEntities::textureUnitSamplerNameFromTextureUnitIndex( const CommonEntities::TextureUnitIndex textureUnitIndex )
{
    switch ( textureUnitIndex )
    {
        case TextureUnitIndexColor:
            return "colorTextureSampler";
        case TextureUnitIndexNormalMap:
            return "normalMapTextureSampler";
        default:
            break;
    }

    return QString();
}

QList< CommonEntities::TextureIndex > CommonEntities::textureIndicesFromTextureUnitIndex( const CommonEntities::TextureUnitIndex textureUnitIndex )
{
    QList< CommonEntities::TextureIndex > textureIndices;
    switch ( textureUnitIndex )
    {
        case CommonEntities::TextureUnitIndexColor:
            textureIndices << TextureIndexWallsColor
                           << TextureIndexFloorColor
                           << TextureIndexRoofSidesColor;
            break;
        case CommonEntities::TextureUnitIndexNormalMap:
            textureIndices << TextureIndexWallsNormalMap
                           << TextureIndexFloorNormalMap
                           << TextureIndexRoofSidesNormalMap;
            break;
        default:
            break;
    }

    return textureIndices;
}

QList< CommonEntities::TextureIndex > CommonEntities::textureIndicesFromVaoIndex( const CommonEntities::VAOIndex vaoIndex )
{
    QList< CommonEntities::TextureIndex > textureIndices;
    switch ( vaoIndex )
    {
        case CommonEntities::VAOIndexFloor:
            textureIndices << TextureIndexFloorColor
                           << TextureIndexFloorNormalMap;
            break;
        case CommonEntities::VAOIndexWalls:
            textureIndices << TextureIndexWallsColor
                           << TextureIndexWallsNormalMap;
            break;
        case CommonEntities::VAOIndexRoofSides:
            textureIndices << TextureIndexRoofSidesColor
                           << TextureIndexRoofSidesNormalMap;
            break;
        default:
            break;
    }

    return textureIndices;
}

QString CommonEntities::vecToString( const glm::vec2& v )
{
    return QString( "%1( %2, %3 )" )
           .arg( QT_STRINGIFY2( glm::vec2 ) )
           .arg( v.x, 0, 'f', 3 )
           .arg( v.y, 0, 'f', 3 );
}

QString CommonEntities::vecToString( const glm::vec3& v )
{
    return QString( "%1( %2, %3, %4 )" )
           .arg( QT_STRINGIFY2( glm::vec3 ) )
           .arg( v.x, 0, 'f', 3 )
           .arg( v.y, 0, 'f', 3 )
           .arg( v.z, 0, 'f', 3 );
}

QString CommonEntities::vecToString( const glm::vec4& v )
{
    return QString( "%1( %2, %3, %4, %5 )" )
            .arg( QT_STRINGIFY2( glm::vec4 ) )
            .arg( v.x, 0, 'f', 3 )
            .arg( v.y, 0, 'f', 3 )
            .arg( v.z, 0, 'f', 3 )
            .arg( v.w, 0, 'f', 3 );
}

#ifndef QT_NO_DEBUG_STREAM
QDebug operator<<( QDebug qdebug, const glm::vec2& v )
{
    return qdebug << CommonEntities::vecToString( v );
}

QDebug operator<<( QDebug qdebug, const glm::vec3& v )
{
    return qdebug << CommonEntities::vecToString( v );
}

QDebug operator<<( QDebug qdebug, const glm::vec4& v )
{
    return qdebug << CommonEntities::vecToString( v );
}
#endif
