#include "GLWidget.h"

#include "CommonEntities.h"
#include "GLTools.h"
#include "Mesh3.h"
#include "Utilities/ImageUtilities.h"
#include "Utilities/MathUtilities.h"

#ifdef USE_GLM
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#endif

#include <QApplication>
#include <QByteArray>
#include <QDebug>
#include <QDir>
#include <QFile>
#include <QKeyEvent>
#include <qmath.h>
#include <QStandardPaths>
#include <QStringBuilder>

#define LOGGING_ENABLED
#ifdef LOGGING_ENABLED
    #define LOG_GL_ERROR() GLWidget::logGlError( __FILE__, __LINE__ )
#else
    #define LOG_GL_ERROR()
#endif

#define AXES_COUNT_2D 2
#define AXES_COUNT_3D 3
#define AXES_COUNT_4D 4
#define TRIANGLE_VERTICES_COUNT 3
#define SQUARE_VERTICES_COUNT 4
#define RGB_COLOR_COMPONENTS_COUNT 3

const glm::vec3 GLWidget::s_xAxisDir( 1.0f, 0.0f, 0.0f );
const glm::vec3 GLWidget::s_yAxisDir( 0.0f, 1.0f, 0.0f );

const GLuint GLWidget::s_invalidShaderProgramHandle = 0U;
const QList< GLWidget::GenericVertexAttributeIndex > GLWidget::ALL_ORDERED_GENERIC_VERTEX_ATTRIBUTE_INDICES =
        QList< GLWidget::GenericVertexAttributeIndex >()
        << GLWidget::GenericVertexAttributeIndexVertexPos
        << GLWidget::GenericVertexAttributeIndexVertexNormal
        << GLWidget::GenericVertexAttributeIndexVertexTangent
        << GLWidget::GenericVertexAttributeIndexVertexTexCoord;

using namespace Utilities;

GLWidget::GLWidget( QWidget* parent )
    : QGLWidget( parent ),
      m_shaderProgramHandle( s_invalidShaderProgramHandle ),
      m_isOpenGLVersion330CoreProfileUsed( false ),
      m_graphicsData( CommonEntities::DEFAULT_GRAPHICS_DATA )
{
    this->init();
}

GLWidget::GLWidget( const QGLFormat& format, QWidget* parent )
    : QGLWidget( format, parent ),
      m_shaderProgramHandle( s_invalidShaderProgramHandle ),
      m_isOpenGLVersion330CoreProfileUsed( false ),
      m_graphicsData( CommonEntities::DEFAULT_GRAPHICS_DATA )
{
    this->init();
}

GLWidget::~GLWidget()
{
    this->clearShaderProgram();

    glDeleteBuffers( sizeof( m_vbos ) / sizeof( GLuint ), m_vbos );
    LOG_GL_ERROR();

    glDeleteVertexArrays( sizeof( m_vaos ) / sizeof( GLuint ), m_vaos );
    LOG_GL_ERROR();

    const int textureUnitsCount = m_textureUnits.size();
    for ( int textureUnitIndex = 0;
          textureUnitIndex < textureUnitsCount;
          ++ textureUnitIndex )
    {
        const QList< Texture >& textures = m_textureUnits[ textureUnitIndex ].textures;
        const int texturesCount = textures.size();
        for ( int textureIndex = 0;
              textureIndex < texturesCount;
              ++ textureIndex )
        {
            const GLuint textureName = textures[ textureIndex ].name;
            if ( textureName != 0 )
            {
                glDeleteTextures( 1, & textureName );
                LOG_GL_ERROR();
            }
        }
    }
}

void GLWidget::setGraphicsData( const GraphicsData& aGraphicsData )
{
    this->applyGraphicsData( aGraphicsData, false );
    m_graphicsData = aGraphicsData;
}

const GraphicsData& GLWidget::graphicsData() const
{
    return m_graphicsData;
}

void GLWidget::setEffectType( const GraphicsData::EffectType aEffectType )
{
    if ( ! GraphicsData::isEffectTypeValid( aEffectType )
         || aEffectType == m_graphicsData.effectType )
    {
        return;
    }

    this->applyEffectType( aEffectType, false, true );
    m_graphicsData.effectType = aEffectType;
}

void GLWidget::setTextureFilePath(
        const GraphicsData::PartTypeIndex partTypeIndex,
        const QString& textureFilePath )
{
    if ( ! GraphicsData::isPartTypeIndexValid( partTypeIndex ) || textureFilePath.isEmpty() )
    {
        return;
    }

    const QFileInfo textureFileInfo( textureFilePath );
    if ( ! textureFileInfo.exists() )
    {
        return;
    }

    QImage colorTextureImage( textureFilePath );
    if ( colorTextureImage.isNull() )
    {
        return;
    }

    const int textureImageWidth  = colorTextureImage.width();
    const int textureImageHeight = colorTextureImage.height();
    const int minSize = pow( 2, qFloor( log2( qMin( textureImageWidth, textureImageHeight ) ) ) );
    if ( minSize <= 0 )
    {
        return;
    }

    if ( minSize != textureImageWidth
         || minSize != textureImageHeight )
    {
        colorTextureImage = colorTextureImage.copy( 0, 0, minSize, minSize );
    }

    const QString normalMapsDirPath = QStandardPaths::writableLocation( QStandardPaths::DesktopLocation ) % QDir::separator()
                                      % qAppName() % "_normalMaps";
    const QDir normalMapsDir( normalMapsDirPath );
    if ( ! normalMapsDir.exists() )
    {
        if ( ! normalMapsDir.mkpath( normalMapsDirPath ) )
        {
            return;
        }
    }

    const QImage normalMapTextureImage = ImageUtilities::toEnhancedNormalMap( colorTextureImage, m_graphicsData.bumpSize );

// #define SAVE_NORMAL_MAPS
#ifdef SAVE_NORMAL_MAPS
    if ( ! normalMapTextureImage.isNull() )
    {
        const QString normalMapTextureFileName = textureFileInfo.completeBaseName() % "_nm." % textureFileInfo.suffix();
        const QString normalMapTextureFilePath = normalMapsDir.filePath( normalMapTextureFileName );
        if ( ! normalMapTextureImage.save( normalMapTextureFilePath ) )
        {
            return;
        }
    }
#endif

    m_graphicsData.textureFilePaths[ partTypeIndex ] = textureFilePath;

    CommonEntities::TextureIndex colorTextureIndex     = CommonEntities::TextureIndexInvalid;
    CommonEntities::TextureIndex normalMapTextureIndex = CommonEntities::TextureIndexInvalid;
    switch ( partTypeIndex )
    {
        case GraphicsData::PartTypeIndexFloor:
            colorTextureIndex     = CommonEntities::TextureIndexFloorColor;
            normalMapTextureIndex = CommonEntities::TextureIndexFloorNormalMap;
            break;
        case GraphicsData::PartTypeIndexWalls:
            colorTextureIndex     = CommonEntities::TextureIndexWallsColor;
            normalMapTextureIndex = CommonEntities::TextureIndexWallsNormalMap;
            break;
        case GraphicsData::PartTypeIndexRoofSides:
            colorTextureIndex     = CommonEntities::TextureIndexRoofSidesColor;
            normalMapTextureIndex = CommonEntities::TextureIndexRoofSidesNormalMap;
            break;
        default:
            return;
    }

    this->setTexture( colorTextureIndex, colorTextureImage );
    this->setTexture( normalMapTextureIndex, normalMapTextureImage );
}

void GLWidget::setBumpSize( const qreal aBumpSize )
{
    if ( qFuzzyCompare( aBumpSize, m_graphicsData.bumpSize ) )
    {
        return;
    }

    m_graphicsData.bumpSize = aBumpSize;
    if ( this->effectType() != GraphicsData::EffectTypePhongIlluminationNormalMapping )
    {
        return;
    }

    for ( int i = 0; i < GraphicsData::PartTypeIndicesCount; ++ i )
    {
        this->setTextureFilePath( static_cast< GraphicsData::PartTypeIndex >( i ), m_graphicsData.textureFilePaths[ i ] );
    }
}

GraphicsData::EffectType GLWidget::effectType() const
{
    return m_graphicsData.effectType;
}

QString GLWidget::textureFilePath( const GraphicsData::PartTypeIndex partTypeIndex ) const
{
    return GraphicsData::isPartTypeIndexValid( partTypeIndex )
           ? m_graphicsData.textureFilePaths[ partTypeIndex ]
           : QString();
}

qreal GLWidget::bumpSize() const
{
    return m_graphicsData.bumpSize;
}

void GLWidget::initializeGL()
{
    // problem with nVidia chips:
    // http://stackoverflow.com/questions/8302625/segmentation-fault-at-glgenvertexarrays-1-vao
    glewExperimental = GL_TRUE;

    GLenum err = glewInit();
    LOG_GL_ERROR();
    if ( GLEW_OK != err )
    {
        qDebug().nospace() << "Error initializing GLEW: " << glewGetErrorString( err );
        return;
    }

    // this->printGLInfo();

    ShaderInfo vertexShader;
    vertexShader.type = ShaderTypeVertex;
    vertexShader.filePath = ":/VertexShader.vert";

    foreach ( const GenericVertexAttributeIndex genericVertexAttributeIndex, ALL_ORDERED_GENERIC_VERTEX_ATTRIBUTE_INDICES )
    {
        vertexShader.variables.insert(
                    genericVertexAttributeIndex,
                    GLWidget::genericVertexAttributeName( genericVertexAttributeIndex ) );
    }

    ShaderInfo fragmentShader;
    fragmentShader.type = ShaderTypeFragment;
    fragmentShader.filePath = ":/FragmentShader.frag";
    fragmentShader.variables.insert( FragmentOutputVariableIndexColor, "fColor" );

    if ( ! this->loadShaderPairWithAttributes( vertexShader, fragmentShader ) )
    {
        return;
    }

    const GLuint activeShaderProgramHandle = GLWidget::activeShaderProgramHandle();
    const bool changeActiveShaderProgramHandle = activeShaderProgramHandle != m_shaderProgramHandle;
    if ( changeActiveShaderProgramHandle )
    {
        glUseProgram( m_shaderProgramHandle );
        LOG_GL_ERROR();
    }

    for ( int i = 0; i < UniformVarsCount; ++ i )
    {
        QPair< GLint, QString >& uniformVar = m_uniformVars[ i ];
        GLint& uniformVarLocation = uniformVar.first;

        uniformVarLocation = glGetUniformLocation( m_shaderProgramHandle, uniformVar.second.toLatin1().constData() );
        LOG_GL_ERROR();
    }

    this->applyGraphicsData( m_graphicsData, true, false );
    this->applyMatrices( false );

    glEnable( GL_LINE_SMOOTH );
    LOG_GL_ERROR();

    glEnable( GL_POLYGON_SMOOTH );
    LOG_GL_ERROR();

    glHint( GL_LINE_SMOOTH_HINT, GL_NICEST );
    LOG_GL_ERROR();

    glHint( GL_POLYGON_SMOOTH_HINT, GL_NICEST );
    LOG_GL_ERROR();

    glEnable( GL_DEPTH_TEST );
    LOG_GL_ERROR();

    glEnable( GL_CULL_FACE );
    LOG_GL_ERROR();

    glClearColor( 1.0f, 1.0f, 1.0f, 0.0f );
    LOG_GL_ERROR();

    m_textureUnits.clear();
    m_textureUnits.reserve( CommonEntities::TextureUnitsCount );
    for ( int textureUnitIndex = CommonEntities::TextureUnitIndexFirst;
          textureUnitIndex < CommonEntities::TextureUnitsCount;
          ++ textureUnitIndex )
    {
        const CommonEntities::TextureUnitIndex textureUnitIndexIntrinsic = static_cast< CommonEntities::TextureUnitIndex > ( textureUnitIndex );

        TextureUnit textureUnit;
        textureUnit.index = textureUnitIndexIntrinsic;

        glActiveTexture( CommonEntities::textureUnitDescriptorFromTextureUnitIndex( textureUnitIndexIntrinsic ) );
        LOG_GL_ERROR();

        const QString samplerUniformVarName = CommonEntities::textureUnitSamplerNameFromTextureUnitIndex( textureUnitIndexIntrinsic );
        textureUnit.samplerLocation = glGetUniformLocation( m_shaderProgramHandle,
                                                            samplerUniformVarName.toLatin1().constData() );
        LOG_GL_ERROR();

        if ( CommonEntities::isUniformVarLocationValid( textureUnit.samplerLocation ) )
        {
            glUniform1i( textureUnit.samplerLocation, static_cast< GLint > ( textureUnitIndexIntrinsic ) );
            LOG_GL_ERROR();
        }
        else
        {
            qDebug().nospace() << "sampler " << samplerUniformVarName << " not found";
        }

        const QList< CommonEntities::TextureIndex > textureIndices = CommonEntities::textureIndicesFromTextureUnitIndex( textureUnitIndexIntrinsic );
        const int textureIndicesCount = textureIndices.size();

        QList< Texture >& textures = textureUnit.textures;
        textures.reserve( textureIndicesCount );
        for ( int i = 0; i < textureIndicesCount; ++ i )
        {
            const CommonEntities::TextureIndex textureIndex = textureIndices[ i ];

            Texture texture;
            texture.index = textureIndex;

            glGenTextures( 1, & ( texture.name ) );
            LOG_GL_ERROR();

            textures.append( texture );
        }

        m_textureUnits.append( textureUnit );
    }

    for ( int i = 0; i < CommonEntities::TextureIndicesCount; ++ i )
    {
        const CommonEntities::TextureIndex textureIndex = static_cast< CommonEntities::TextureIndex >( i );
        this->setTexture( textureIndex, QImage( CommonEntities::defaultTextureFilePathFromTextureIndex( textureIndex ) ) );
    }

    if ( m_isOpenGLVersion330CoreProfileUsed )
    {
        static const GLfloat actualZDirection = 1.0f;

        enum VertexIndex
        {
            VertexIndexFloorFrontLeft    = 0,
            VertexIndexFloorBackLeft     = 1,
            VertexIndexFloorBackRight    = 2,
            VertexIndexFloorFrontRight   = 3,

            VertexIndexCeilingFrontLeft  = 4,
            VertexIndexCeilingFrontRight = 5,
            VertexIndexCeilingBackRight  = 6,
            VertexIndexCeilingBackLeft   = 7,

            VertexIndexRoofPeak          = 8,
            VerticesCount                = 9
        };

        QList< glm::vec3 > allUniqueVertices;
        allUniqueVertices.reserve( VerticesCount );
        allUniqueVertices
                // floor
                << glm::vec3( -0.5f, -0.5f, 0.5f * actualZDirection )
                << glm::vec3( -0.5f, -0.5f, -0.5f * actualZDirection )
                << glm::vec3( 0.5f, -0.5f, -0.5f * actualZDirection )
                << glm::vec3( 0.5f, -0.5f, 0.5f * actualZDirection )

                // ceiling
                << glm::vec3( -0.5f, 0.5f, 0.5f * actualZDirection )
                << glm::vec3( 0.5f, 0.5f, 0.5f * actualZDirection )
                << glm::vec3( 0.5f, 0.5f, -0.5f * actualZDirection )
                << glm::vec3( -0.5f, 0.5f, -0.5f * actualZDirection )

                // roof peak
                << glm::vec3( 0.0f, 0.75f, 0.0f * actualZDirection );

        QList< QPair< Triangle3, Triangle2 > > floorTriangles;
        floorTriangles
                << QPair< Triangle3, Triangle2 >( Triangle3( allUniqueVertices[ VertexIndexFloorBackLeft ],
                                                             allUniqueVertices[ VertexIndexFloorBackRight ],
                                                             allUniqueVertices[ VertexIndexFloorFrontRight ] ),
                                          #ifdef USE_SINGLE_UNWRAPPED_TEXTURE
                                                  Triangle2( glm::vec2( 2.0f / 3.0f, 2.0f / 3.0f ),
                                                             glm::vec2( 1.0f, 2.0f / 3.0f ),
                                                             glm::vec2( 1.0f, 1.0f ) )
                                          #else
                                                  Triangle2( glm::vec2( 0.0f, 0.0f ),
                                                             glm::vec2( 1.0f, 0.0f ),
                                                             glm::vec2( 1.0f, 1.0f ) )
                                          #endif
                                                  )
                << QPair< Triangle3, Triangle2 >( Triangle3( allUniqueVertices[ VertexIndexFloorFrontRight ],
                                                             allUniqueVertices[ VertexIndexFloorFrontLeft ],
                                                             allUniqueVertices[ VertexIndexFloorBackLeft ] ),
#ifdef USE_SINGLE_UNWRAPPED_TEXTURE
                                                  Triangle2( glm::vec2( 1.0f, 1.0f ),
                                                             glm::vec2( 2.0f / 3.0f, 1.0f ),
                                                             glm::vec2( 2.0f / 3.0f, 2.0f / 3.0f ) )
#else
                                                  Triangle2( glm::vec2( 1.0f, 1.0f ),
                                                             glm::vec2( 0.0f, 1.0f ),
                                                             glm::vec2( 0.0f, 0.0f ) )
#endif
                                                  );

        QList< QPair< Triangle3, Triangle2 > > wallsTriangles;
        wallsTriangles
                // FRONT
                << QPair< Triangle3, Triangle2 >( Triangle3( allUniqueVertices[ VertexIndexFloorFrontLeft ],
                                                             allUniqueVertices[ VertexIndexFloorFrontRight ],
                                                             allUniqueVertices[ VertexIndexCeilingFrontRight ] ),
#ifdef USE_SINGLE_UNWRAPPED_TEXTURE
                                                  Triangle2( glm::vec2( 0.0f,        0.0f ),
                                                             glm::vec2( 1.0f / 3.0f, 0.0f ),
                                                             glm::vec2( 1.0f / 3.0f, 1.0f / 3.0f ) )
#else
                                                  Triangle2( glm::vec2( 0.0f, 0.0f ),
                                                             glm::vec2( 1.0f, 0.0f ),
                                                             glm::vec2( 1.0f, 1.0f ) )
#endif
                                                  )
                << QPair< Triangle3, Triangle2 >( Triangle3( allUniqueVertices[ VertexIndexCeilingFrontRight ],
                                                             allUniqueVertices[ VertexIndexCeilingFrontLeft ],
                                                             allUniqueVertices[ VertexIndexFloorFrontLeft ] ),
#ifdef USE_SINGLE_UNWRAPPED_TEXTURE
                                                  Triangle2( glm::vec2( 1.0f / 3.0f, 1.0f / 3.0f ),
                                                             glm::vec2( 0.0f,        1.0f / 3.0f ),
                                                             glm::vec2( 0.0f,        0.0f ) )

#else
                                                  Triangle2( glm::vec2( 1.0f, 1.0f ),
                                                             glm::vec2( 0.0f, 1.0f ),
                                                             glm::vec2( 0.0f, 0.0f ) )
#endif
                                                  )

                // RIGHT
                << QPair< Triangle3, Triangle2 >( Triangle3( allUniqueVertices[ VertexIndexFloorFrontRight ],
                                                             allUniqueVertices[ VertexIndexFloorBackRight ],
                                                             allUniqueVertices[ VertexIndexCeilingBackRight ] ),
#ifdef USE_SINGLE_UNWRAPPED_TEXTURE
                                                  Triangle2( glm::vec2( 1.0f / 3.0f, 0.0f ),
                                                             glm::vec2( 2.0f / 3.0f, 0.0f ),
                                                             glm::vec2( 2.0f / 3.0f, 1.0f / 3.0f ) )
#else
                                                  Triangle2( glm::vec2( 0.0f, 0.0f ),
                                                             glm::vec2( 1.0f, 0.0f ),
                                                             glm::vec2( 1.0f, 1.0f ) )
#endif
                                                  )
                << QPair< Triangle3, Triangle2 >( Triangle3( allUniqueVertices[ VertexIndexCeilingBackRight ],
                                                             allUniqueVertices[ VertexIndexCeilingFrontRight ],
                                                             allUniqueVertices[ VertexIndexFloorFrontRight ] ),
#ifdef USE_SINGLE_UNWRAPPED_TEXTURE
                                                  Triangle2( glm::vec2( 2.0f / 3.0f, 1.0f / 3.0f ),
                                                             glm::vec2( 1.0f / 3.0f, 1.0f / 3.0f ),
                                                             glm::vec2( 1.0f / 3.0f, 0.0f        ) )
#else
                                                  Triangle2( glm::vec2( 1.0f, 1.0f ),
                                                             glm::vec2( 0.0f, 1.0f ),
                                                             glm::vec2( 0.0f, 0.0f ) )
#endif
                                                  )

                // BACK
                << QPair< Triangle3, Triangle2 >( Triangle3( allUniqueVertices[ VertexIndexFloorBackRight ],
                                                             allUniqueVertices[ VertexIndexFloorBackLeft ],
                                                             allUniqueVertices[ VertexIndexCeilingBackLeft ] ),
#ifdef USE_SINGLE_UNWRAPPED_TEXTURE
                                                  Triangle2( glm::vec2( 0.0f,        1.0f / 3.0f + 1.0f / 6.0f ),
                                                             glm::vec2( 1.0f / 3.0f, 1.0f / 3.0f + 1.0f / 6.0f ),
                                                             glm::vec2( 1.0f / 3.0f, 2.0f / 3.0f + 1.0f / 6.0f ) )
#else
                                                  Triangle2( glm::vec2( 0.0f, 0.0f ),
                                                             glm::vec2( 1.0f, 0.0f ),
                                                             glm::vec2( 1.0f, 1.0f ) )
#endif
                                                  )
                << QPair< Triangle3, Triangle2 >( Triangle3( allUniqueVertices[ VertexIndexCeilingBackLeft ],
                                                             allUniqueVertices[ VertexIndexCeilingBackRight ],
                                                             allUniqueVertices[ VertexIndexFloorBackRight ] ),
                                          #ifdef USE_SINGLE_UNWRAPPED_TEXTURE
                                                  Triangle2( glm::vec2( 1.0f / 3.0f, 2.0f / 3.0f + 1.0f / 6.0f ),
                                                             glm::vec2( 0.0f,        2.0f / 3.0f + 1.0f / 6.0f ),
                                                             glm::vec2( 0.0f,        1.0f / 3.0f + 1.0f / 6.0f ) )
                                          #else
                                                  Triangle2( glm::vec2( 1.0f, 1.0f ),
                                                             glm::vec2( 0.0f, 1.0f ),
                                                             glm::vec2( 0.0f, 0.0f ) )
                                          #endif
                                                  )

                // LEFT
                << QPair< Triangle3, Triangle2 >( Triangle3( allUniqueVertices[ VertexIndexFloorBackLeft ],
                                                             allUniqueVertices[ VertexIndexFloorFrontLeft ],
                                                             allUniqueVertices[ VertexIndexCeilingFrontLeft ] ),
                                          #ifdef USE_SINGLE_UNWRAPPED_TEXTURE
                                                  Triangle2( glm::vec2( 1.0f / 3.0f, 1.0f / 3.0f + 1.0f / 6.0f ),
                                                             glm::vec2( 2.0f / 3.0f, 1.0f / 3.0f + 1.0f / 6.0f ),
                                                             glm::vec2( 2.0f / 3.0f, 2.0f / 3.0f + 1.0f / 6.0f ) )
                                          #else
                                                  Triangle2( glm::vec2( 0.0f, 0.0f ),
                                                             glm::vec2( 1.0f, 0.0f ),
                                                             glm::vec2( 1.0f, 1.0f ) )
                                          #endif
                                                  )
                << QPair< Triangle3, Triangle2 >( Triangle3( allUniqueVertices[ VertexIndexCeilingFrontLeft ],
                                                             allUniqueVertices[ VertexIndexCeilingBackLeft ],
                                                             allUniqueVertices[ VertexIndexFloorBackLeft ] ),
                                          #ifdef USE_SINGLE_UNWRAPPED_TEXTURE
                                                  Triangle2( glm::vec2( 2.0f / 3.0f, 2.0f / 3.0f + 1.0f / 6.0f ),
                                                             glm::vec2( 1.0f / 3.0f, 2.0f / 3.0f + 1.0f / 6.0f ),
                                                             glm::vec2( 1.0f / 3.0f, 1.0f / 3.0f + 1.0f / 6.0f ) )
                                          #else
                                                  Triangle2( glm::vec2( 1.0f, 1.0f ),
                                                             glm::vec2( 0.0f, 1.0f ),
                                                             glm::vec2( 0.0f, 0.0f ) )
                                          #endif
                                                  );

        QList< QPair< Triangle3, Triangle2 > > roofTriangles;
        roofTriangles
                // FRONT
                << QPair< Triangle3, Triangle2 >( Triangle3( allUniqueVertices[ VertexIndexCeilingFrontLeft ],
                                                             allUniqueVertices[ VertexIndexCeilingFrontRight ],
                                                             allUniqueVertices[ VertexIndexRoofPeak ] ),
                                          #ifdef USE_SINGLE_UNWRAPPED_TEXTURE
                                                  Triangle2( glm::vec2( 0.0f,        1.0f / 3.0f               ),
                                                             glm::vec2( 1.0f / 3.0f, 1.0f / 3.0f               ),
                                                             glm::vec2( 1.0f / 6.0f, 1.0f / 3.0f + 1.0f / 6.0f ) )
                                          #else
                                                  Triangle2( glm::vec2( 0.0f, 0.0f ),
                                                             glm::vec2( 1.0f, 0.0f ),
                                                             glm::vec2( 0.5f, 1.0f ) )
                                          #endif
                                                  )

                // RIGHT
                << QPair< Triangle3, Triangle2 >( Triangle3( allUniqueVertices[ VertexIndexCeilingFrontRight ],
                                                             allUniqueVertices[ VertexIndexCeilingBackRight ],
                                                             allUniqueVertices[ VertexIndexRoofPeak ] ),
                                          #ifdef USE_SINGLE_UNWRAPPED_TEXTURE
                                                  Triangle2( glm::vec2( 1.0f / 3.0f, 1.0f / 3.0f                             ),
                                                             glm::vec2( 2.0f / 3.0f, 1.0f / 3.0f                             ),
                                                             glm::vec2( 1.0f / 3.0f + 1.0f / 6.0f, 1.0f / 3.0f + 1.0f / 6.0f ) )
                                          #else
                                                  Triangle2( glm::vec2( 0.0f, 0.0f ),
                                                             glm::vec2( 1.0f, 0.0f ),
                                                             glm::vec2( 0.5f, 1.0f ) )
                                          #endif
                                                  )

                // BACK
                << QPair< Triangle3, Triangle2 >( Triangle3( allUniqueVertices[ VertexIndexCeilingBackRight ],
                                                             allUniqueVertices[ VertexIndexCeilingBackLeft ],
                                                             allUniqueVertices[ VertexIndexRoofPeak ] ),
                                          #ifdef USE_SINGLE_UNWRAPPED_TEXTURE
                                                  Triangle2( glm::vec2( 0.0f,        2.0f / 3.0f               + 1.0f / 6.0f ),
                                                             glm::vec2( 1.0f / 3.0f, 2.0f / 3.0f               + 1.0f / 6.0f ),
                                                             glm::vec2( 1.0f / 6.0f, 2.0f / 3.0f + 1.0f / 6.0f + 1.0f / 6.0f ) )
                                          #else
                                                  Triangle2( glm::vec2( 0.0f, 0.0f ),
                                                             glm::vec2( 1.0f, 0.0f ),
                                                             glm::vec2( 0.5f, 1.0f ) )
                                          #endif
                                                  )

                // LEFT
                << QPair< Triangle3, Triangle2 >( Triangle3( allUniqueVertices[ VertexIndexCeilingBackLeft ],
                                                             allUniqueVertices[ VertexIndexCeilingFrontLeft ],
                                                             allUniqueVertices[ VertexIndexRoofPeak ] ),
                                          #ifdef USE_SINGLE_UNWRAPPED_TEXTURE
                                                  Triangle2( glm::vec2( 1.0f / 3.0f, 2.0f / 3.0f                             + 1.0f / 6.0f ),
                                                             glm::vec2( 2.0f / 3.0f, 2.0f / 3.0f                             + 1.0f / 6.0f ),
                                                             glm::vec2( 1.0f / 3.0f + 1.0f / 6.0f, 2.0f / 3.0f + 1.0f / 6.0f + 1.0f / 6.0f ) )
                                          #else
                                                  Triangle2( glm::vec2( 0.0f, 0.0f ),
                                                             glm::vec2( 1.0f, 0.0f ),
                                                             glm::vec2( 0.5f, 1.0f ) )
                                          #endif
                                                  );

        QList< QPair< Triangle3, Triangle2 > > allTriangles;
        allTriangles << floorTriangles
                     << wallsTriangles
                     << roofTriangles;

#ifdef THOROUGH_LOGGING_ENABLED
//        qDebug() << "allTriangles:";
//        const int allTrianglesCount = allTriangles.size();
//        for ( int i = 0; i < allTrianglesCount; ++ i )
//        {
//            const QPair< Triangle3, Triangle2 >& triangle = allTriangles[ i ];
//            qDebug().nospace() << "\ttriangle #" << i << ':' << endl
//                               << "\t\tg:" << triangle.first << endl
//                               << "\t\tt:" << triangle.second;
//        }
#endif

        const Mesh3 mesh( allTriangles );

        QList< glm::vec3 > allUniqueVertexNormals;
        allUniqueVertexNormals.reserve( VerticesCount );

        QList< glm::vec4 > allUniqueVertexTangents;
        allUniqueVertexTangents.reserve( VerticesCount );
        for ( int i = 0; i < VerticesCount; ++ i )
        {
            const glm::vec3& uniqueVertex = allUniqueVertices[ i ];
            allUniqueVertexNormals.append( mesh.normalAtVertex( uniqueVertex ) );
            allUniqueVertexTangents.append( mesh.tangentAtVertex( uniqueVertex ) );
        }

        qDebug() << "all tangents:" << endl
                 << allUniqueVertexTangents;

        glGenBuffers( CommonEntities::VBOIndicesCount, m_vbos );
        LOG_GL_ERROR();

        QVector< GLfloat > verticesAxes;
        QVector< GLfloat > vertexNormalsAxes;
        QVector< GLfloat > vertexTangentsAxes;
        QVector< GLfloat > vertexTextureCoordsAxes;

        // floor
        GLWidget::axesFromTriangles( verticesAxes,
                                     vertexNormalsAxes,
                                     vertexTangentsAxes,
                                     vertexTextureCoordsAxes,
                                     floorTriangles,
                                     allUniqueVertices,
                                     allUniqueVertexNormals,
                                     allUniqueVertexTangents );

        // floor - vertices
        glBindBuffer( GL_ARRAY_BUFFER, m_vbos[ CommonEntities::VBOIndexFloorVertex ] );
        LOG_GL_ERROR();
        glBufferData( GL_ARRAY_BUFFER, verticesAxes.size() * sizeof( GLfloat ), verticesAxes.constData(), GL_STATIC_DRAW );
        LOG_GL_ERROR();

        // floor - vertex normals
        glBindBuffer( GL_ARRAY_BUFFER, m_vbos[ CommonEntities::VBOIndexFloorVertexNormal ] );
        LOG_GL_ERROR();
        glBufferData( GL_ARRAY_BUFFER, vertexNormalsAxes.size() * sizeof( GLfloat ), vertexNormalsAxes.constData(), GL_STATIC_DRAW );
        LOG_GL_ERROR();

        // floor - vertex tangents
        glBindBuffer( GL_ARRAY_BUFFER, m_vbos[ CommonEntities::VBOIndexFloorVertexTangent ] );
        LOG_GL_ERROR();
        glBufferData( GL_ARRAY_BUFFER, vertexTangentsAxes.size() * sizeof( GLfloat ), vertexTangentsAxes.constData(), GL_STATIC_DRAW );
        LOG_GL_ERROR();

        // floor - texture coordinates
        glBindBuffer( GL_ARRAY_BUFFER, m_vbos[ CommonEntities::VBOIndexFloorTexCoord ] );
        LOG_GL_ERROR();
        glBufferData( GL_ARRAY_BUFFER, vertexTextureCoordsAxes.size() * sizeof( GLfloat ), vertexTextureCoordsAxes.constData(), GL_STATIC_DRAW );
        LOG_GL_ERROR();

        // walls
        GLWidget::axesFromTriangles( verticesAxes,
                                     vertexNormalsAxes,
                                     vertexTangentsAxes,
                                     vertexTextureCoordsAxes,
                                     wallsTriangles,
                                     allUniqueVertices,
                                     allUniqueVertexNormals,
                                     allUniqueVertexTangents );

        // walls - vertices
        glBindBuffer( GL_ARRAY_BUFFER, m_vbos[ CommonEntities::VBOIndexWallsVertex ] );
        LOG_GL_ERROR();
        glBufferData( GL_ARRAY_BUFFER, verticesAxes.size() * sizeof( GLfloat ), verticesAxes.constData(), GL_STATIC_DRAW );
        LOG_GL_ERROR();

        // walls - vertex normals
        glBindBuffer( GL_ARRAY_BUFFER, m_vbos[ CommonEntities::VBOIndexWallsVertexNormal ] );
        LOG_GL_ERROR();
        glBufferData( GL_ARRAY_BUFFER, vertexNormalsAxes.size() * sizeof( GLfloat ), vertexNormalsAxes.constData(), GL_STATIC_DRAW );
        LOG_GL_ERROR();

        // floor - vertex tangents
        glBindBuffer( GL_ARRAY_BUFFER, m_vbos[ CommonEntities::VBOIndexWallsVertexTangent ] );
        LOG_GL_ERROR();
        glBufferData( GL_ARRAY_BUFFER, vertexTangentsAxes.size() * sizeof( GLfloat ), vertexTangentsAxes.constData(), GL_STATIC_DRAW );
        LOG_GL_ERROR();

        // walls - texture coordinates
        glBindBuffer( GL_ARRAY_BUFFER, m_vbos[ CommonEntities::VBOIndexWallsTexCoord ] );
        LOG_GL_ERROR();
        glBufferData( GL_ARRAY_BUFFER, vertexTextureCoordsAxes.size() * sizeof( GLfloat ), vertexTextureCoordsAxes.constData(), GL_STATIC_DRAW );
        LOG_GL_ERROR();

        // roof
        GLWidget::axesFromTriangles( verticesAxes,
                                     vertexNormalsAxes,
                                     vertexTangentsAxes,
                                     vertexTextureCoordsAxes,
                                     roofTriangles,
                                     allUniqueVertices,
                                     allUniqueVertexNormals,
                                     allUniqueVertexTangents );

        // roof - vertices
        glBindBuffer( GL_ARRAY_BUFFER, m_vbos[ CommonEntities::VBOIndexRoofSidesVertex ] );
        LOG_GL_ERROR();
        glBufferData( GL_ARRAY_BUFFER, verticesAxes.size() * sizeof( GLfloat ), verticesAxes.constData(), GL_STATIC_DRAW );
        LOG_GL_ERROR();

        // roof - vertex normals
        glBindBuffer( GL_ARRAY_BUFFER, m_vbos[ CommonEntities::VBOIndexRoofSidesVertexNormal ] );
        LOG_GL_ERROR();
        glBufferData( GL_ARRAY_BUFFER, vertexNormalsAxes.size() * sizeof( GLfloat ), vertexNormalsAxes.constData(), GL_STATIC_DRAW );
        LOG_GL_ERROR();

        // roof - vertex tangents
        glBindBuffer( GL_ARRAY_BUFFER, m_vbos[ CommonEntities::VBOIndexRoofSidesVertexTangent ] );
        LOG_GL_ERROR();
        glBufferData( GL_ARRAY_BUFFER, vertexTangentsAxes.size() * sizeof( GLfloat ), vertexTangentsAxes.constData(), GL_STATIC_DRAW );
        LOG_GL_ERROR();

        // roof - texture coordinates
        glBindBuffer( GL_ARRAY_BUFFER, m_vbos[ CommonEntities::VBOIndexRoofSidesTexCoord ] );
        LOG_GL_ERROR();
        glBufferData( GL_ARRAY_BUFFER, vertexTextureCoordsAxes.size() * sizeof( GLfloat ), vertexTextureCoordsAxes.constData(), GL_STATIC_DRAW );
        LOG_GL_ERROR();

        glGenVertexArrays( CommonEntities::VAOIndicesCount, m_vaos );
        LOG_GL_ERROR();

        // Floor
        glBindVertexArray( m_vaos[ CommonEntities::VAOIndexFloor ] );
        LOG_GL_ERROR();

        GLWidget::enableGenericVertexAttributeArrays( true );

        // Map index 0 to the vertices buffer
        glBindBuffer( GL_ARRAY_BUFFER, m_vbos[ CommonEntities::VBOIndexFloorVertex ] );
        LOG_GL_ERROR();
        glVertexAttribPointer( GenericVertexAttributeIndexVertexPos,
                               3,
                               GL_FLOAT,
                               GL_FALSE,
                               0,
                               ( GLubyte* ) NULL );
        LOG_GL_ERROR();

        // Map index 1 to the vertex normals buffer
        glBindBuffer( GL_ARRAY_BUFFER, m_vbos[ CommonEntities::VBOIndexFloorVertexNormal ] );
        LOG_GL_ERROR();
        glVertexAttribPointer( GenericVertexAttributeIndexVertexNormal,
                               3,
                               GL_FLOAT,
                               GL_FALSE,
                               0,
                               ( GLubyte* ) NULL );
        LOG_GL_ERROR();

        // Map index 2 to the vertex tangents buffer
        glBindBuffer( GL_ARRAY_BUFFER, m_vbos[ CommonEntities::VBOIndexFloorVertexTangent ] );
        LOG_GL_ERROR();
        glVertexAttribPointer( GenericVertexAttributeIndexVertexTangent,
                               4,
                               GL_FLOAT,
                               GL_FALSE,
                               0,
                               ( GLubyte* ) NULL );
        LOG_GL_ERROR();

        // Map index 3 to the texture coordinates buffer
        glBindBuffer( GL_ARRAY_BUFFER, m_vbos[ CommonEntities::VBOIndexFloorTexCoord ] );
        LOG_GL_ERROR();
        glVertexAttribPointer( GenericVertexAttributeIndexVertexTexCoord,
                               2,
                               GL_FLOAT,
                               GL_FALSE,
                               0,
                               ( GLubyte* ) NULL );
        LOG_GL_ERROR();

        GLWidget::enableGenericVertexAttributeArrays( false );

        // Walls
        glBindVertexArray( m_vaos[ CommonEntities::VAOIndexWalls ] );
        LOG_GL_ERROR();

        GLWidget::enableGenericVertexAttributeArrays( true );

        // Map index 0 to the vertices buffer
        glBindBuffer( GL_ARRAY_BUFFER, m_vbos[ CommonEntities::VBOIndexWallsVertex ] );
        LOG_GL_ERROR();
        glVertexAttribPointer( GenericVertexAttributeIndexVertexPos,
                               3,
                               GL_FLOAT,
                               GL_FALSE,
                               0,
                               ( GLubyte* ) NULL );
        LOG_GL_ERROR();

        // Map index 1 to the vertex normals buffer
        glBindBuffer( GL_ARRAY_BUFFER, m_vbos[ CommonEntities::VBOIndexWallsVertexNormal ] );
        LOG_GL_ERROR();
        glVertexAttribPointer( GenericVertexAttributeIndexVertexNormal,
                               3,
                               GL_FLOAT,
                               GL_FALSE,
                               0,
                               ( GLubyte* ) NULL );
        LOG_GL_ERROR();

        // Map index 2 to the vertex tangents buffer
        glBindBuffer( GL_ARRAY_BUFFER, m_vbos[ CommonEntities::VBOIndexWallsVertexTangent ] );
        LOG_GL_ERROR();
        glVertexAttribPointer( GenericVertexAttributeIndexVertexTangent,
                               4,
                               GL_FLOAT,
                               GL_FALSE,
                               0,
                               ( GLubyte* ) NULL );
        LOG_GL_ERROR();

        // Map index 3 to the texture coordinates buffer
        glBindBuffer( GL_ARRAY_BUFFER, m_vbos[ CommonEntities::VBOIndexWallsTexCoord ] );
        LOG_GL_ERROR();
        glVertexAttribPointer( GenericVertexAttributeIndexVertexTexCoord,
                               2,
                               GL_FLOAT,
                               GL_FALSE,
                               0,
                               ( GLubyte* ) NULL );
        LOG_GL_ERROR();

        GLWidget::enableGenericVertexAttributeArrays( false );

        // Roof
        glBindVertexArray( m_vaos[ CommonEntities::VAOIndexRoofSides ] );
        LOG_GL_ERROR();

        GLWidget::enableGenericVertexAttributeArrays( true );

        // Map index 0 to the vertices buffer
        glBindBuffer( GL_ARRAY_BUFFER, m_vbos[ CommonEntities::VBOIndexRoofSidesVertex ] );
        LOG_GL_ERROR();
        glVertexAttribPointer( GenericVertexAttributeIndexVertexPos,
                               3,
                               GL_FLOAT,
                               GL_FALSE,
                               0,
                               ( GLubyte* ) NULL );
        LOG_GL_ERROR();

        // Map index 1 to the vertex normals
        glBindBuffer( GL_ARRAY_BUFFER, m_vbos[ CommonEntities::VBOIndexRoofSidesVertexNormal ] );
        LOG_GL_ERROR();
        glVertexAttribPointer( GenericVertexAttributeIndexVertexNormal,
                               3,
                               GL_FLOAT,
                               GL_FALSE,
                               0,
                               ( GLubyte* ) NULL );
        LOG_GL_ERROR();

        // Map index 2 to the vertex tangents buffer
        glBindBuffer( GL_ARRAY_BUFFER, m_vbos[ CommonEntities::VBOIndexRoofSidesVertexTangent ] );
        LOG_GL_ERROR();
        glVertexAttribPointer( GenericVertexAttributeIndexVertexTangent,
                               4,
                               GL_FLOAT,
                               GL_FALSE,
                               0,
                               ( GLubyte* ) NULL );
        LOG_GL_ERROR();

        // Map index 3 to the texture coordinates buffer
        glBindBuffer( GL_ARRAY_BUFFER, m_vbos[ CommonEntities::VBOIndexRoofSidesTexCoord ] );
        LOG_GL_ERROR();
        glVertexAttribPointer( GenericVertexAttributeIndexVertexTexCoord,
                               2,
                               GL_FLOAT,
                               GL_FALSE,
                               0,
                               ( GLubyte* ) NULL );
        LOG_GL_ERROR();

        GLWidget::enableGenericVertexAttributeArrays( false );

        glBindVertexArray( 0 );
        LOG_GL_ERROR();
    }

    if ( changeActiveShaderProgramHandle )
    {
        glUseProgram( activeShaderProgramHandle );
        LOG_GL_ERROR();
    }
}

void GLWidget::resizeGL( int w, int h )
{
    const int minSideSize = qMin( w, h );
    glViewport( ( w - minSideSize ) / 2,
                ( h - minSideSize ) / 2,
                minSideSize,
                minSideSize );
    LOG_GL_ERROR();
}

void GLWidget::paintGL()
{
    if ( m_isOpenGLVersion330CoreProfileUsed )
    {
        glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
        LOG_GL_ERROR();

        if ( m_shaderProgramHandle == 0 )
        {
            return;
        }

        const GLuint activeShaderProgramHandle = GLWidget::activeShaderProgramHandle();
        const bool changeActiveShaderProgramHandle = activeShaderProgramHandle != m_shaderProgramHandle;
        if ( changeActiveShaderProgramHandle )
        {
            glUseProgram( m_shaderProgramHandle );
            LOG_GL_ERROR();
        }

        static const GLsizei verticesCountPerVaoIndex[] =
        {
            1 /* sides */ * 2 /* triangles */ * TRIANGLE_VERTICES_COUNT,
            4 /* sides */ * 2 /* triangles */ * TRIANGLE_VERTICES_COUNT,
            4 /* sides */ * 1 /* triangles */ * TRIANGLE_VERTICES_COUNT
        };

        for ( int i = 0; i < CommonEntities::VAOIndicesCount; ++ i )
        {
            glBindVertexArray( m_vaos[ i ] );
            LOG_GL_ERROR();

            GLWidget::enableGenericVertexAttributeArrays( true );

            this->bindTexturesForVaoIndex( static_cast< CommonEntities::VAOIndex > ( i ) );

            glDrawArrays( GL_TRIANGLES, 0, verticesCountPerVaoIndex[ i ] );
            LOG_GL_ERROR();

            GLWidget::enableGenericVertexAttributeArrays( false );
        }

        glBindVertexArray( 0 );
        LOG_GL_ERROR();

        if ( changeActiveShaderProgramHandle )
        {
            glUseProgram( activeShaderProgramHandle );
            LOG_GL_ERROR();
        }
    }
    else
    {
        glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
        LOG_GL_ERROR();
        glEnable( GL_TEXTURE_2D );
        LOG_GL_ERROR();
        glTexEnvf( GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE );
        LOG_GL_ERROR();

        glMatrixMode( GL_MODELVIEW );
        LOG_GL_ERROR();
        glLoadIdentity();
        LOG_GL_ERROR();

        // TODO: multiply the modelview/projection matrix by the keyboard, mouse and the cached one

        static const GLfloat actualZDirection = 1.0f;

        static const GLfloat textureCoordsSquare[ SQUARE_VERTICES_COUNT ][ AXES_COUNT_2D ] =
        {
            { 0.0f, 0.0f },
            { 1.0f, 0.0f },
            { 1.0f, 1.0f },
            { 0.0f, 1.0f }
        };

        static const GLfloat cubeSides[][ SQUARE_VERTICES_COUNT ][ AXES_COUNT_3D ] =
        {
            // FRONT
            {
                { -0.5f, -0.5f, -0.5f * actualZDirection },
                { 0.5f, -0.5f, -0.5f * actualZDirection },
                { 0.5f, 0.5f, -0.5f * actualZDirection },
                { -0.5f, 0.5f, -0.5f * actualZDirection }
            },

            // BACK
            {
                { 0.5f, -0.5f, 0.5f * actualZDirection },
                { -0.5f, -0.5f, 0.5f * actualZDirection },
                { -0.5f, 0.5f, 0.5f * actualZDirection },
                { 0.5f, 0.5f, 0.5f * actualZDirection }
            },

            // LEFT
            {
                { -0.5f, -0.5f, 0.5f * actualZDirection },
                { -0.5f, -0.5f, -0.5f * actualZDirection },
                { -0.5f, 0.5f, -0.5f * actualZDirection },
                { -0.5f, 0.5f, 0.5f * actualZDirection }
            },

            // RIGHT
            {
                { 0.5f, -0.5f, -0.5f * actualZDirection },
                { 0.5f, -0.5f, 0.5f * actualZDirection },
                { 0.5f, 0.5f, 0.5f * actualZDirection },
                { 0.5f, 0.5f, -0.5f * actualZDirection }
            },

            // TOP
            {
                { -0.5f, 0.5f, -0.5f * actualZDirection },
                { 0.5f, 0.5f, -0.5f * actualZDirection },
                { 0.5f, 0.5f, 0.5f * actualZDirection },
                { -0.5f, 0.5f, 0.5f * actualZDirection }
            },

            // BOTTOM
            {
                { -0.5f, -0.5f, 0.5f * actualZDirection },
                { 0.5f, -0.5f, 0.5f * actualZDirection },
                { 0.5f, -0.5f, -0.5f * actualZDirection },
                { -0.5f, -0.5f, -0.5f * actualZDirection },
            }
        };

        static const GLubyte cubeSideColors[][ RGB_COLOR_COMPONENTS_COUNT ] =
        {
            { 255, 0, 0 },
            { 0, 255, 0 },
            { 0, 0, 255 },
            { 255, 255, 0 },
            { 0, 255, 255 },
            { 127, 127, 127 }
        };

        this->bindTextureForTextureIndex( CommonEntities::TextureIndexWallsColor );

        glBegin( GL_QUADS );
        LOG_GL_ERROR();
        for ( int i = 0; i < CubeSidesCount - 1; ++ i )
        {
            glColor3ubv( cubeSideColors[ i ] );
            LOG_GL_ERROR();
            for ( int j = 0; j < SQUARE_VERTICES_COUNT; ++j )
            {
                glTexCoord3fv( textureCoordsSquare[ j ] );
                LOG_GL_ERROR();
                glVertex3fv( cubeSides[ i ][ j ] );
                LOG_GL_ERROR();
            }
        }
        glEnd();
        LOG_GL_ERROR();

        this->bindTextureForTextureIndex( CommonEntities::TextureIndexFloorColor );

        glBegin( GL_QUADS );
        LOG_GL_ERROR();
        glColor3ubv( cubeSideColors[ CubeSidesCount - 1 ] );
        LOG_GL_ERROR();
        for ( int j = 0; j < SQUARE_VERTICES_COUNT; ++j )
        {
            glTexCoord3fv( textureCoordsSquare[ j ] );
            LOG_GL_ERROR();
            glVertex3fv( cubeSides[ CubeSidesCount - 1 ][ j ] );
            LOG_GL_ERROR();
        }
        glEnd();
        LOG_GL_ERROR();

        for ( int i = 0; i < CubeSidesCount; ++ i )
        {
            glLineWidth( 1.0f );
            LOG_GL_ERROR();
            glColor3ub( 0, 0, 0 );
            LOG_GL_ERROR();
            glBegin( GL_LINE_LOOP );
            LOG_GL_ERROR();
            for ( int j = 0; j < SQUARE_VERTICES_COUNT; ++j )
            {
                glVertex3fv( cubeSides[ i ][ j ] );
            }
            glEnd();
        }

        static const GLfloat roofSides[][ SQUARE_VERTICES_COUNT ][ AXES_COUNT_3D ] =
        {
            // LEFT
            {
                { -0.5f, 0.5f, 0.5f * actualZDirection },
                { -0.5f, 0.5f, -0.5f * actualZDirection },
                { 0.0f, 0.75f, -0.5f * actualZDirection },
                { 0.0f, 0.75f, 0.5f * actualZDirection }
            },

            // RIGHT
            {
                { 0.5f, 0.5f, -0.5f * actualZDirection },
                { 0.5f, 0.5f, 0.5f * actualZDirection },
                { 0.0f, 0.75f, 0.5f * actualZDirection },
                { 0.0f, 0.75f, -0.5f * actualZDirection },
            }
        };

        this->bindTextureForTextureIndex( CommonEntities::TextureIndexRoofSidesColor );

        glBegin( GL_QUADS );
        LOG_GL_ERROR();
        const int roofSidesCount = sizeof( roofSides ) / sizeof( roofSides[ 0 ] );
        for ( int i = 0; i < roofSidesCount; ++ i )
        {
            for ( int j = 0; j < SQUARE_VERTICES_COUNT; ++j )
            {
                glTexCoord3fv( textureCoordsSquare[ j ] );
                LOG_GL_ERROR();
                glVertex3fv( roofSides[ i ][ j ] );
                LOG_GL_ERROR();
            }
        }
        glEnd();

        static const GLfloat roofWalls[][ TRIANGLE_VERTICES_COUNT ][ AXES_COUNT_3D ] =
        {
            // FRONT
            {
                { -0.5, 0.5, -0.5 * actualZDirection },
                { 0.5, 0.5, -0.5 * actualZDirection },
                { 0.0, 0.75, -0.5 * actualZDirection }
            },

            // BACK
            {
                { 0.5, 0.5, 0.5 * actualZDirection },
                { -0.5, 0.5, 0.5 * actualZDirection },
                { 0.0, 0.75, 0.5 * actualZDirection }
            }
        };

        static const GLfloat textureCoordsRoofWall[ TRIANGLE_VERTICES_COUNT ][ AXES_COUNT_2D ] =
        {
            { 0.0, 0.0 },
            { 1.0, 0.0 },
            { 0.5, 0.25 }
        };

        this->bindTextureForTextureIndex( CommonEntities::TextureIndexFloorColor );

        LOG_GL_ERROR();
        glBegin( GL_TRIANGLES );
        LOG_GL_ERROR();
        const int roofWallsCount = sizeof( roofWalls ) / sizeof( roofWalls[ 0 ] );
        for ( int i = 0; i < roofWallsCount; ++ i )
        {
            for ( int j = 0; j < TRIANGLE_VERTICES_COUNT; ++j )
            {
                glTexCoord3fv( textureCoordsRoofWall[ j ] );
                LOG_GL_ERROR();
                glVertex3fv( roofWalls[ i ][ j ] );
                LOG_GL_ERROR();
            }
        }
        glEnd();

        glFlush();
        LOG_GL_ERROR();
        glDisable( GL_TEXTURE_2D );
        LOG_GL_ERROR();
    }
}

void GLWidget::keyPressEvent( QKeyEvent* event )
{
    QWidget::keyPressEvent( event );

    const QHash< Qt::Key, QPair< glm::vec3, GLfloat > >& arrowKeysMatrixDeltaChangesContainer = GLWidget::arrowKeysMatrixDeltaChanges();
    const QHash< Qt::Key, QPair< glm::vec3, GLfloat > >::const_iterator arrowKeysMatrixDeltaChangesContainerIter = arrowKeysMatrixDeltaChangesContainer.find( static_cast< Qt::Key >( event->key() ) );
    if ( arrowKeysMatrixDeltaChangesContainerIter == arrowKeysMatrixDeltaChangesContainer.constEnd() )
    {
        return;
    }

    const QPair< glm::vec3, GLfloat >& matrixDeltaChanges = arrowKeysMatrixDeltaChangesContainerIter.value();
    glm::mat4& keyMatrix = m_matricesData[ event->modifiers().testFlag( Qt::ShiftModifier )
                                           ? MatrixDataIndexView
                                           : MatrixDataIndexModel ].keyMatrix;
    keyMatrix = glm::rotate( keyMatrix, matrixDeltaChanges.second, matrixDeltaChanges.first );

    this->applyMatrices();
}

void GLWidget::keyReleaseEvent( QKeyEvent* event )
{
    QWidget::keyReleaseEvent( event );

    if ( ! GLWidget::arrowKeysMatrixDeltaChanges().contains( static_cast< Qt::Key > ( event->key() ) ) )
    {
        return;
    }

    MatrixData& matrixData = m_matricesData[ event->modifiers().testFlag( Qt::ShiftModifier )
                                             ? MatrixDataIndexView
                                             : MatrixDataIndexModel ];
    glm::mat4& keyMatrix = matrixData.keyMatrix;
    glm::mat4& cacheMatrix = matrixData.cacheMatrix;
    cacheMatrix = keyMatrix * cacheMatrix;
    keyMatrix = glm::mat4( 1.0f );

    this->applyMatrices();
}

void GLWidget::mousePressEvent( QMouseEvent* event )
{
    QGLWidget::mousePressEvent( event );

    const Qt::MouseButton pressedMouseButton = event->button();
    if ( pressedMouseButton != Qt::LeftButton
         && pressedMouseButton != Qt::RightButton )
    {
        return;
    }

    m_mousePressPos = event->pos();
}

void GLWidget::mouseMoveEvent( QMouseEvent* event )
{
    QGLWidget::mouseMoveEvent( event );

    // QMouseEvent::button()
    // Note that the returned value is always Qt::NoButton for mouse move events.

    const Qt::MouseButtons pressedMouseButtons = event->buttons();
    const bool isLeftMouseButtonPressed = pressedMouseButtons.testFlag( Qt::LeftButton );
    const bool isRightMouseButtonPressed = pressedMouseButtons.testFlag( Qt::RightButton );
    if ( ! isLeftMouseButtonPressed
         && ! isRightMouseButtonPressed )
    {
        return;
    }

    const QPoint offset = event->pos() - m_mousePressPos;

    const GLfloat xRotAngle = GLfloat( offset.y() ) / this->height() * 360.0f;
    const GLfloat yRotAngle = GLfloat( offset.x() ) / this->width() * 360.0f;

    glm::mat4& mouseMatrix = m_matricesData[ isLeftMouseButtonPressed
                                             ? MatrixDataIndexModel
                                             : MatrixDataIndexView ].mouseMatrix;
    mouseMatrix = glm::mat4( 1.0f );
    mouseMatrix = glm::rotate( mouseMatrix, xRotAngle, s_xAxisDir );
    mouseMatrix = glm::rotate( mouseMatrix, yRotAngle, s_yAxisDir );

    this->applyMatrices();
}

void GLWidget::mouseReleaseEvent( QMouseEvent* event )
{
    QGLWidget::mouseReleaseEvent( event );

    const Qt::MouseButton pressedMouseButton = event->button();
    const bool isLeftMouseButton = pressedMouseButton == Qt::LeftButton;
    if ( ! isLeftMouseButton
         && pressedMouseButton != Qt::RightButton )
    {
        return;
    }

    MatrixData& matrixData = m_matricesData[ isLeftMouseButton
                                             ? MatrixDataIndexModel
                                             : MatrixDataIndexView ];
    glm::mat4& mouseMatrix = matrixData.mouseMatrix;
    glm::mat4& cacheMatrix = matrixData.cacheMatrix;
    cacheMatrix = mouseMatrix * cacheMatrix;
    mouseMatrix = glm::mat4( 1.0f );

    this->applyMatrices();

    if ( ! this->hasFocus() )
    {
        this->setFocus( Qt::MouseFocusReason );
    }
}

#ifndef QT_NO_WHEELEVENT
void GLWidget::wheelEvent( QWheelEvent* event )
{
    QGLWidget::wheelEvent( event );
}
#endif

QString GLWidget::genericVertexAttributeName( const GLWidget::GenericVertexAttributeIndex genericVertexAttributeIndex )
{
    switch ( genericVertexAttributeIndex )
    {
        case GenericVertexAttributeIndexVertexPos:
            return "vVertexPos";
        case GenericVertexAttributeIndexVertexNormal:
            return "vVertexNormal";
        case GenericVertexAttributeIndexVertexTangent:
            return "vVertexTangent";
        case GenericVertexAttributeIndexVertexTexCoord:
            return "vVertexTexCoord";
        default:
            break;
    }

    return QString();
}

void GLWidget::init()
{
    for ( int i = 0; i < CommonEntities::VBOIndicesCount; ++ i )
    {
        m_vbos[ i ] = 0;
    }

    for ( int i = 0; i < CommonEntities::VAOIndicesCount; ++ i )
    {
        m_vaos[ i ] = 0;
    }

    for ( int i = 0; i < UniformVarsCount; ++ i )
    {
        QPair< GLint, QString >& uniformVar = m_uniformVars[ i ];
        uniformVar.first = CommonEntities::INVALID_UNIFORM_VAR_LOCATION;
        uniformVar.second = GLWidget::uniformVarName( static_cast< UniformVarIndex > ( i ) );
    }

    m_shaderProgramHandle = 0;
    m_isOpenGLVersion330CoreProfileUsed = this->isOpenGLVersion330CoreProfile();
}

bool GLWidget::loadShaderPairWithAttributes( const ShaderInfo& vertexShaderInfo,
                                             const ShaderInfo& fragmentShaderInfo )
{
    this->clearShaderProgram();

    struct ShaderInternalInfo
    {
        QString filePath;
        GLenum type;
        GLuint handle;
    };

    ShaderInternalInfo shaderInfos[] =
    {
        {
            vertexShaderInfo.filePath,
            GL_VERTEX_SHADER,
            0
        },
        {
            fragmentShaderInfo.filePath,
            GL_FRAGMENT_SHADER,
            0
        }
    };

    const int shadersInternalInfoCount = sizeof( shaderInfos ) / sizeof( shaderInfos[ 0 ] );
    bool shadersSetupSuccessfully = true;
    for ( int shaderIndex = 0; shaderIndex < shadersInternalInfoCount; ++ shaderIndex )
    {
        ShaderInternalInfo& shaderInfo = shaderInfos[ shaderIndex ];
        shaderInfo.handle = glCreateShader( shaderInfo.type );
        LOG_GL_ERROR();
        if ( 0 == shaderInfo.handle )
        {
            qDebug() << "Shader:" << shaderInfo.filePath << ':' << "failed creating shader of type: " << shaderInfo.type;
            shadersSetupSuccessfully = false;
            continue;
        }

        QFile shaderFile( shaderInfo.filePath );
        if ( ! shaderFile.exists() )
        {
            qDebug() << "Shader:" << shaderInfo.filePath << ':' << "file doesn't exist";
            shadersSetupSuccessfully = false;
            continue;
        }

        if ( ! shaderFile.open( QIODevice::ReadOnly ) )
        {
            qDebug() << "Shader:" << shaderInfo.filePath << ':' << "file cannot be opened for reading";
            shadersSetupSuccessfully = false;
            continue;
        }

        const QByteArray shaderSource = shaderFile.readAll();
        shaderFile.close();

        const GLchar* shaderCode = shaderSource.data();
        const GLchar* codeArray[] = { shaderCode };
        glShaderSource( shaderInfo.handle, 1, codeArray, NULL );
        LOG_GL_ERROR();

        glCompileShader( shaderInfo.handle );
        LOG_GL_ERROR();

        GLint shaderCompilationStatus = GL_FALSE;
        glGetShaderiv( shaderInfo.handle, GL_COMPILE_STATUS, &shaderCompilationStatus );
        LOG_GL_ERROR();

        if ( GL_TRUE == shaderCompilationStatus )
        {
            continue;
        }

        shadersSetupSuccessfully = false;
        qDebug() << "Shader:" << shaderInfo.filePath << ':' << "compilation failed";

        GLint logLength = 0;
        glGetShaderiv( shaderInfo.handle, GL_INFO_LOG_LENGTH, &logLength );
        LOG_GL_ERROR();

        if ( logLength == 0 )
        {
            qDebug() << "Shader:" << shaderInfo.filePath << ':' << "could not obtain compilation log length";
            continue;
        }

        char* const log = ( char* ) malloc( logLength );
        glGetShaderInfoLog( shaderInfo.handle, logLength, NULL, log );
        LOG_GL_ERROR();

        qDebug() << "Shader:" << shaderInfo.filePath << ':' << "compilation log:" << endl << log;
        free( log );
    }

    if ( ! shadersSetupSuccessfully )
    {
        qDebug() << "Either the vertex or the fragment shader wasn't setup correctly." << endl
                 << "Skipping program creation and attributes assignment.";

        return false;
    }

    m_shaderProgramHandle = glCreateProgram();
    LOG_GL_ERROR();
    if ( 0 == m_shaderProgramHandle )
    {
        qDebug() << "Error creating program object.";
        return false;
    }

    for ( int shaderIndex = 0; shaderIndex < shadersInternalInfoCount; ++ shaderIndex )
    {
        glAttachShader( m_shaderProgramHandle, shaderInfos[ shaderIndex ].handle );
        LOG_GL_ERROR();
    }

    const ShaderInfo* const shaders[] =
    {
        &vertexShaderInfo,
        &fragmentShaderInfo
    };

    const int shadersCount = sizeof( shaders ) / sizeof( shaders[ 0 ] );
    for ( int shaderIndex = 0;
          shaderIndex < shadersCount;
          ++ shaderIndex )
    {
        const ShaderInfo* const shader = shaders[ shaderIndex ];
        const ShaderVariableInfoContainer& shaderVariables = shader->variables;
        const ShaderVariableInfoContainer::const_iterator shaderVariablesIterEnd = shaderVariables.constEnd();
        for ( ShaderVariableInfoContainer::const_iterator shaderVariablesIter = shaderVariables.constBegin();
              shaderVariablesIter != shaderVariablesIterEnd;
              ++ shaderVariablesIter )
        {
            if ( shader->type == ShaderTypeVertex )
            {
                glBindAttribLocation( m_shaderProgramHandle, shaderVariablesIter.key(),
                                      shaderVariablesIter.value().toLatin1().constData() );
                LOG_GL_ERROR();
            }
            else if ( shader->type == ShaderTypeFragment )
            {
                glBindFragDataLocation( m_shaderProgramHandle, shaderVariablesIter.key(),
                                        shaderVariablesIter.value().toLatin1().constData() );
                LOG_GL_ERROR();
            }
        }
    }

    glLinkProgram( m_shaderProgramHandle );
    LOG_GL_ERROR();

    for ( int shaderIndex = 0;
          shaderIndex < shadersInternalInfoCount;
          ++ shaderIndex )
    {
        const GLuint shaderHandle = shaderInfos[ shaderIndex ].handle;
        glDetachShader( m_shaderProgramHandle, shaderHandle );
        LOG_GL_ERROR();

        glDeleteShader( shaderHandle );
        LOG_GL_ERROR();
    }

    GLint programLinkingStatus = GL_FALSE;
    glGetProgramiv( m_shaderProgramHandle, GL_LINK_STATUS, &programLinkingStatus );
    LOG_GL_ERROR();

    if ( GL_FALSE == programLinkingStatus )
    {
        qDebug() << "Failed to link shader program.";
        GLint logLength = 0;
        glGetProgramiv( m_shaderProgramHandle, GL_INFO_LOG_LENGTH, &logLength );
        LOG_GL_ERROR();

        if ( logLength == 0 )
        {
            qDebug() << "Failed to obtain program linking status message length.";
            return false;
        }

        char* const log = ( char* ) malloc( logLength );
        GLsizei written = 0;
        glGetProgramInfoLog( m_shaderProgramHandle, logLength, &written, log );
        LOG_GL_ERROR();

        qDebug() << "Program log:" << endl << log;
        free( log );

        this->clearShaderProgram();

        return false;
    }

    this->printPostLinkGLInfo();

    return true;
}

void GLWidget::printGLInfo()
{
#ifdef THOROUGH_LOGGING_ENABLED
    const GLubyte* renderer = glGetString( GL_RENDERER );
    LOG_GL_ERROR();

    const GLubyte* vendor = glGetString( GL_VENDOR );
    LOG_GL_ERROR();

    const GLubyte* version = glGetString( GL_VERSION );
    LOG_GL_ERROR();

    const GLubyte* glslVersion = glGetString( GL_SHADING_LANGUAGE_VERSION );
    LOG_GL_ERROR();

    GLint major = -1;
    GLint minor = -1;
    glGetIntegerv( GL_MAJOR_VERSION, &major );
    LOG_GL_ERROR();

    glGetIntegerv( GL_MINOR_VERSION, &minor );
    LOG_GL_ERROR();

    GLint maxVertexTextureUnitsCount = 0;
    glGetIntegerv( GL_MAX_VERTEX_TEXTURE_IMAGE_UNITS, & maxVertexTextureUnitsCount );
    LOG_GL_ERROR();

    GLint maxFragmentTextureUnitsCount = 0;
    glGetIntegerv( GL_MAX_TEXTURE_IMAGE_UNITS, & maxFragmentTextureUnitsCount );
    LOG_GL_ERROR();

    GLint maxCombinedTextureUnitsCount = 0;
    glGetIntegerv( GL_MAX_COMBINED_TEXTURE_IMAGE_UNITS, & maxCombinedTextureUnitsCount );
    LOG_GL_ERROR();

    qDebug() << "GL Vendor:" << QLatin1String( reinterpret_cast< const char* > ( vendor ) ) << endl
             << "GL Renderer:" << QLatin1String( reinterpret_cast< const char* > ( renderer ) ) << endl
             << "GL Version (string):" << QLatin1String( reinterpret_cast< const char* > ( version ) ) << endl
             << "GL Version (integer):" << QString( "%1.%2" ).arg( major ).arg( minor ) << endl
             << "GLSL Version:" << QLatin1String( reinterpret_cast< const char* > ( glslVersion ) ) << endl
             << "Vertex texture units count:" << maxVertexTextureUnitsCount << endl
             << "Fragment texture units count:" << maxFragmentTextureUnitsCount << endl
             << "Combined texture units count:" << maxCombinedTextureUnitsCount;

    GLint extensionsCount = 0;
    glGetIntegerv( GL_NUM_EXTENSIONS, &extensionsCount );
    LOG_GL_ERROR();

    qDebug() << "extensions:";

    const int extensionsCountDigitsCount = qFloor( log10( static_cast< float >( extensionsCount ) ) ) + 1;
    for ( int i = 0; i < extensionsCount; ++i )
    {
        qDebug().nospace() << '\t' << QString( "%1" ).arg( i, extensionsCountDigitsCount, 10, QChar( QLatin1Char( ' ' ) ) ) << ": " << QLatin1String( reinterpret_cast< const char* > ( glGetStringi( GL_EXTENSIONS, i ) ) );
        LOG_GL_ERROR();
    }

    GLint frontFace = -1;
    glGetIntegerv( GL_FRONT_FACE, & frontFace );

    qDebug() << "GL_FRONT_FACE:" << ( frontFace == GL_CCW
                                      ? "CCW"
                                      : ( frontFace == GL_CW
                                          ? "CW"
                                          : "unknown" ) );
#endif
}

void GLWidget::printPostLinkGLInfo() const
{
#ifdef THOROUGH_LOGGING_ENABLED
    if ( m_shaderProgramHandle == 0 )
    {
        return;
    }

    GLint uniformVarsCount = 0;
    // NOTE: this is a program variable
    glGetProgramiv( m_shaderProgramHandle, GL_ACTIVE_UNIFORMS, &uniformVarsCount );
    LOG_GL_ERROR();

    qDebug() << "Uniform variables count:" << uniformVarsCount;

    qDebug() << "Uniform variables:";
    for ( int uniformVarIndex = 0;
          uniformVarIndex < uniformVarsCount;
          ++ uniformVarIndex )
    {
        GLsizei uniformVarNameLength = 0;
        GLint uniformVarSize = 0;
        GLenum uniformVarType = 0;
#ifndef UNIFORM_VAR_NAME_MAX_LENGTH
    #define UNIFORM_VAR_NAME_MAX_LENGTH 32
#endif
        GLchar uniformVarName[ UNIFORM_VAR_NAME_MAX_LENGTH ] = { '\0' };
        glGetActiveUniform( m_shaderProgramHandle,
                            uniformVarIndex,
                            UNIFORM_VAR_NAME_MAX_LENGTH,
                            & uniformVarNameLength,
                            & uniformVarSize,
                            & uniformVarType,
                            uniformVarName );

        if ( glGetError() != GL_NO_ERROR )
        {
            qDebug() << QString( "\tindex: %1: no uniform variable" )
                        .arg( uniformVarIndex );
            continue;
        }

        qDebug() << QString( "\tindex: %1, name: %2, type: %3 (%4), size: %5" )
                    .arg( uniformVarIndex )
                    .arg( uniformVarName )
                    .arg( GLWidget::shaderVariableTypeDescription( uniformVarType ) )
                    .arg( "0x" + QString::number( uniformVarType, 16 ).toUpper() )
                    .arg( uniformVarSize );
    }

    GLint maxVertexInputAttributesCount = 0;
    // The maximum number of 4-component generic vertex attributes accessible to a vertex shader.
    // The value must be at least 16.
    // NOTE: this is a OpenGL driver variable
    glGetIntegerv( GL_MAX_VERTEX_ATTRIBS, &maxVertexInputAttributesCount );
    LOG_GL_ERROR();

    qDebug() << "The maximum allowed count of 4-component generic vertex attributes accessible to a vertex shader:" << maxVertexInputAttributesCount;

    GLint vertexInputAttributesCount = 0;
    // NOTE: this is a program variable
    glGetProgramiv( m_shaderProgramHandle, GL_ACTIVE_ATTRIBUTES, &vertexInputAttributesCount );
    LOG_GL_ERROR();

    qDebug() << "The number of registered 4-component generic vertex attributes accessible to a vertex shader:" << vertexInputAttributesCount;

    GLint maxVertexInputAttributeNameLength = 0;
    // NOTE: this is a program variable
    glGetProgramiv( m_shaderProgramHandle, GL_ACTIVE_ATTRIBUTE_MAX_LENGTH, &maxVertexInputAttributeNameLength );
    LOG_GL_ERROR();

    qDebug() << "The maximum allowed character length of the name of a "
                "4-component generic vertex attributes accessible to a vertex shader:" << maxVertexInputAttributeNameLength;

    GLchar* const vertexInputAttributeName = ( GLchar* ) malloc( maxVertexInputAttributeNameLength * sizeof( GLchar ) );

    GLsizei vertexInputAttributeNameLength = -1;
    GLint vertexInputAttributeByteSize = 0;
    GLenum vertexInputAttributeType = 0;
    for ( int i = 0; i < vertexInputAttributesCount; ++ i )
    {
        vertexInputAttributeNameLength = 0;
        vertexInputAttributeByteSize = 0;
        vertexInputAttributeType = 0;

        glGetActiveAttrib( m_shaderProgramHandle,
                           i,
                           maxVertexInputAttributeNameLength,
                           &vertexInputAttributeNameLength,
                           &vertexInputAttributeByteSize,
                           &vertexInputAttributeType,
                           vertexInputAttributeName );
        LOG_GL_ERROR();

        if ( vertexInputAttributeNameLength == -1 )
        {
            qDebug() << "vertex input attribute:" << endl
                     << "index:" << i << endl
                     << "An error occurred.";
            continue;
        }

        if ( vertexInputAttributeNameLength == 0 )
        {
            qDebug() << "vertex input attribute:" << endl
                     << "index:" << i << endl
                     << "No information available.";
            continue;
        }

        qDebug() << "vertex input attribute:" << endl
                 << "index:" << i << endl
                 << "name:" << vertexInputAttributeName << endl
                 << "type:" << ( "0x" + QString::number( vertexInputAttributeType, 16 ).toUpper() ) << endl
                 << "byte-size:" << vertexInputAttributeByteSize;
    }

    free( vertexInputAttributeName );
#endif
}

void GLWidget::clearShaderProgram()
{
    if ( m_shaderProgramHandle != s_invalidShaderProgramHandle )
    {
        glDeleteProgram( m_shaderProgramHandle );
        LOG_GL_ERROR();
        m_shaderProgramHandle = s_invalidShaderProgramHandle;
    }
}

bool GLWidget::isOpenGLVersion330CoreProfile() const
{
    const QGLFormat openGLFormat = this->context()->format();
    return openGLFormat.majorVersion() >= 3
           && openGLFormat.minorVersion() >= 3
           && openGLFormat.profile() == QGLFormat::CoreProfile;
}

QString GLWidget::shaderVariableTypeDescription( const GLenum shaderVariableType )
{
    // https://www.opengl.org/sdk/docs/man4/html/glGetActiveUniform.xhtml
    switch ( shaderVariableType )
    {
        case GL_FLOAT: return "float";
        case GL_FLOAT_VEC2: return "vec2";
        case GL_FLOAT_VEC3: return "vec3";
        case GL_FLOAT_VEC4: return "vec4";
        case GL_DOUBLE: return "double";
        case GL_DOUBLE_VEC2: return "dvec2";
        case GL_DOUBLE_VEC3: return "dvec3";
        case GL_DOUBLE_VEC4: return "dvec4";
        case GL_INT: return "int";
        case GL_INT_VEC2: return "ivec2";
        case GL_INT_VEC3: return "ivec3";
        case GL_INT_VEC4: return "ivec4";
        case GL_UNSIGNED_INT: return "unsigned int";
        case GL_UNSIGNED_INT_VEC2: return "uvec2";
        case GL_UNSIGNED_INT_VEC3: return "uvec3";
        case GL_UNSIGNED_INT_VEC4: return "uvec4";
        case GL_BOOL: return "bool";
        case GL_BOOL_VEC2: return "bvec2";
        case GL_BOOL_VEC3: return "bvec3";
        case GL_BOOL_VEC4: return "bvec4";
        case GL_FLOAT_MAT2: return "mat2";
        case GL_FLOAT_MAT3: return "mat3";
        case GL_FLOAT_MAT4: return "mat4";
        case GL_FLOAT_MAT2x3: return "mat2x3";
        case GL_FLOAT_MAT2x4: return "mat2x4";
        case GL_FLOAT_MAT3x2: return "mat3x2";
        case GL_FLOAT_MAT3x4: return "mat3x4";
        case GL_FLOAT_MAT4x2: return "mat4x2";
        case GL_FLOAT_MAT4x3: return "mat4x3";
        case GL_DOUBLE_MAT2: return "dmat2";
        case GL_DOUBLE_MAT3: return "dmat3";
        case GL_DOUBLE_MAT4: return "dmat4";
        case GL_DOUBLE_MAT2x3: return "dmat2x3";
        case GL_DOUBLE_MAT2x4: return "dmat2x4";
        case GL_DOUBLE_MAT3x2: return "dmat3x2";
        case GL_DOUBLE_MAT3x4: return "dmat3x4";
        case GL_DOUBLE_MAT4x2: return "dmat4x2";
        case GL_DOUBLE_MAT4x3: return "dmat4x3";
        case GL_SAMPLER_1D: return "sampler1D";
        case GL_SAMPLER_2D: return "sampler2D";
        case GL_SAMPLER_3D: return "sampler3D";
        case GL_SAMPLER_CUBE: return "samplerCube";
        case GL_SAMPLER_1D_SHADOW: return "sampler1DShadow";
        case GL_SAMPLER_2D_SHADOW: return "sampler2DShadow";
        case GL_SAMPLER_1D_ARRAY: return "sampler1DArray";
        case GL_SAMPLER_2D_ARRAY: return "sampler2DArray";
        case GL_SAMPLER_1D_ARRAY_SHADOW: return "sampler1DArrayShadow";
        case GL_SAMPLER_2D_ARRAY_SHADOW: return "sampler2DArrayShadow";
        case GL_SAMPLER_2D_MULTISAMPLE: return "sampler2DMS";
        case GL_SAMPLER_2D_MULTISAMPLE_ARRAY: return "sampler2DMSArray";
        case GL_SAMPLER_CUBE_SHADOW: return "samplerCubeShadow";
        case GL_SAMPLER_BUFFER: return "samplerBuffer";
        case GL_SAMPLER_2D_RECT: return "sampler2DRect";
        case GL_SAMPLER_2D_RECT_SHADOW: return "sampler2DRectShadow";
        case GL_INT_SAMPLER_1D: return "isampler1D";
        case GL_INT_SAMPLER_2D: return "isampler2D";
        case GL_INT_SAMPLER_3D: return "isampler3D";
        case GL_INT_SAMPLER_CUBE: return "isamplerCube";
        case GL_INT_SAMPLER_1D_ARRAY: return "isampler1DArray";
        case GL_INT_SAMPLER_2D_ARRAY: return "isampler2DArray";
        case GL_INT_SAMPLER_2D_MULTISAMPLE: return "isampler2DMS";
        case GL_INT_SAMPLER_2D_MULTISAMPLE_ARRAY: return "isampler2DMSArray";
        case GL_INT_SAMPLER_BUFFER: return "isamplerBuffer";
        case GL_INT_SAMPLER_2D_RECT: return "isampler2DRect";
        case GL_UNSIGNED_INT_SAMPLER_1D: return "usampler1D";
        case GL_UNSIGNED_INT_SAMPLER_2D: return "usampler2D";
        case GL_UNSIGNED_INT_SAMPLER_3D: return "usampler3D";
        case GL_UNSIGNED_INT_SAMPLER_CUBE: return "usamplerCube";
        case GL_UNSIGNED_INT_SAMPLER_1D_ARRAY: return "usampler2DArray";
        case GL_UNSIGNED_INT_SAMPLER_2D_ARRAY: return "usampler2DArray";
        case GL_UNSIGNED_INT_SAMPLER_2D_MULTISAMPLE: return "usampler2DMS";
        case GL_UNSIGNED_INT_SAMPLER_2D_MULTISAMPLE_ARRAY: return "usampler2DMSArray";
        case GL_UNSIGNED_INT_SAMPLER_BUFFER: return "usamplerBuffer";
        case GL_UNSIGNED_INT_SAMPLER_2D_RECT: return "usampler2DRect";
        case GL_IMAGE_1D: return "image1D";
        case GL_IMAGE_2D: return "image2D";
        case GL_IMAGE_3D: return "image3D";
        case GL_IMAGE_2D_RECT: return "image2DRect";
        case GL_IMAGE_CUBE: return "imageCube";
        case GL_IMAGE_BUFFER: return "imageBuffer";
        case GL_IMAGE_1D_ARRAY: return "image1DArray";
        case GL_IMAGE_2D_ARRAY: return "image2DArray";
        case GL_IMAGE_2D_MULTISAMPLE: return "image2DMS";
        case GL_IMAGE_2D_MULTISAMPLE_ARRAY: return "image2DMSArray";
        case GL_INT_IMAGE_1D: return "iimage1D";
        case GL_INT_IMAGE_2D: return "iimage2D";
        case GL_INT_IMAGE_3D: return "iimage3D";
        case GL_INT_IMAGE_2D_RECT: return "iimage2DRect";
        case GL_INT_IMAGE_CUBE: return "iimageCube";
        case GL_INT_IMAGE_BUFFER: return "iimageBuffer";
        case GL_INT_IMAGE_1D_ARRAY: return "iimage1DArray";
        case GL_INT_IMAGE_2D_ARRAY: return "iimage2DArray";
        case GL_INT_IMAGE_2D_MULTISAMPLE: return "iimage2DMS";
        case GL_INT_IMAGE_2D_MULTISAMPLE_ARRAY: return "iimage2DMSArray";
        case GL_UNSIGNED_INT_IMAGE_1D: return "uimage1D";
        case GL_UNSIGNED_INT_IMAGE_2D: return "uimage2D";
        case GL_UNSIGNED_INT_IMAGE_3D: return "uimage3D";
        case GL_UNSIGNED_INT_IMAGE_2D_RECT: return "uimage2DRect";
        case GL_UNSIGNED_INT_IMAGE_CUBE: return "uimageCube";
        case GL_UNSIGNED_INT_IMAGE_BUFFER: return "uimageBuffer";
        case GL_UNSIGNED_INT_IMAGE_1D_ARRAY: return "uimage1DArray";
        case GL_UNSIGNED_INT_IMAGE_2D_ARRAY: return "uimage2DArray";
        case GL_UNSIGNED_INT_IMAGE_2D_MULTISAMPLE: return "uimage2DMS";
        case GL_UNSIGNED_INT_IMAGE_2D_MULTISAMPLE_ARRAY: return "uimage2DMSArray";
        case GL_UNSIGNED_INT_ATOMIC_COUNTER: return "atomic_uint";
        default:
            break;
    }

    return QString();
}

void GLWidget::logGlError( const QString& filePath, const int lineNumber )
{
    const GLenum openGlErrorCode = glGetError();
    if ( openGlErrorCode == GL_NO_ERROR )
    {
        return;
    }

    QString errorDescription;
    switch ( openGlErrorCode )
    {
        case GL_INVALID_ENUM:
            errorDescription = QT_STRINGIFY2( GL_INVALID_ENUM );
            break;
        case GL_INVALID_VALUE:
            errorDescription = QT_STRINGIFY2( GL_INVALID_VALUE );
            break;
        case GL_INVALID_OPERATION:
            errorDescription = QT_STRINGIFY2( GL_INVALID_OPERATION );
            break;
        case GL_STACK_OVERFLOW:
            errorDescription = QT_STRINGIFY2( GL_STACK_OVERFLOW );
            break;
        case GL_STACK_UNDERFLOW:
            errorDescription = QT_STRINGIFY2( GL_STACK_UNDERFLOW );
            break;
        case GL_OUT_OF_MEMORY:
            errorDescription = QT_STRINGIFY2( GL_OUT_OF_MEMORY );
            break;
        default:
            break;
    }

    qDebug() << "OpenGL error:" << endl
             << "\tfile:" << filePath << endl
             << "\tline:" << lineNumber << endl
             << "\tcode:" << QString::number( openGlErrorCode, 16 ) << endl
             << "\tdescription:" << errorDescription;
}

void GLWidget::bindTexturesForVaoIndex( const CommonEntities::VAOIndex vaoIndex ) const
{
    const QList< CommonEntities::TextureIndex > textureIndices = CommonEntities::textureIndicesFromVaoIndex( vaoIndex );
    const int textureIndicesCount = textureIndices.size();
    for ( int i = 0; i < textureIndicesCount; ++ i )
    {
        this->bindTextureForTextureIndex( textureIndices[ i ] );
    }
}

bool GLWidget::bindTextureForTextureIndex( const CommonEntities::TextureIndex textureIndex ) const
{
    const CommonEntities::TextureUnitIndex textureUnitIndex = CommonEntities::textureUnitIndexFromTextureIndex( textureIndex );
    if ( ! CommonEntities::isTextureUnitIndexValid( textureUnitIndex ) )
    {
        return false;
    }

    glActiveTexture( CommonEntities::textureUnitDescriptorFromTextureUnitIndex( textureUnitIndex ) );
    LOG_GL_ERROR();

    const int textureUnitsCount = m_textureUnits.size();
    for ( int i = 0;
          i < textureUnitsCount;
          ++ i )
    {
        const TextureUnit& textureUnit = m_textureUnits[ i ];
        if ( textureUnit.index != i )
        {
            continue;
        }

        const QList< Texture >& textures = textureUnit.textures;
        const int texturesCount = textures.size();
        for ( int j = 0;
              j < texturesCount;
              ++ j )
        {
            const Texture& texture = textures[ j ];
            if ( texture.index != textureIndex )
            {
                continue;
            }

            glBindTexture( GL_TEXTURE_2D, texture.name );
            LOG_GL_ERROR();

            return true;
        }
    }

    return false;
}

void GLWidget::axesFromTriangles(
        QVector< GLfloat >& verticesAxes,
        QVector< GLfloat >& vertexNormalsAxes,
        QVector< GLfloat >& vertexTangentsAxes,
        QVector< GLfloat >& vertexTextureCoordsAxes,
        const QList< QPair< Triangle3, Triangle2 > >& triangles,
        const QList< glm::vec3 >& uniqueVertices,
        const QList< glm::vec3 >& uniqueVertexNormals,
        const QList< glm::vec4 >& uniqueVertexTangents )
{
    verticesAxes.clear();
    vertexNormalsAxes.clear();
    vertexTangentsAxes.clear();
    vertexTextureCoordsAxes.clear();

    const int uniqueVerticesCount       = uniqueVertices.size();
    const int uniqueVertexNormalsCount  = uniqueVertexNormals.size();
    const int uniqueVertexTangentsCount = uniqueVertexTangents.size();
    if ( uniqueVerticesCount != uniqueVertexNormalsCount
         || uniqueVerticesCount != uniqueVertexTangentsCount )
    {
        return;
    }

    const int trianglesCount = triangles.size();
    const int axesCount = trianglesCount * Triangle3::VERTICES_COUNT * AXES_COUNT_3D;

    verticesAxes.reserve( axesCount );
    vertexNormalsAxes.reserve( axesCount );
    vertexTangentsAxes.reserve( axesCount );
    vertexTextureCoordsAxes.reserve( trianglesCount * Triangle3::VERTICES_COUNT * AXES_COUNT_2D );

    for ( int i = 0; i < trianglesCount; ++ i )
    {
        const QPair< Triangle3, Triangle2 >& triangle                   = triangles[ i ];
        const QList< glm::vec3 >&            triangleVertices           = triangle.first.vertices();
        const QList< glm::vec2 >&            triangleTextureCoordinates = triangle.second.vertices();
        const int                            triangleVerticesCount      = triangleVertices.size();
        for ( int j = 0; j < triangleVerticesCount; ++ j )
        {
            const glm::vec3& triangleVertex = triangleVertices[ j ];
            const int triangleUniqueVertexIndex = MathUtilities::vertexIndex( uniqueVertices, triangleVertex );
            if ( triangleUniqueVertexIndex < 0 )
            {
                qDebug() << "normal not found";
                continue;
            }

            verticesAxes << triangleVertex.x
                         << triangleVertex.y
                         << triangleVertex.z;

            const glm::vec2& triangleTextureCoordinate = triangleTextureCoordinates[ j ];
            vertexTextureCoordsAxes << triangleTextureCoordinate.x
                                    << triangleTextureCoordinate.y;

            const glm::vec3& triangleVertexNormal = uniqueVertexNormals[ triangleUniqueVertexIndex ];
            vertexNormalsAxes << triangleVertexNormal.x
                              << triangleVertexNormal.y
                              << triangleVertexNormal.z;

            const glm::vec4& triangleVertexTangent = uniqueVertexTangents[ triangleUniqueVertexIndex ];
            vertexTangentsAxes << triangleVertexTangent.x
                               << triangleVertexTangent.y
                               << triangleVertexTangent.z
                               << triangleVertexTangent.w;
        }
    }
}

void GLWidget::applyGraphicsData(
        const GraphicsData& aGraphicsData,
        const bool force,
        const bool shouldUpdateGL )
{
    if ( m_shaderProgramHandle == s_invalidShaderProgramHandle )
    {
        return;
    }

    const GLuint activeShaderProgramHandle = GLWidget::activeShaderProgramHandle();
    const bool changeActiveShaderProgramHandle = activeShaderProgramHandle != m_shaderProgramHandle;
    if ( changeActiveShaderProgramHandle )
    {
        glUseProgram( m_shaderProgramHandle );
        LOG_GL_ERROR();
    }

    bool isDataApplied = false;
    if ( force || aGraphicsData.ambientColor != m_graphicsData.ambientColor )
    {
        const GLint uniformVarLocation = m_uniformVars[ UniformVarIndexAmbientColor ].first;
        if ( CommonEntities::isUniformVarLocationValid( uniformVarLocation ) )
        {
            const glm::vec3 ambientColor = ImageUtilities::realFromIntColor( aGraphicsData.ambientColor );
            glUniform3fv( uniformVarLocation, 1, glm::value_ptr( ambientColor ) );
            LOG_GL_ERROR();

            isDataApplied = true;
        }
    }

    if ( force || aGraphicsData.lightColor != m_graphicsData.lightColor )
    {
        const GLint uniformVarLocation = m_uniformVars[ UniformVarIndexLightColor ].first;
        if ( CommonEntities::isUniformVarLocationValid( uniformVarLocation ) )
        {
            const glm::vec3 lightColor = ImageUtilities::realFromIntColor( aGraphicsData.lightColor );
            glUniform3fv( uniformVarLocation, 1, glm::value_ptr( lightColor ) );
            LOG_GL_ERROR();

            isDataApplied = true;
        }
    }

    if ( force || ! MathUtilities::epsilonEqual( aGraphicsData.lightPosition, m_graphicsData.lightPosition ) )
    {
        const GLint uniformVarLocation = m_uniformVars[ UniformVarIndexLightPos ].first;
        if ( CommonEntities::isUniformVarLocationValid( uniformVarLocation ) )
        {
            const glm::vec3& lightPos = aGraphicsData.lightPosition;
            glUniform3fv( uniformVarLocation, 1, glm::value_ptr( lightPos ) );
            LOG_GL_ERROR();

            isDataApplied = true;
        }
    }

    if ( force || ! MathUtilities::epsilonEqual( aGraphicsData.eyePosition, m_graphicsData.eyePosition ) )
    {
        const GLint uniformVarLocation = m_uniformVars[ UniformVarIndexEyePos ].first;
        if ( CommonEntities::isUniformVarLocationValid( uniformVarLocation ) )
        {
            const glm::vec3& eyePos = aGraphicsData.eyePosition;
            glUniform3fv( uniformVarLocation, 1, glm::value_ptr( eyePos ) );
            LOG_GL_ERROR();

            isDataApplied = true;
        }
    }

    if ( force || ! qFuzzyCompare( aGraphicsData.materialShininessLevel, m_graphicsData.materialShininessLevel ) )
    {
        const GLint uniformVarLocation = m_uniformVars[ UniformVarIndexMaterialShininessLevel ].first;
        if ( CommonEntities::isUniformVarLocationValid( uniformVarLocation ) )
        {
            glUniform1f( uniformVarLocation, static_cast< float > ( aGraphicsData.materialShininessLevel ) );
            LOG_GL_ERROR();

            isDataApplied = true;
        }
    }

    if ( force || ! qFuzzyCompare( aGraphicsData.lightIntensityLevel, m_graphicsData.lightIntensityLevel ) )
    {
        const GLint uniformVarLocation = m_uniformVars[ UniformVarIndexLightIntensityLevel ].first;
        if ( CommonEntities::isUniformVarLocationValid( uniformVarLocation ) )
        {
            glUniform1f( uniformVarLocation, static_cast< float > ( aGraphicsData.lightIntensityLevel ) );
            LOG_GL_ERROR();

            isDataApplied = true;
        }
    }

    for ( int i = 0; i < GraphicsData::PartTypeIndicesCount; ++ i )
    {
        const QString& textureFilePath = aGraphicsData.textureFilePaths[ i ];
        if ( force || textureFilePath != m_graphicsData.textureFilePaths[ i ] )
        {
            this->setTextureFilePath( static_cast< GraphicsData::PartTypeIndex >( i ), textureFilePath );
        }
    }

    if ( force || aGraphicsData.effectType != m_graphicsData.effectType )
    {
        isDataApplied = this->applyEffectType( aGraphicsData.effectType, force, false );
    }

    if ( changeActiveShaderProgramHandle )
    {
        glUseProgram( activeShaderProgramHandle );
        LOG_GL_ERROR();
    }

    if ( shouldUpdateGL && isDataApplied )
    {
        this->updateGL();
    }
}

bool GLWidget::applyEffectType(
        const GraphicsData::EffectType aEffectType,
        const bool force,
        const bool shouldUpdateGL )
{
    if ( m_shaderProgramHandle == s_invalidShaderProgramHandle
         || ! GraphicsData::isEffectTypeValid( aEffectType )
         || ( ! force && aEffectType == m_graphicsData.effectType ) )
    {
        return false;
    }

    const GLuint activeShaderProgramHandle = GLWidget::activeShaderProgramHandle();
    const bool changeActiveShaderProgramHandle = activeShaderProgramHandle != m_shaderProgramHandle;
    if ( changeActiveShaderProgramHandle )
    {
        glUseProgram( m_shaderProgramHandle );
        LOG_GL_ERROR();
    }

    bool isDataApplied = false;
    const GLint uniformVarLocation = m_uniformVars[ UniformVarIndexEffectType ].first;
    if ( CommonEntities::isUniformVarLocationValid( uniformVarLocation ) )
    {
        glUniform1i( uniformVarLocation, static_cast< int > ( aEffectType ) );
        LOG_GL_ERROR();

        isDataApplied = true;
    }

    if ( changeActiveShaderProgramHandle )
    {
        glUseProgram( activeShaderProgramHandle );
        LOG_GL_ERROR();
    }

    if ( shouldUpdateGL && isDataApplied )
    {
        this->updateGL();
    }

    return isDataApplied;
}

void GLWidget::applyMatrices( const bool shouldUpdateGL )
{
    if ( m_shaderProgramHandle == s_invalidShaderProgramHandle )
    {
        return;
    }

    const glm::mat4 modelMatrix      = m_matricesData[ MatrixDataIndexModel ].completeMatrix();
    const glm::mat3 normalMatrix     = glm::transpose( glm::inverse( glm::mat3( modelMatrix ) ) );
    const glm::mat4 viewMatrix       = m_matricesData[ MatrixDataIndexView ].completeMatrix();
    const glm::mat4 projectionMatrix( 1.0f );

    const QPair< UniformVarIndex, const GLfloat* > uniformVarIndexRawMatrices[] =
    {
        QPair< UniformVarIndex, const GLfloat* >( UniformVarIndexModelMatrix,      glm::value_ptr( modelMatrix ) ),
        QPair< UniformVarIndex, const GLfloat* >( UniformVarIndexNormalMatrix,     glm::value_ptr( normalMatrix ) ),
        QPair< UniformVarIndex, const GLfloat* >( UniformVarIndexViewMatrix,       glm::value_ptr( viewMatrix ) ),
        QPair< UniformVarIndex, const GLfloat* >( UniformVarIndexProjectionMatrix, glm::value_ptr( projectionMatrix ) )
    };

    const GLuint activeShaderProgramHandle = GLWidget::activeShaderProgramHandle();
    const bool changeActiveShaderProgramHandle = activeShaderProgramHandle != m_shaderProgramHandle;
    if ( changeActiveShaderProgramHandle )
    {
        glUseProgram( m_shaderProgramHandle );
        LOG_GL_ERROR();
    }

    const int uniformVarIndexRawMatricesCount = sizeof( uniformVarIndexRawMatrices ) / sizeof( QPair< UniformVarIndex, const GLfloat* > );
    for ( int i = 0; i < uniformVarIndexRawMatricesCount; ++ i )
    {
        const QPair< UniformVarIndex, const GLfloat* >& uniformVarIndexRawMatrix = uniformVarIndexRawMatrices[ i ];

        const UniformVarIndex matrixUniforVarIndex = uniformVarIndexRawMatrix.first;
        const GLint matrixUniformVarLocation = m_uniformVars[ matrixUniforVarIndex ].first;
        if ( CommonEntities::isUniformVarLocationValid( matrixUniformVarLocation ) )
        {
            ( matrixUniforVarIndex == UniformVarIndexNormalMatrix
              ? glUniformMatrix3fv
              : glUniformMatrix4fv )( matrixUniformVarLocation, 1, GL_FALSE, uniformVarIndexRawMatrix.second );
            LOG_GL_ERROR();
        }
    }

    if ( changeActiveShaderProgramHandle )
    {
        glUseProgram( activeShaderProgramHandle );
        LOG_GL_ERROR();
    }

    if ( shouldUpdateGL )
    {
        this->updateGL();
    }
}

QString GLWidget::uniformVarName( const GLWidget::UniformVarIndex uniformVarIndex )
{
    switch ( uniformVarIndex )
    {
        case UniformVarIndexModelMatrix:
            return "modelMatrix";
        case UniformVarIndexNormalMatrix:
            return "normalMatrix";
        case UniformVarIndexViewMatrix:
            return "viewMatrix";
        case UniformVarIndexProjectionMatrix:
            return "projectionMatrix";
        case UniformVarIndexAmbientColor:
            return "ambientColor";
        case UniformVarIndexLightColor:
            return "lightColor";
        case UniformVarIndexLightPos:
            return "lightPos";
        case UniformVarIndexEyePos:
            return "eyePos";
        case UniformVarIndexMaterialShininessLevel:
            return "materialShininessLevel";
        case UniformVarIndexLightIntensityLevel:
            return "lightIntensityLevel";
        case UniformVarIndexEffectType:
            return "effectType";
        default:
            break;
    }

    return QString();
}

void GLWidget::enableGenericVertexAttributeArrays( const bool enable )
{
    foreach ( const GenericVertexAttributeIndex genericVertexAttributeIndex, ALL_ORDERED_GENERIC_VERTEX_ATTRIBUTE_INDICES )
    {
        if ( enable )
        {
            glEnableVertexAttribArray( genericVertexAttributeIndex );
            LOG_GL_ERROR();
        }
        else
        {
            glDisableVertexAttribArray( genericVertexAttributeIndex );
            LOG_GL_ERROR();
        }
    }
}

GLuint GLWidget::activeShaderProgramHandle()
{
    // http://cu-droplet.googlecode.com/git-history/5dd30c9749a595441098aa1d448445075b6f363d/DropletSimDemos/DropletRenderer/src/RenderableDroplet.cpp
    // OpenGL 3.3:  GL_CURRENT_PROGRAM
    // OpenGL 4.2+: GL_ACTIVE_PROGRAM

    GLint activeShaderProgramHandle = static_cast< GLint > ( s_invalidShaderProgramHandle );
    glGetIntegerv( GL_CURRENT_PROGRAM, &activeShaderProgramHandle );
    LOG_GL_ERROR();

    return static_cast< GLuint > ( activeShaderProgramHandle );
}

const QHash< Qt::Key, QPair< glm::vec3, GLfloat > >& GLWidget::arrowKeysMatrixDeltaChanges()
{
    static QHash< Qt::Key, QPair< glm::vec3, GLfloat > > arrowKeysMatrixDeltaChangesContainer;
    if ( arrowKeysMatrixDeltaChangesContainer.isEmpty() )
    {
        static const GLfloat DELTA_ANGLE_DEGREES = 5.0f;
        arrowKeysMatrixDeltaChangesContainer.insert( Qt::Key_Left,  QPair< glm::vec3, GLfloat >( s_yAxisDir, -DELTA_ANGLE_DEGREES ) );
        arrowKeysMatrixDeltaChangesContainer.insert( Qt::Key_Right, QPair< glm::vec3, GLfloat >( s_yAxisDir, DELTA_ANGLE_DEGREES ) );
        arrowKeysMatrixDeltaChangesContainer.insert( Qt::Key_Up,    QPair< glm::vec3, GLfloat >( s_xAxisDir, -DELTA_ANGLE_DEGREES ) );
        arrowKeysMatrixDeltaChangesContainer.insert( Qt::Key_Down,  QPair< glm::vec3, GLfloat >( s_xAxisDir, DELTA_ANGLE_DEGREES ) );
    }

    return arrowKeysMatrixDeltaChangesContainer;
}

void GLWidget::setTexture(
        const CommonEntities::TextureIndex textureIndex,
        const QImage& textureImage )
{
    if ( ! CommonEntities::isTextureIndexValid( textureIndex )
         || m_shaderProgramHandle == s_invalidShaderProgramHandle
         || textureImage.isNull() )
    {
        return;
    }

    const GLuint activeShaderProgramHandle = GLWidget::activeShaderProgramHandle();
    const bool changeActiveShaderProgramHandle = activeShaderProgramHandle != m_shaderProgramHandle;
    if ( changeActiveShaderProgramHandle )
    {
        glUseProgram( m_shaderProgramHandle );
        LOG_GL_ERROR();
    }

    const QImage openglReadyImage = QGLWidget::convertToGLFormat( textureImage );
    if ( openglReadyImage.isNull() )
    {
        return;
    }

    if ( ! this->bindTextureForTextureIndex( textureIndex ) )
    {
        return;
    }

    /* Loading a texture is only the first step toward getting a texture applied to geometry. At a
     * minimum we must also supply texture coordinates and set up the texture coordinate wrap
     * modes and texture filter. Finally, we may choose to mipmap our textures to improve
     * texturing performance and/or visual quality. Of course in all of this, we are assuming our
     * shaders are doing the "right thing."
     *
     * The process of calculating color fragments from a stretched or shrunken texture map is
     * called texture filtering.
     */

    glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE );
    LOG_GL_ERROR();
    glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE );
    LOG_GL_ERROR();

    glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR );
    LOG_GL_ERROR();
    glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR );
    LOG_GL_ERROR();

    glPixelStorei( GL_UNPACK_ALIGNMENT, 1 );
    LOG_GL_ERROR();
    glTexImage2D( GL_TEXTURE_2D, 0, GL_RGBA,
                  openglReadyImage.width(),
                  openglReadyImage.height(),
                  0, GL_RGBA, GL_UNSIGNED_BYTE,
                  openglReadyImage.bits() );
    LOG_GL_ERROR();

    glGenerateMipmap( GL_TEXTURE_2D );
    LOG_GL_ERROR();

    glBindTexture( GL_TEXTURE_2D, 0 );
    LOG_GL_ERROR();

    glActiveTexture( GL_TEXTURE0 );
    LOG_GL_ERROR();

    if ( changeActiveShaderProgramHandle )
    {
        glUseProgram( activeShaderProgramHandle );
        LOG_GL_ERROR();
    }

    this->updateGL();
}

glm::mat4 GLWidget::MatrixData::completeMatrix() const
{
    return mouseMatrix
           * keyMatrix
           * cacheMatrix;
}
