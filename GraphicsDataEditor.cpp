#include "GraphicsDataEditor.h"

#include "ColorPicker.h"
#include "CommonEntities.h"
#include "FilePathPicker.h"
#include "Vec3Editor.h"

#include <QButtonGroup>
#include <QDoubleSpinBox>
#include <QGridLayout>
#include <QGroupBox>
#include <QLabel>
#include <QPushButton>
#include <QRadioButton>
#include <QSlider>

GraphicsDataEditor::GraphicsDataEditor( QWidget* parent )
    : QWidget( parent ),
      m_ambientColorLabel( 0 ),
      m_ambientColorPicker( 0 ),
      m_lightColorLabel( 0 ),
      m_lightColorPicker( 0 ),
      m_lightPositionLabel( 0 ),
      m_lightPositionEditor( 0 ),
      m_materialShininessLabel( 0 ),
      m_materialShininessLevelEditor( 0 ),
      m_lightIntensityLevelLabel( 0 ),
      m_lightIntensityLevelEditor( 0 ),
      m_eyePositionLabel( 0 ),
      m_eyePositionEditor( 0 ),
      m_bumpSizeLabel( 0 ),
      m_bumpSizeEditor( 0 ),
      m_effectTypeSelectionGroupBox( 0 ),
      m_effectTypeSelectorsGroup( 0 ),
      m_phongAveragedSurfaceNormalsSelector( 0 ),
      m_phongNormalMappingSelector( 0 )
{
    m_ambientColorLabel            = new QLabel;
    m_ambientColorPicker           = new ColorPicker;

    m_lightColorLabel              = new QLabel;
    m_lightColorPicker             = new ColorPicker;

    m_lightPositionLabel           = new QLabel;
    m_lightPositionEditor          = new Vec3Editor;
    GraphicsDataEditor::configurePositionEditor( m_lightPositionEditor );

    m_materialShininessLabel       = new QLabel;
    m_materialShininessLevelEditor = new QDoubleSpinBox;
    m_materialShininessLevelEditor->setDecimals( 2 );
    m_materialShininessLevelEditor->setRange( GraphicsData::MIN_MATERIAL_SHININESS_LEVEL,
                                              GraphicsData::MAX_MATERIAL_SHININESS_LEVEL );

    m_lightIntensityLevelLabel     = new QLabel;
    m_lightIntensityLevelEditor    = new QDoubleSpinBox;
    m_lightIntensityLevelEditor->setDecimals( 2 );
    m_lightIntensityLevelEditor->setMinimum( GraphicsData::MIN_LIGHT_INTENSITY_LEVEL );

    m_eyePositionLabel             = new QLabel;
    m_eyePositionEditor            = new Vec3Editor;
    GraphicsDataEditor::configurePositionEditor( m_eyePositionEditor );

    m_bumpSizeLabel = new QLabel;
    m_bumpSizeEditor = new QSlider( Qt::Horizontal );
    m_bumpSizeEditor->setRange( -10, 10 );
    connect( m_bumpSizeEditor,
             SIGNAL(valueChanged(int)),
             SLOT(onBumpSizeChanged(int)) );
    connect( m_bumpSizeEditor,
             SIGNAL(sliderReleased()),
             SLOT(onBumpSizeEdited()) );

    m_phongAveragedSurfaceNormalsSelector = new QRadioButton;
    m_phongNormalMappingSelector = new QRadioButton;

    m_effectTypeSelectorsGroup = new QButtonGroup( this );
    connect( m_effectTypeSelectorsGroup,
             SIGNAL(buttonClicked(QAbstractButton*)),
             SLOT(notifyEffectTypeEdited()) );

    m_effectTypeSelectorsGroup->addButton( m_phongAveragedSurfaceNormalsSelector );
    m_effectTypeSelectorsGroup->addButton( m_phongNormalMappingSelector );

    m_effectTypeSelectionGroupBox = new QGroupBox;
    QVBoxLayout* const effectTypeSelectionGroupBoxLayout = new QVBoxLayout( m_effectTypeSelectionGroupBox );
    effectTypeSelectionGroupBoxLayout->addWidget( m_phongAveragedSurfaceNormalsSelector );
    effectTypeSelectionGroupBoxLayout->addWidget( m_phongNormalMappingSelector );
    this->setUiEffectType( GraphicsData::EffectTypePhongIlluminationAveragedSurfaceNormals );

    QGridLayout* const mainLayout = new QGridLayout( this );
    mainLayout->addWidget( m_ambientColorLabel           , 0, 0 );
    mainLayout->addWidget( m_ambientColorPicker          , 0, 1 );
    mainLayout->addWidget( m_lightColorLabel             , 1, 0 );
    mainLayout->addWidget( m_lightColorPicker            , 1, 1 );
    mainLayout->addWidget( m_lightPositionLabel          , 2, 0 );
    mainLayout->addWidget( m_lightPositionEditor         , 2, 1 );
    mainLayout->addWidget( m_materialShininessLabel      , 3, 0 );
    mainLayout->addWidget( m_materialShininessLevelEditor, 3, 1 );
    mainLayout->addWidget( m_lightIntensityLevelLabel    , 4, 0 );
    mainLayout->addWidget( m_lightIntensityLevelEditor   , 4, 1 );
    mainLayout->addWidget( m_eyePositionLabel            , 5, 0 );
    mainLayout->addWidget( m_eyePositionEditor           , 5, 1 );

    m_textureFilePathPickers.reserve( GraphicsData::PartTypeIndicesCount );
    for ( int i = 0; i < GraphicsData::PartTypeIndicesCount; ++ i )
    {
        FilePathPicker* const textureFilePathPicker = new FilePathPicker;
        CommonEntities::TextureIndex textureIndex = CommonEntities::TextureIndexInvalid;
        switch ( i )
        {
            case GraphicsData::PartTypeIndexFloor:
                textureIndex = CommonEntities::TextureIndexFloorColor;
                break;
            case GraphicsData::PartTypeIndexWalls:
                textureIndex = CommonEntities::TextureIndexWallsColor;
                break;
            case GraphicsData::PartTypeIndexRoofSides:
                textureIndex = CommonEntities::TextureIndexRoofSidesColor;
                break;
            default:
                break;
        }

        if ( CommonEntities::isTextureIndexValid( textureIndex ) )
        {
            textureFilePathPicker->setFilePath( CommonEntities::defaultTextureFilePathFromTextureIndex( textureIndex ) );
        }

        textureFilePathPicker->layout()->setMargin( 0 );
        connect( textureFilePathPicker,
                 SIGNAL(filePathPicked(QString)),
                 SLOT(onTextureFilePathChosen(QString)) );
        m_textureFilePathPickers.append( textureFilePathPicker );
        mainLayout->addWidget( textureFilePathPicker, 6 + i, 0, 1, 2 );
    }

    QHBoxLayout* const bumpSizeControlsLayout = new QHBoxLayout;
    bumpSizeControlsLayout->addWidget( m_bumpSizeLabel, 0, Qt::AlignLeft );
    bumpSizeControlsLayout->addWidget( m_bumpSizeEditor, 1 );

    mainLayout->addLayout( bumpSizeControlsLayout, 6 + GraphicsData::PartTypeIndicesCount, 0, 1, 2 );
    mainLayout->addWidget( m_effectTypeSelectionGroupBox , 6 + GraphicsData::PartTypeIndicesCount + 1, 0, 1, 2 );

    mainLayout->setColumnStretch( 0, 0 );
    mainLayout->setColumnStretch( 1, 1 );

    this->retranslateUi();
}

GraphicsDataEditor::~GraphicsDataEditor()
{
}

void GraphicsDataEditor::retranslateUi()
{
    m_ambientColorLabel->setText( tr( "Ambient color:" ) );
    m_ambientColorPicker->retranslateUi();

    m_lightColorLabel->setText( tr( "Light color:" ) );
    m_lightColorPicker->retranslateUi();

    m_lightPositionLabel->setText( tr( "Light position:" ) );
    m_lightPositionEditor->retranslateUi();

    m_materialShininessLabel->setText( tr( "Material shininess level:" ) );

    m_lightIntensityLevelLabel->setText( tr( "Light intensity level:" ) );

    m_eyePositionLabel->setText( tr( "Eye position:" ) );
    m_eyePositionEditor->retranslateUi();

    m_bumpSizeLabel->setText( tr( "Bump size:" ) );

    for ( int i = 0; i < GraphicsData::PartTypeIndicesCount; ++ i )
    {
        QString informativeText;
        switch ( i )
        {
            case GraphicsData::PartTypeIndexFloor:
                informativeText = tr( "Floor texture:" );
                break;
            case GraphicsData::PartTypeIndexWalls:
                informativeText = tr( "Walls texture:" );
                break;
            case GraphicsData::PartTypeIndexRoofSides:
                informativeText = tr( "Roof texture:" );
                break;
            default:
                break;
        }

        FilePathPicker* const filePathPicker = m_textureFilePathPickers[ i ];
        filePathPicker->setInformativeText( informativeText );
        filePathPicker->setFileTypesFilter( tr( "Image Files (*.png *.jpg *.bmp)" ) );
    }

    m_effectTypeSelectionGroupBox->setTitle( tr( "Effect Type Selection" ) );
    m_phongAveragedSurfaceNormalsSelector->setText( tr( "Phong Illumination + Averaged Surface Normals" ) );
    m_phongNormalMappingSelector->setText( tr( "Phong Illumination + Normal Mapping" ) );
}

void GraphicsDataEditor::setGraphicsData( const GraphicsData& aGraphicsData )
{
    this->setUiEffectType( aGraphicsData.effectType );

    for ( int i = 0; i < GraphicsData::PartTypeIndicesCount; ++ i )
    {
        m_textureFilePathPickers[ i ]->setFilePath( aGraphicsData.textureFilePaths[ i ] );
    }

    m_bumpSizeEditor->setValue( qRound( aGraphicsData.bumpSize ) );

    m_ambientColorPicker->setColor( aGraphicsData.ambientColor );
    m_lightColorPicker->setColor( aGraphicsData.lightColor );
    m_lightPositionEditor->setVec3( aGraphicsData.lightPosition );
    m_eyePositionEditor->setVec3( aGraphicsData.eyePosition );
    m_materialShininessLevelEditor->setValue( aGraphicsData.materialShininessLevel );
    m_lightIntensityLevelEditor->setValue( aGraphicsData.lightIntensityLevel );
}

GraphicsData GraphicsDataEditor::graphicsData() const
{
    return GraphicsData(
                this->uiEffectType(),
                m_textureFilePathPickers[ GraphicsData::PartTypeIndexFloor     ]->filePath(),
                m_textureFilePathPickers[ GraphicsData::PartTypeIndexWalls     ]->filePath(),
                m_textureFilePathPickers[ GraphicsData::PartTypeIndexRoofSides ]->filePath(),
                m_bumpSizeEditor->value(),
                m_ambientColorPicker->color(),
                m_lightColorPicker->color(),
                m_lightPositionEditor->vec3(),
                m_eyePositionEditor->vec3(),
                m_materialShininessLevelEditor->value(),
                m_lightIntensityLevelEditor->value() );
}

void GraphicsDataEditor::notifyEffectTypeEdited()
{
    const GraphicsData::EffectType effectType = this->uiEffectType();
    emit effectTypeChanged( effectType );
    emit effectTypeEdited( effectType );
}

void GraphicsDataEditor::onTextureFilePathChosen( const QString& aNewTextureFilePath )
{
    FilePathPicker* const textureFilePathPicker = dynamic_cast< FilePathPicker* > ( this->sender() );
    if ( textureFilePathPicker == 0 )
    {
        return;
    }

    const GraphicsData::PartTypeIndex partTypeIndex = static_cast< GraphicsData::PartTypeIndex > ( m_textureFilePathPickers.indexOf( textureFilePathPicker ) );
    if ( ! GraphicsData::isPartTypeIndexValid( partTypeIndex ) )
    {
        return;
    }

    emit textureFilePathEdited( partTypeIndex, aNewTextureFilePath );
}

void GraphicsDataEditor::onBumpSizeChanged( const int aBumpSize )
{
    m_bumpSizeEditor->setToolTip( QString::number( aBumpSize ) );
    emit bumpSizeChanged( static_cast< qreal > ( aBumpSize ) );
}

void GraphicsDataEditor::onBumpSizeEdited()
{
    emit bumpSizeEdited( static_cast< qreal > ( m_bumpSizeEditor->value() ) );
}

void GraphicsDataEditor::configurePositionEditor( Vec3Editor* const positionEditor )
{
    if ( positionEditor == 0 )
    {
        return;
    }

    static const int DEFAULT_DECIMALS_COUNT = 3;
    static const qreal DEFAULT_AXIS_VALUE_MIN = -10.0;
    static const qreal DEFAULT_AXIS_VALUE_MAX = 10.0;

    positionEditor->setDecimalsCount( DEFAULT_DECIMALS_COUNT );
    positionEditor->setRange( DEFAULT_AXIS_VALUE_MIN, DEFAULT_AXIS_VALUE_MAX );
}

void GraphicsDataEditor::setUiEffectType( const GraphicsData::EffectType aUiEffectType )
{
    if ( ! GraphicsData::isEffectTypeValid( aUiEffectType ) )
    {
        return;
    }

    const GraphicsData::EffectType currentEffectType = this->uiEffectType();
    if ( currentEffectType == aUiEffectType )
    {
        return;
    }

    QRadioButton* const effectSelector = GraphicsDataEditor::effectTypeSelectorFromEffectType( aUiEffectType );
    if ( effectSelector == 0 )
    {
        return;
    }

    {
        const QSignalBlocker effectSelectorSignalsBlocker( effectSelector );
        Q_UNUSED( effectSelectorSignalsBlocker )
        effectSelector->click();
    }

    emit effectTypeChanged( aUiEffectType );
}

GraphicsData::EffectType GraphicsDataEditor::uiEffectType() const
{
    return GraphicsDataEditor::effectTypeFromEffectTypeSelector( dynamic_cast< QRadioButton* > ( m_effectTypeSelectorsGroup->checkedButton() ) );
}

GraphicsData::EffectType GraphicsDataEditor::effectTypeFromEffectTypeSelector( QRadioButton* const aEffectTypeSelector ) const
{
    if ( aEffectTypeSelector == m_phongAveragedSurfaceNormalsSelector )
    {
        return GraphicsData::EffectTypePhongIlluminationAveragedSurfaceNormals;
    }
    else if ( aEffectTypeSelector == m_phongNormalMappingSelector )
    {
        return GraphicsData::EffectTypePhongIlluminationNormalMapping;
    }

    return GraphicsData::EffectTypeInvalid;
}

QRadioButton* GraphicsDataEditor::effectTypeSelectorFromEffectType( const GraphicsData::EffectType aEffectType ) const
{
    switch ( aEffectType )
    {
        case GraphicsData::EffectTypePhongIlluminationAveragedSurfaceNormals:
            return m_phongAveragedSurfaceNormalsSelector;
        case GraphicsData::EffectTypePhongIlluminationNormalMapping:
            return m_phongNormalMappingSelector;
        default:
            break;
    }

    return 0;
}
