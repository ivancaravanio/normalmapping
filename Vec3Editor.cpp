#include "Vec3Editor.h"

#include "CommonEntities.h"
#include "Utilities/MathUtilities.h"

#include <QDoubleSpinBox>
#include <QHBoxLayout>
#include <QLabel>

using namespace Utilities;

Vec3Editor::Vec3Editor( QWidget* parent )
    : QWidget( parent )
{
    QHBoxLayout* const mainLayout = new QHBoxLayout( this );
    mainLayout->setMargin( 0 );
    for ( int i = 0; i < AxesCount; ++ i )
    {
        QLabel* const axisLabel = new QLabel;
        m_axisLabels[ i ] = axisLabel;

        QDoubleSpinBox* const axisEditor = new QDoubleSpinBox;
        connect( axisEditor, SIGNAL(valueChanged(double)), SIGNAL(valueChanged()) );
        connect( axisEditor, SIGNAL(valueChanged(double)), SIGNAL(valueEdited()) );
        m_axisEditors[ i ] = axisEditor;

        mainLayout->addWidget( axisLabel, 0 );
        mainLayout->addWidget( axisEditor, 1 );
    }

    this->retranslateUi();
}

Vec3Editor::~Vec3Editor()
{
}

void Vec3Editor::setMinimum( const double aMinimum )
{
    bool notifyValueChanged = false;
    for ( int i = 0; i < AxesCount; ++ i )
    {
        QDoubleSpinBox* const axisEditor = m_axisEditors[ i ];
        const double axisEditorPreviousValue = axisEditor->value();
        axisEditor->blockSignals( true );
        axisEditor->setMinimum( aMinimum );
        axisEditor->blockSignals( false );

        if ( ! qFuzzyCompare( axisEditorPreviousValue, axisEditor->value() ) )
        {
            notifyValueChanged = true;
        }
    }

    if ( notifyValueChanged )
    {
        emit valueChanged();
    }
}

double Vec3Editor::minimum() const
{
    return m_axisEditors[ AxisIndexX ]->minimum();
}

void Vec3Editor::setMaximum( const double aMaximum )
{
    bool notifyValueChanged = false;
    for ( int i = 0; i < AxesCount; ++ i )
    {
        QDoubleSpinBox* const axisEditor = m_axisEditors[ i ];
        const double axisEditorPreviousValue = axisEditor->value();
        axisEditor->blockSignals( true );
        axisEditor->setMaximum( aMaximum );
        axisEditor->blockSignals( false );

        if ( ! qFuzzyCompare( axisEditorPreviousValue, axisEditor->value() ) )
        {
            notifyValueChanged = true;
        }
    }

    if ( notifyValueChanged )
    {
        emit valueChanged();
    }
}

double Vec3Editor::maximum() const
{
    return m_axisEditors[ AxisIndexX ]->maximum();
}

void Vec3Editor::setRange( const double aMinimum, const double aMaximum )
{
    bool notifyValueChanged = false;
    for ( int i = 0; i < AxesCount; ++ i )
    {
        QDoubleSpinBox* const axisEditor = m_axisEditors[ i ];
        const double axisEditorPreviousValue = axisEditor->value();
        axisEditor->blockSignals( true );
        axisEditor->setRange( aMinimum, aMaximum );
        axisEditor->blockSignals( false );

        if ( ! qFuzzyCompare( axisEditorPreviousValue, axisEditor->value() ) )
        {
            notifyValueChanged = true;
        }
    }

    if ( notifyValueChanged )
    {
        emit valueChanged();
    }
}

void Vec3Editor::setDecimalsCount( const int aDecimalsCount )
{
    bool notifyValueChanged = false;
    for ( int i = 0; i < AxesCount; ++ i )
    {
        QDoubleSpinBox* const axisEditor = m_axisEditors[ i ];
        const double axisEditorPreviousValue = axisEditor->value();
        axisEditor->blockSignals( true );
        axisEditor->setDecimals( aDecimalsCount );
        axisEditor->blockSignals( false );

        if ( ! qFuzzyCompare( axisEditorPreviousValue, axisEditor->value() ) )
        {
            notifyValueChanged = true;
        }
    }

    if ( notifyValueChanged )
    {
        emit valueChanged();
    }
}

int Vec3Editor::decimalsCount() const
{
    return m_axisEditors[ AxisIndexX ]->decimals();
}

void Vec3Editor::setVec3( const glm::vec3& aVec3 )
{
    const glm::vec3 previousValue = this->vec3();
    if ( MathUtilities::epsilonEqual( previousValue, aVec3 ) )
    {
        return;
    }

    const glm::vec3::value_type axisValues[] =
    {
        aVec3.x,
        aVec3.y,
        aVec3.z
    };

    for ( int i = 0; i < AxesCount; ++ i )
    {
        QDoubleSpinBox* const axisEditor = m_axisEditors[ i ];
        axisEditor->blockSignals( true );
        axisEditor->setValue( axisValues[ i ] );
        axisEditor->blockSignals( false );
    }

    if ( ! MathUtilities::epsilonEqual( this->vec3(), previousValue ) )
    {
        emit valueChanged();
    }
}

glm::vec3 Vec3Editor::vec3() const
{
    return glm::vec3( m_axisEditors[ AxisIndexX ]->value(),
                      m_axisEditors[ AxisIndexY ]->value(),
                      m_axisEditors[ AxisIndexZ ]->value() );
}

void Vec3Editor::retranslateUi()
{
    for ( int i = 0; i < AxesCount; ++ i )
    {
        QString labelText;
        switch ( i )
        {
            case AxisIndexX:
                labelText = tr( "x:" );
                break;
            case AxisIndexY:
                labelText = tr( "y:" );
                break;
            case AxisIndexZ:
                labelText = tr( "z:" );
                break;
            default:
                break;
        }

        m_axisLabels[ i ]->setText( labelText );
    }
}
