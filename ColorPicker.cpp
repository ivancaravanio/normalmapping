#include "ColorPicker.h"

#include "CommonEntities.h"
#include "Utilities/ImageUtilities.h"

#include <QColorDialog>
#include <QGridLayout>
#include <QLabel>
#include <QLineEdit>
#include <QPushButton>
#include <QRegExpValidator>
#include <QSpinBox>

using namespace Utilities;

ColorPicker::ColorPicker( QWidget* parent )
    : QWidget( parent ),
      m_colorHexEditor( 0 ),
      m_colorDisplay( 0 ),
      m_openColorEditorButton( 0 ),
      m_colorPickerDialog( 0 )
{
    QGridLayout* const mainLayout = new QGridLayout( this );
    mainLayout->setMargin( 0 );

    for ( int i = 0; i < ColorComponentsCount; ++ i )
    {
        QLabel* const colorComponentLabel = new QLabel;
        m_colorComponentsLabels[ i ] = colorComponentLabel;

        QSpinBox* const colorComponentEditor = new QSpinBox;
        colorComponentEditor->setRange( ImageUtilities::intColorComponentMin(),
                                        ImageUtilities::intColorComponentMax() );
        connect( colorComponentEditor, SIGNAL(valueChanged(int)), SLOT(onColorUpdatedFromComponentsEditor()) );

        mainLayout->addWidget( colorComponentLabel, 0, i * 2 );

        const int colorComponentEditorLayoutColumn = i * 2 + 1;
        mainLayout->addWidget( colorComponentEditor, 0, colorComponentEditorLayoutColumn );
        mainLayout->setColumnStretch( colorComponentEditorLayoutColumn, 1 );

        m_colorComponentsEditors[ i ] = colorComponentEditor;
    }

    m_colorHexEditor = new QLineEdit;
    m_colorHexEditor->setValidator( new QRegExpValidator( QRegExp( QString( "^[0-9A-F]{%1}$" ).arg( ColorComponentsCount * 2 ) ), this ) );
    connect( m_colorHexEditor, SIGNAL(textEdited(QString)), SLOT(onColorUpdatedFromHexEditor()) );
    mainLayout->addWidget( m_colorHexEditor, 1, 0, 1, 6 );

    m_colorDisplay = new QLabel;
    m_colorDisplay->setFrameShadow( QLabel::Sunken );
    mainLayout->addWidget( m_colorDisplay, 0, 6 );

    m_openColorEditorButton = new QPushButton;
    connect( m_openColorEditorButton, SIGNAL(clicked()), SLOT(pickColor()) );
    mainLayout->addWidget( m_openColorEditorButton, 1, 6 );

    this->colorToUi( m_color );
    this->retranslateUi();
}

ColorPicker::~ColorPicker()
{
}

void ColorPicker::setColor( const QColor& aColor )
{
    if ( m_color == aColor )
    {
        return;
    }

    m_color = aColor;
    this->colorToUi( aColor );

    emit valueChanged();
}

QString ColorPicker::colorToHexString( const QColor& aColor )
{
    return QString( "%1%2%3" )
           .arg( aColor.red(), 2, 16, QLatin1Char( '0' ) )
           .arg( aColor.green(), 2, 16, QLatin1Char( '0' ) )
           .arg( aColor.blue(), 2, 16, QLatin1Char( '0' ) ).toUpper();
}

void ColorPicker::setColorToComponentsEditor( const QColor& aColor )
{
    for ( int i = 0; i < ColorComponentsCount; ++ i )
    {
        QSpinBox* const colorComponentEditor = m_colorComponentsEditors[ i ];
        colorComponentEditor->blockSignals( true );

        int colorComponentValue = 0;
        switch ( i )
        {
            case ColorComponentIndexRed:
                colorComponentValue = aColor.red();
                break;
            case ColorComponentIndexGreen:
                colorComponentValue = aColor.green();
                break;
            case ColorComponentIndexBlue:
                colorComponentValue = aColor.blue();
                break;
            default:
                break;
        }

        colorComponentEditor->setValue( colorComponentValue );
        colorComponentEditor->blockSignals( false );
    }

    m_colorComponentsEditors[ ColorComponentIndexGreen ]->setValue( aColor.green() );
    m_colorComponentsEditors[ ColorComponentIndexBlue ]->setValue( aColor.blue() );
}

QColor ColorPicker::colorFromComponentsEditor() const
{
    return QColor( m_colorComponentsEditors[ ColorComponentIndexRed ]->value(),
                   m_colorComponentsEditors[ ColorComponentIndexGreen ]->value(),
                   m_colorComponentsEditors[ ColorComponentIndexBlue ]->value() );
}

void ColorPicker::setColorToHexEditor( const QColor& aColor )
{
    m_colorHexEditor->setText( ColorPicker::colorToHexString( aColor ) );
}

QColor ColorPicker::colorFromHexEditor() const
{
    bool converted = false;
    const QRgb color = m_colorHexEditor->text().toUInt( & converted, 16 );
    if ( ! converted )
    {
        return QColor();
    }

    return QColor( qRed( color ),
                   qGreen( color ),
                   qBlue( color ) );
}

void ColorPicker::setColorToPicker( const QColor& aColor )
{
    if ( m_colorPickerDialog != 0 )
    {
        m_colorPickerDialog->setCurrentColor( aColor );
    }
}

QColor ColorPicker::colorFromPicker() const
{
    return m_colorPickerDialog == 0
           ? QColor()
           : m_colorPickerDialog->currentColor();
}

void ColorPicker::colorToUi( const QColor& aColor )
{
    this->setColorToComponentsEditor( aColor );
    this->setColorToHexEditor( aColor );
    this->setColorToPicker( aColor );
    this->updateColorDisplay();
}

QColor ColorPicker::color() const
{
    return m_color;
}

void ColorPicker::retranslateUi()
{
    for ( int i = 0; i < ColorComponentsCount; ++ i )
    {
        QString colorComponentDescription;
        switch ( i )
        {
            case ColorComponentIndexRed:
                colorComponentDescription = tr( "R:" );
                break;
            case ColorComponentIndexGreen:
                colorComponentDescription = tr( "G:" );
                break;
            case ColorComponentIndexBlue:
                colorComponentDescription = tr( "B:" );
                break;
            default:
                break;
        }

        m_colorComponentsLabels[ i ]->setText( colorComponentDescription );
    }

    m_openColorEditorButton->setText( tr( "Pick..." ) );
}

void ColorPicker::updateColorDisplay()
{
    m_colorDisplay->setStyleSheet( QString( "background-color: #%1" ).arg( ColorPicker::colorToHexString( m_color ) ) );
}

void ColorPicker::pickColor()
{
    if ( m_colorPickerDialog == 0 )
    {
        m_colorPickerDialog = new QColorDialog( this );
        connect( m_colorPickerDialog, SIGNAL(colorSelected(QColor)), SLOT(onColorUpdatedFromPicker()) );
    }

    if ( m_colorPickerDialog->isVisible() )
    {
        return;
    }

    m_colorPickerDialog->setCurrentColor( m_color );
    m_colorPickerDialog->show();
}

void ColorPicker::onColorUpdatedFromComponentsEditor()
{
    m_color = this->colorFromComponentsEditor();
    this->setColorToHexEditor( m_color );
    this->setColorToPicker( m_color );
    this->updateColorDisplay();

    emit valueChanged();
    emit valueEdited();
}

void ColorPicker::onColorUpdatedFromHexEditor()
{
    m_color = this->colorFromHexEditor();
    this->setColorToComponentsEditor( m_color );
    this->setColorToPicker( m_color );
    this->updateColorDisplay();

    emit valueChanged();
    emit valueEdited();
}

void ColorPicker::onColorUpdatedFromPicker()
{
    m_color = m_colorPickerDialog->currentColor();
    this->setColorToComponentsEditor( m_color );
    this->setColorToHexEditor( m_color );
    this->updateColorDisplay();

    emit valueChanged();
    emit valueEdited();
}
