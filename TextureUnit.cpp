#include "TextureUnit.h"

#include "CommonEntities.h"

TextureUnit::TextureUnit()
    : index( CommonEntities::TextureUnitIndexInvalid ),
      samplerLocation( CommonEntities::INVALID_UNIFORM_VAR_LOCATION )
{
}

TextureUnit::~TextureUnit()
{
}

void TextureUnit::clear()
{
    index = CommonEntities::TextureUnitIndexInvalid;
    samplerLocation = CommonEntities::INVALID_UNIFORM_VAR_LOCATION;
    textures.clear();
}
