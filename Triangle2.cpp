#include "Triangle2.h"

#include "CommonEntities.h"
#include "Utilities/MathUtilities.h"

#include <glm/glm.hpp>
#include <glm/gtx/vector_angle.hpp>

#include <QTextStream>
#include <QtGlobal>

using namespace Utilities;

const int Triangle2::VERTICES_COUNT = 3;

Triangle2::Triangle2( const glm::vec2& aV0,
                      const glm::vec2& aV1,
                      const glm::vec2& aV2 )
{
    m_vertices.reserve( VERTICES_COUNT );
    m_vertices << aV0
               << aV1
               << aV2;
}

Triangle2::~Triangle2()
{
}

void Triangle2::setVertices(
        const glm::vec2& aV0,
        const glm::vec2& aV1,
        const glm::vec2& aV2 )
{
    this->setV0( aV0 );
    this->setV1( aV1 );
    this->setV2( aV2 );
}

const QList< glm::vec2 >& Triangle2::vertices() const
{
    return m_vertices;
}

void Triangle2::setVertexAtIndex( const int index, const glm::vec2& v )
{
    m_vertices[ Triangle2::cyclicVertexIndex( index ) ] = v;
}

glm::vec2 Triangle2::vertexAtIndex( const int index ) const
{
    return m_vertices[ Triangle2::cyclicVertexIndex( index ) ];
}

void Triangle2::setV0( const glm::vec2& aV0 )
{
    this->setVertexAtIndex( 0, aV0 );
}

glm::vec2 Triangle2::v0() const
{
    return this->vertexAtIndex( 0 );
}

void Triangle2::setV1( const glm::vec2& aV1 )
{
    this->setVertexAtIndex( 1, aV1 );
}

glm::vec2 Triangle2::v1() const
{
    return this->vertexAtIndex( 1 );
}

void Triangle2::setV2( const glm::vec2& aV2 )
{
    this->setVertexAtIndex( 2, aV2 );
}

glm::vec2 Triangle2::v2() const
{
    return this->vertexAtIndex( 2 );
}

void Triangle2::reverse()
{
    qSwap( m_vertices[ 0 ], m_vertices[ 2 ] );
}

int Triangle2::indexOfVertex( const glm::vec2& v ) const
{
    return MathUtilities::vertexIndex( m_vertices, v );
}

bool Triangle2::isVertex( const glm::vec2& v ) const
{
    return this->indexOfVertex( v ) >= 0;
}

bool Triangle2::contains( const glm::vec2& v ) const
{
    const glm::vec2 v0 = this->v0();
    const glm::vec2 v1 = this->v1();
    const glm::vec2 v2 = this->v2();

    const bool b0 = Triangle2::sign( v, v0, v1 ) < 0.0f;
    const bool b1 = Triangle2::sign( v, v1, v2 ) < 0.0f;
    const bool b2 = Triangle2::sign( v, v2, v0 ) < 0.0f;

    return ( b0 == b1 ) && ( b1 == b2 );
}

bool Triangle2::contains( const Triangle2& t ) const
{
    const int otherTriangleVerticesCount = t.vertices().size();
    for ( int i = 0; i < otherTriangleVerticesCount; ++ i )
    {
        if ( ! this->contains( t.vertexAtIndex( i ) ) )
        {
            return false;
        }
    }

    return true;
}

glm::vec2::value_type Triangle2::angleAtVertex( const glm::vec2& v ) const
{
    const int vIndex = this->indexOfVertex( v );
    if ( vIndex < 0 )
    {
        return 0.0f;
    }

    return glm::angle( glm::normalize( this->vertexAtIndex( vIndex - 1 ) - v ),
                       glm::normalize( this->vertexAtIndex( vIndex + 1 ) - v ) );
}

glm::vec2::value_type Triangle2::area() const
{
    const glm::vec2 v0 = this->v0();
    const glm::vec2 v1 = this->v1();
    const glm::vec2 v2 = this->v2();

    const glm::vec2 edge10 = v1 - v0;
    const glm::vec2 edge20 = v2 - v0;
    const glm::vec2::value_type edgesLengthProduct = glm::length( edge10 ) * glm::length( edge20 );
    const glm::vec2::value_type sinAngle = glm::sqrt( 1.0f - glm::pow( glm::dot( edge10, edge20 ) / edgesLengthProduct, 2.0f ) );

    return sinAngle * edgesLengthProduct / 2.0f;
}

bool Triangle2::operator==( const Triangle2& other ) const
{
    const int verticesCount = m_vertices.size();
    if ( verticesCount != other.vertices().size() )
    {
        return false;
    }

    for ( int i = 0; i < verticesCount; ++ i )
    {
        if ( ! MathUtilities::epsilonEqual(
                 this->vertexAtIndex( i ),
                 other.vertexAtIndex( i ) ) )
        {
            return false;
        }
    }

    return true;
}

bool Triangle2::operator!=( const Triangle2& other ) const
{
    return ! ( *this == other );
}

QString Triangle2::toString() const
{
    QString s;

    QTextStream ts( &s );

    ts << QT_STRINGIFY2( Triangle2 ) << '(' << endl
       << "\tv0: " << CommonEntities::vecToString( this->v0() ) << endl
       << "\tv1: " << CommonEntities::vecToString( this->v1() ) << endl
       << "\tv2: " << CommonEntities::vecToString( this->v2() ) << " )";

    return s;
}

int Triangle2::cyclicVertexIndex( const int vertexIndex ) const
{
    const int verticesCount = m_vertices.size();
    return vertexIndex >= 0
           ? vertexIndex % verticesCount
           : ( verticesCount - qAbs( vertexIndex ) % verticesCount );
}

glm::vec2::value_type Triangle2::sign(
        const glm::vec2& v0,
        const glm::vec2& v1,
        const glm::vec2& v2 )
{
    return   ( v0.x - v2.x ) * ( v1.y - v2.y )
           - ( v1.x - v2.x ) * ( v0.y - v2.y );
}

#ifndef QT_NO_DEBUG_STREAM
QDebug operator<<( QDebug qdebug, const Triangle2& t )
{
    return qdebug << t.toString();
}
#endif
