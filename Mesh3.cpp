#include "Mesh3.h"

#include "CommonEntities.h"
#include "Utilities/MathUtilities.h"

#include <glm/glm.hpp>

#include <algorithm>

using namespace Utilities;

Mesh3::Mesh3( const QList< QPair< Triangle3, Triangle2 > >& aTriangles )
    : m_triangles( aTriangles )
{
}

Mesh3::~Mesh3()
{
}

void Mesh3::setTriangles( const QList< QPair< Triangle3, Triangle2 > >& aTriangles )
{
    m_triangles = aTriangles;
}

const QList< QPair< Triangle3, Triangle2 > >& Mesh3::triangles() const
{
    return m_triangles;
}

QList< QPair< Triangle3, Triangle2 > > Mesh3::trianglesSharingVertex( const glm::vec3& v ) const
{
    QList< QPair< Triangle3, Triangle2 > > triangles;
    const int trianglesCount = m_triangles.size();
    for ( int i = 0; i < trianglesCount; ++ i )
    {
        const QPair< Triangle3, Triangle2 >& triangle = m_triangles[ i ];
        if ( triangle.first.isVertex( v ) )
        {
            triangles.append( triangle );
        }
    }

    return triangles;
}

glm::vec3 Mesh3::normalAtVertex( const glm::vec3& v ) const
{
    const QList< QPair< Triangle3, Triangle2 > > triangles = this->trianglesSharingVertex( v );
    glm::vec3 vn;

    const int trianglesCount = triangles.size();
    for ( int i = 0; i < trianglesCount; ++ i )
    {
        const Triangle3& triangle = triangles[ i ].first;
        vn += triangle.normal()
              * triangle.area()
              * triangle.angleAtVertex( v );
    }

    return glm::normalize( vn );
}

glm::vec4 Mesh3::tangentAtVertex( const glm::vec3& v ) const
{
    return this->tangentOrBitangentAtVertex( v, true, false );
}

glm::vec4 Mesh3::bitangentAtVertex( const glm::vec3& v ) const
{
    return this->tangentOrBitangentAtVertex( v, false, false );
}

bool Mesh3::isVertex( const glm::vec3 v ) const
{
    const int trianglesCount = m_triangles.size();
    for ( int i = 0; i < trianglesCount; ++ i )
    {
        if ( m_triangles[ i ].first.isVertex( v ) )
        {
            return true;
        }
    }

    return false;
}

bool Mesh3::contains( const Triangle3& t ) const
{
    const int trianglesCount = m_triangles.size();
    for ( int i = 0; i < trianglesCount; ++ i )
    {
        if ( m_triangles[ i ].first == t )
        {
            return true;
        }
    }

    return false;
}

glm::vec3::value_type Mesh3::area() const
{
    glm::vec3::value_type area = 0.0f;
    const int trianglesCount = m_triangles.size();
    for ( int i = 0; i < trianglesCount; ++ i )
    {
        area += m_triangles[ i ].first.area();
    }

    return area;
}

bool Mesh3::operator==( const Mesh3& other ) const
{
    const QList< QPair< Triangle3, Triangle2 > >& otherTriangles = other.triangles();
    if ( m_triangles.size() != otherTriangles.size() )
    {
        return false;
    }

    return std::equal( m_triangles.begin(),
                       m_triangles.end(),
                       otherTriangles.begin() );
}

bool Mesh3::operator!=( const Mesh3& other ) const
{
    return ! ( *this == other );
}

glm::vec4 Mesh3::tangentOrBitangentAtVertex(
        const glm::vec3& v,
        const bool requestTangent,
        const bool shouldCalculateManually ) const
{
#define TB_CALCULATION_MATRIX 0
#define TB_CALCULATION_VECTOR_1 1
#define TB_CALCULATION_VECTOR_2 2
#define TB_CALCULATION_APPROACH TB_CALCULATION_MATRIX

    const glm::vec3 vn = this->normalAtVertex( v );
    const QList< QPair< Triangle3, Triangle2 > > triangles = this->trianglesSharingVertex( v );

#ifdef THOROUGH_LOGGING_ENABLED
//    qDebug() << "==========================" << endl
//             << "    v(" << v << ')' << endl
//             << "    n(" << vn << ')' << endl
//             << "------------------------";
#endif

#if TB_CALCULATION_APPROACH == TB_CALCULATION_MATRIX
    Q_UNUSED( shouldCalculateManually )

    glm::vec3 vt;
    glm::vec3 vb;
#elif TB_CALCULATION_APPROACH == TB_CALCULATION_VECTOR_1
    glm::vec3 vtManual;
    glm::vec3 vbManual;
#elif TB_CALCULATION_APPROACH == TB_CALCULATION_VECTOR_2
    glm::vec3 vtManual2;
    glm::vec3 vbManual2;
#endif

    const int trianglesCount = triangles.size();
    for ( int i = 0; i < trianglesCount; ++ i )
    {
        const QPair< Triangle3, Triangle2 >& triangle = triangles[ i ];
        const Triangle3& triangleGeometry      = triangle.first;
        const Triangle2& triangleTextureCoords = triangle.second;

        const glm::vec3 v10 = triangleGeometry.v1() - triangleGeometry.v0();
        const glm::vec3 v20 = triangleGeometry.v2() - triangleGeometry.v0();

        const glm::vec2 vt10 = triangleTextureCoords.v1() - triangleTextureCoords.v0();
        const glm::vec2 vt20 = triangleTextureCoords.v2() - triangleTextureCoords.v0();

        const glm::vec3::value_type weightFactor =
                triangleGeometry.area()
                * triangleGeometry.angleAtVertex( v );

#if TB_CALCULATION_APPROACH == TB_CALCULATION_MATRIX
        // matrix approach
        const glm::mat2x3 geometryMat(
                    v10,
                    v20 );

        const glm::mat2x2 textureCoordsMat = glm::transpose( glm::mat2x2(
                                                                 vt10,
                                                                 vt20 ) );

        const glm::mat2x3 tbMat = glm::transpose( glm::inverse( textureCoordsMat )
                                                  * glm::transpose( geometryMat ) );

        const glm::vec3& triangleTangent   = tbMat[ 0 ];
        const glm::vec3& triangleBiTangent = tbMat[ 1 ];

        vt += triangleTangent   * weightFactor;
        vb += triangleBiTangent * weightFactor;

#elif TB_CALCULATION_APPROACH == TB_CALCULATION_VECTOR_1
        // vector approach 1
        const float r = 1.0f / ( vt10.x * vt20.y - vt20.x * vt10.y );

        glm::vec3 sdir( vt20.y * v10.x - vt10.y * v20.x,
                        vt20.y * vt10.y - vt10.y * v20.y,
                        vt20.y * v10.z - vt10.y * v20.z );
        sdir *= r;
        vtManual += sdir * weightFactor;

        glm::vec3 tdir( vt10.x * v20.x - vt20.x * v10.x,
                        vt10.x * v20.y - vt20.x * v10.y,
                        vt10.x * v20.z - vt20.x * v10.z );
        tdir *= r;
        vbManual += tdir * weightFactor;

#elif TB_CALCULATION_APPROACH == TB_CALCULATION_VECTOR_2
        // vector approach 2
        const float r = 1.0f / ( vt10.x * vt20.y - vt20.x * vt10.y );

        const glm::vec3 tangent   = ( v10 * vt20.y - v20 * vt10.y ) * r;
        const glm::vec3 bitangent = ( v20 * vt10.x - v10 * vt20.x ) * r;

        vtManual2 += tangent   * weightFactor;
        vbManual2 += bitangent * weightFactor;
#endif
    }

    // Gram-Schmidt orthogonalization matrix approach
#if TB_CALCULATION_APPROACH == TB_CALCULATION_MATRIX
    const QList< glm::vec3 > grammSchmidtOrthonormalized =
            MathUtilities::gramSchmidtOrthogonalization( QList< glm::vec3 >()
                                                         << vn
                                                         << vt
                                                         << vb );
    const glm::vec3 vtOrtho   = grammSchmidtOrthonormalized[ 1 ];
    const glm::vec3 vbOrtho   = grammSchmidtOrthonormalized[ 2 ];
    const glm::vec3 vnOrtho   = grammSchmidtOrthonormalized[ 0 ];

//    const glm::vec3 vtOrtho1  =  vt - vn * glm::dot( vn, vt );
//    const glm::vec3 vbOrtho1  =  vb - vn * glm::dot( vn, vb ) - vtOrtho1 * glm::dot( vtOrtho1, vb ) / glm::dot( vtOrtho1, vtOrtho1 );
//    const glm::vec3 vnOrtho1  =  vn;

//    const glm::vec3 vtOrtho2  =  vt - glm::dot( vt, vn ) / ( glm::length( vn ) * glm::length( vn ) ) * vn;
//    const glm::vec3 vbOrtho21 =  vb - glm::dot( vb, vn ) / ( glm::length( vn ) * glm::length( vn ) ) * vn - glm::dot( vb, vtOrtho2 ) / ( glm::length( vtOrtho2 ) * glm::length( vtOrtho2 ) ) * vtOrtho2;
//    const glm::vec3 vbOrtho22 =  vb - glm::dot( vb, vn ) / ( glm::dot( vn, vn ) ) * vn - glm::dot( vb, vtOrtho2 ) / ( glm::dot( vtOrtho2, vtOrtho2 ) ) * vtOrtho2;
//    const glm::vec3 vnOrtho2  =  vn;

#ifdef THOROUGH_LOGGING_ENABLED
//    qDebug() << endl
//             << "vtOrtho   =" << vtOrtho << endl
//             << "vbOrtho   =" << vbOrtho << endl
//             << "vnOrtho   =" << vnOrtho << endl
//             << "-----------" << endl
//             << "vtOrtho1  =" << vtOrtho1 << endl
//             << "vbOrtho1  =" << vbOrtho1 << endl
//             << "vnOrtho1  =" << vnOrtho1 << endl
//             << "-----------" << endl
//             << "vtOrtho2  =" << vtOrtho2 << endl
//             << "vbOrtho21 =" << vbOrtho21 << endl
//             << "vbOrtho22 =" << vbOrtho22 << endl
//             << "vnOrtho2  =" << vnOrtho2;
#endif

    const glm::vec3 vtOrthoNorm = MathUtilities::safeNormalize( vtOrtho );
    const glm::vec3 vbOrthoNorm = MathUtilities::safeNormalize( vbOrtho );
    const glm::vec3 vnOrthoNorm = MathUtilities::safeNormalize( vnOrtho );

    const glm::vec3::value_type handedness = glm::determinant( glm::transpose( glm::mat3x3( vtOrthoNorm, vbOrthoNorm, vnOrthoNorm ) ) );
    const glm::vec3& vTorBOrthoNorm = MathUtilities::safeNormalize( requestTangent ? vtOrthoNorm : vbOrthoNorm );
    const glm::vec4 vTorBOrthoNormHand = glm::vec4(
                                             vTorBOrthoNorm.x,
                                             vTorBOrthoNorm.y,
                                             vTorBOrthoNorm.z,
                                             handedness );
// #define USE_FAKE_TANGENTS
#ifdef USE_FAKE_TANGENTS
    static QList< QPair< glm::vec3, glm::vec3 > > vertexPositionsTangents;
    if ( vertexPositionsTangents.isEmpty() )
    {
        vertexPositionsTangents
                << QPair< glm::vec3, glm::vec3 >( glm::vec3( -0.5, -0.5,  0.5 ), glm::vec3(  0.5,  0.0,  0.5 ) )
                << QPair< glm::vec3, glm::vec3 >( glm::vec3(  0.5, -0.5,  0.5 ), glm::vec3(  0.5,  0.0, -0.5 ) )
                << QPair< glm::vec3, glm::vec3 >( glm::vec3(  0.5, -0.5, -0.5 ), glm::vec3( -0.5,  0.0, -0.5 ) )
                << QPair< glm::vec3, glm::vec3 >( glm::vec3( -0.5, -0.5, -0.5 ), glm::vec3( -0.5,  0.0,  0.5 ) )

                << QPair< glm::vec3, glm::vec3 >( glm::vec3( -0.5, 0.5,  0.5 ),  glm::vec3(  0.5,  0.0,  0.5 ) )
                << QPair< glm::vec3, glm::vec3 >( glm::vec3(  0.5, 0.5,  0.5 ),  glm::vec3(  0.5,  0.0, -0.5 ) )
                << QPair< glm::vec3, glm::vec3 >( glm::vec3(  0.5, 0.5, -0.5 ),  glm::vec3( -0.5,  0.0, -0.5 ) )
                << QPair< glm::vec3, glm::vec3 >( glm::vec3( -0.5, 0.5, -0.5 ),  glm::vec3( -0.5,  0.0,  0.5 ) )

                << QPair< glm::vec3, glm::vec3 >( glm::vec3( 0.0, 0.75, 0.0 ),   glm::vec3( 0.5,  0.0,  0.0 ) );
    }

    const int vertexPositionsTangentsCount = vertexPositionsTangents.size();
    for ( int i = 0; i < vertexPositionsTangentsCount; ++ i )
    {
        const QPair< glm::vec3, glm::vec3 >& vertexPositionTangent = vertexPositionsTangents[ i ];
        if ( MathUtilities::epsilonEqual( vertexPositionTangent.first, v ) )
        {
            return glm::vec4( vertexPositionTangent.second.x,
                              vertexPositionTangent.second.y,
                              vertexPositionTangent.second.z,
                              1.0 );
        }
    }

    return glm::vec4( 0.0 );
#endif

#ifdef THOROUGH_LOGGING_ENABLED
    qDebug() << "========================" << endl
             << "    v(" << v << ')' << endl
             << "m:  t(" << vtOrthoNorm << ')' << endl
             << "m:  b(" << vbOrthoNorm << ')' << endl
             << "m:  h(" << handedness << ')' << endl
             << "------------------------";
#endif

    return vTorBOrthoNormHand;

#elif TB_CALCULATION_APPROACH == TB_CALCULATION_VECTOR_1
    // Gram-Schmidt orthogonalization vector approach 1
    const glm::vec3 vtManualOrtho     = vtManual - vn * glm::dot( vn, vtManual );
    const glm::vec3 vbManualOrtho     = vbManual - vn * glm::dot( vn, vbManual ) - vtManualOrtho * glm::dot( vtManualOrtho, vbManual ) / glm::dot( vtManualOrtho, vtManualOrtho );

    const glm::vec3 vtManualOrthoNorm = MathUtilities::safeNormalize( vtManualOrtho );
    const glm::vec3 vbManualOrthoNorm = MathUtilities::safeNormalize( vbManualOrtho );

#elif TB_CALCULATION_APPROACH == TB_CALCULATION_VECTOR_2
    // Gram-Schmidt orthogonalization vector approach 2
    const glm::vec3 vtManual2Ortho     = vtManual2 - vn * glm::dot( vn, vtManual2 );
    const glm::vec3 vbManual2Ortho     = vbManual2 - vn * glm::dot( vn, vbManual2 ) - vtManual2Ortho * glm::dot( vtManual2Ortho, vbManual2 ) / glm::dot( vtManual2Ortho, vtManual2Ortho );

    const glm::vec3 vtManual2OrthoNorm = MathUtilities::safeNormalize( vtManual2Ortho );
    const glm::vec3 vbManual2OrthoNorm = MathUtilities::safeNormalize( vbManual2Ortho );
#endif

#if TB_CALCULATION_APPROACH == TB_CALCULATION_VECTOR_1
    // vector approach 1: calculate handedness
    const glm::vec3::value_type handednessManual = ( glm::dot( glm::cross( vn, vtManualOrthoNorm ), vbManualOrthoNorm ) < 0.0f ) ? -1.0f : 1.0f;

    const glm::vec3& vTorBManualOrthoNorm = requestTangent ? vtManualOrthoNorm : vbManualOrthoNorm;
    const glm::vec4 vTorBManualOrthoNormHand = glm::normalize( glm::vec4(
                                                                   vTorBManualOrthoNorm.x,
                                                                   vTorBManualOrthoNorm.y,
                                                                   vTorBManualOrthoNorm.z,
                                                                   handednessManual ) );
#ifdef THOROUGH_LOGGING_ENABLED
    qDebug() << "v1: t(" << vtManualOrthoNorm << ')' << endl
             << "v1: b(" << vbManualOrthoNorm << ')' << endl
             << "v1: h(" << handednessManual << ')' << endl
             << "------------------------";
#endif

    return vTorBManualOrthoNormHand;

#elif TB_CALCULATION_APPROACH == TB_CALCULATION_VECTOR_2
    // vector approach 2: calculate handedness
    const glm::vec3::value_type handednessManual2 = ( glm::dot( glm::cross( vn, vtManual2OrthoNorm ), vbManual2OrthoNorm ) < 0.0f ) ? -1.0f : 1.0f;
    const glm::vec3& vTorBManual2OrthoNorm = requestTangent ? vtManual2OrthoNorm : vbManual2OrthoNorm;
    const glm::vec4 vTorBManual2OrthoNormHand = glm::normalize( glm::vec4(
                                                                    vTorBManual2OrthoNorm.x,
                                                                    vTorBManual2OrthoNorm.y,
                                                                    vTorBManual2OrthoNorm.z,
                                                                    handednessManual2 ) );
#ifdef THOROUGH_LOGGING_ENABLED
    qDebug() << "v2: t(" << vtManual2OrthoNorm << ')' << endl
             << "v2: b(" << vbManual2OrthoNorm << ')' << endl
             << "v2: h(" << handednessManual2 << ')' << endl
             << "--------------------------";
#endif

    return vTorBManual2OrthoNormHand;
#endif
}

#ifdef TEST_ORG_ALGORITHM
static const QList< glm::vec3 > allVertices =
        QList< glm::vec3 >()
        /* 0 */ << glm::vec3( -0.5, -0.5,  0.5 ) // front bottom left
        /* 1 */ << glm::vec3(  0.5, -0.5,  0.5 ) // front bottom right
        /* 2 */ << glm::vec3(  0.5, -0.5, -0.5 ) // back bottom right
        /* 3 */ << glm::vec3( -0.5, -0.5, -0.5 ) // back bottom left

        /* 4 */ << glm::vec3( -0.5, 0.5,  0.5 ) // front top left
        /* 5 */ << glm::vec3(  0.5, 0.5,  0.5 ) // front top right
        /* 6 */ << glm::vec3(  0.5, 0.5, -0.5 ) // back top right
        /* 7 */ << glm::vec3( -0.5, 0.5, -0.5 ) // back top left

        /* 8 */ << glm::vec3( 0.0, 0.75, 0.0 ); // peak

static const QList< glm::vec2 > allVertices =
        QList< glm::vec2 >()
        /* 0 */ << glm::vec2( -0.5, -0.5,  0.5 ) // front bottom left
        /* 1 */ << glm::vec2(  0.5, -0.5,  0.5 ) // front bottom right
        /* 2 */ << glm::vec2(  0.5, -0.5, -0.5 ) // back bottom right
        /* 3 */ << glm::vec2( -0.5, -0.5, -0.5 ) // back bottom left

        /* 4 */ << glm::vec2( -0.5, 0.5,  0.5 ) // front top left
        /* 5 */ << glm::vec2(  0.5, 0.5,  0.5 ) // front top right
        /* 6 */ << glm::vec2(  0.5, 0.5, -0.5 ) // back top right
        /* 7 */ << glm::vec2( -0.5, 0.5, -0.5 ) // back top left

        /* 8 */ << glm::vec2( 0.0, 0.75, 0.0 ); // peak


static const QList< glm::ivec3 > triangles =
        QList< glm::ivec3 >()
        // bottom
        << glm::ivec3( 3, 2, 0 )
        << glm::ivec3( 0, 2, 1 )

        // front
        << glm::ivec3( 0, 1, 4 )
        << glm::ivec3( 4, 1, 5 )

        // back
        << glm::ivec3( 2, 3, 6 )
        << glm::ivec3( 6, 3, 7 )

        // left
        << glm::ivec3( 3, 0, 7 )
        << glm::ivec3( 7, 0, 4 )

        // right
        << glm::ivec3( 1, 2, 5 )
        << glm::ivec3( 5, 2, 6 )

        // roof
        << glm::ivec3( 4, 5, 8 )
        << glm::ivec3( 5, 6, 8 )
        << glm::ivec3( 6, 7, 8 )
        << glm::ivec3( 7, 4, 8 );

struct Triangle
{
    ushort index[ 3 ];
};

#include <glm/vec3.hpp>
#include <glm/vec4.hpp>

QList< glm::vec4 > calculateTangents(
        const QList< glm::vec3 >& vertices,
        const QList< glm::vec3 >& normals,
        const QList< glm::vec2 >& texcoords,
        const QList< glm::ivec3 >& triangles )
{
    const int vertexCount = vertices.size();
    QList< glm::vec3 > tan1;
    QList< glm::vec3 > tan2;
    tan1.reserve( vertexCount );
    tan2.reserve( vertexCount );
    for ( int i = 0; i < vertexCount; ++ i )
    {
        tan1.append( glm::vec3( 0.0f ) );
        tan2.append( glm::vec3( 0.0f ) );
    }

    const int triangleCount = triangles.size();
    for ( int i = 0; i < triangleCount; ++ i )
    {
        const glm::ivec3& triangle = triangles[ i ];
        int i1 = triangle.x;
        int i2 = triangle.y;
        int i3 = triangle.z;

        const glm::vec3& v1 = vertices[i1];
        const glm::vec3& v2 = vertices[i2];
        const glm::vec3& v3 = vertices[i3];

        const glm::vec2& w1 = texcoords[i1];
        const glm::vec2& w2 = texcoords[i2];
        const glm::vec2& w3 = texcoords[i3];

        float x1 = v2.x - v1.x;
        float x2 = v3.x - v1.x;
        float y1 = v2.y - v1.y;
        float y2 = v3.y - v1.y;
        float z1 = v2.z - v1.z;
        float z2 = v3.z - v1.z;

        float s1 = w2.x - w1.x;
        float s2 = w3.x - w1.x;
        float t1 = w2.y - w1.y;
        float t2 = w3.y - w1.y;

        float r = 1.0F / (s1 * t2 - s2 * t1);
        glm::vec3 sdir((t2 * x1 - t1 * x2) * r, (t2 * y1 - t1 * y2) * r,
                (t2 * z1 - t1 * z2) * r);
        glm::vec3 tdir((s1 * x2 - s2 * x1) * r, (s1 * y2 - s2 * y1) * r,
                (s1 * z2 - s2 * z1) * r);

        tan1[i1] += sdir;
        tan1[i2] += sdir;
        tan1[i3] += sdir;

        tan2[i1] += tdir;
        tan2[i2] += tdir;
        tan2[i3] += tdir;
    }

    QList< glm::vec4 > tangents;
    for ( int i = 0; i < vertexCount; ++ i )
    {
        const glm::vec3& n = normals[i];
        const glm::vec3& t = tan1[i];

        // Gram-Schmidt orthogonalize
        const glm::vec3 tangentGeom = glm::normalize(t - n * glm::dot(n, t));

        // Calculate handedness
        const float handedness = (glm::dot(glm::cross(n, t), tan2[i]) < 0.0F) ? -1.0F : 1.0F;
        const glm::vec4 tangent( tangentGeom.x, tangentGeom.x, tangentGeom.x, handedness );
        tangents.append( tangent );
    }

    return tangents;
}
#endif
