#include "ImageUtilities.h"

#include <glm/glm.hpp>

#include <QImage>
#include <QPainter>
#include <QRgb>
#include <QtGlobal>

#include <cmath>

// http://stackoverflow.com/questions/3903223/qt4-how-to-blur-qpixmap-image

QT_BEGIN_NAMESPACE
extern Q_WIDGETS_EXPORT void qt_blurImage( QPainter *p, QImage &blurImage, qreal radius, bool quality, bool alphaOnly, int transposed = 0 );
QT_END_NAMESPACE

using namespace Utilities;

// don't define
// ImageUtilities::ImageUtilities()
// {
// }

int ImageUtilities::intColorComponentMin()
{
    return std::numeric_limits< quint8 >::min();
}

int ImageUtilities::intColorComponentMax()
{
    return std::numeric_limits< quint8 >::max();
}

glm::vec3::value_type ImageUtilities::realFromIntColorComponent( const int intColorComponent )
{
    return glm::clamp( intColorComponent / glm::vec3::value_type( ImageUtilities::intColorComponentMax() ), 0.0f, 1.0f );
}

int ImageUtilities::intFromRealColorComponent( const glm::vec3::value_type realColorComponent )
{
    const int minIntColorComponent = ImageUtilities::intColorComponentMin();
    const int maxIntColorComponent = ImageUtilities::intColorComponentMax();

    return glm::clamp( int( glm::round( realColorComponent * ( maxIntColorComponent - minIntColorComponent ) + minIntColorComponent ) ),
                       minIntColorComponent,
                       maxIntColorComponent );
}

glm::vec3 ImageUtilities::realFromIntColor( const QColor& intColor )
{
    return glm::vec3( ImageUtilities::realFromIntColorComponent( intColor.red() ),
                      ImageUtilities::realFromIntColorComponent( intColor.green() ),
                      ImageUtilities::realFromIntColorComponent( intColor.blue() ) );
}

QColor ImageUtilities::intFromRealColor( const glm::vec3& realColor )
{
    return QColor( ImageUtilities::intFromRealColorComponent( realColor.r ),
                   ImageUtilities::intFromRealColorComponent( realColor.g ),
                   ImageUtilities::intFromRealColorComponent( realColor.b ) );
}

QImage ImageUtilities::toGrayscale( const QImage& image )
{
    QImage grayscaleImage = image.convertToFormat( QImage::Format_RGB32 );
    const int height = grayscaleImage.height();
    const int width  = grayscaleImage.width();
    for ( int y = 0; y < height; ++ y )
    {
        for ( int x = 0; x < width; ++ x )
        {
            const QRgb pixelColor = grayscaleImage.pixel( x, y );
            const int gray = qGray( pixelColor );
            const int alpha = qAlpha( pixelColor );
            grayscaleImage.setPixel( x, y, qRgba( gray, gray, gray, alpha ) );
        }
    }

    return grayscaleImage;
}

QImage ImageUtilities::toNormalMap( const QImage& image, const qreal scaleFactor )
{
    const QImage heightMap = ImageUtilities::toGrayscale( image );
    const int height = heightMap.height();
    const int width  = heightMap.width();
    QImage normalMapImage( width, height, QImage::Format_RGB32 );

    const int lastY = height - 1;
    const int lastX = width - 1;
    for ( int y = 0; y <= lastY; ++ y )
    {
        const int previousVPixelY = y == 0
                                    ? 0
                                    : y - 1;
        const int nextVPixelY = y == lastY
                                ? lastY
                                : y + 1;

        for ( int x = 0; x <= lastX; ++ x )
        {
            const glm::vec3 t(
                        0.0,
                        1.0,
                        scaleFactor
                        * ( ImageUtilities::geometricComponentFromColorComponent( qRed( heightMap.pixel( x, previousVPixelY ) ) )
                            - ImageUtilities::geometricComponentFromColorComponent( qRed( heightMap.pixel( x, nextVPixelY ) ) ) ) );

            const int previousHPixelX = x == 0
                                        ? 0
                                        : x - 1;

            const int nextHPixelX = x == lastX
                                    ? lastX
                                    : x + 1;
            const glm::vec3 s(
                        1.0,
                        0.0,
                        scaleFactor
                        * ( ImageUtilities::geometricComponentFromColorComponent( qRed( heightMap.pixel( previousHPixelX, y ) ) )
                            - ImageUtilities::geometricComponentFromColorComponent( qRed( heightMap.pixel( nextHPixelX, y ) ) ) ) );


            const glm::vec3 n = glm::normalize( glm::cross( glm::normalize( s ), glm::normalize( t ) ) );
            QColor color = ImageUtilities::colorFromNormal( n );
            color.setRed( 255 - color.red() );

            normalMapImage.setPixel( x, y, color.rgba() );
        }
    }

    return normalMapImage;
}

#define USE_QT_INTEGRATED_OVERLAY

QImage ImageUtilities::toEnhancedNormalMap( const QImage& image, const qreal scaleFactor )
{
    const QImage normalMap = ImageUtilities::toNormalMap( image, scaleFactor );
    static const int maxStackedNormalMaps = 5;

    QImage enhancedNormalMap;
#ifdef USE_QT_INTEGRATED_OVERLAY
    QPainter p;
#endif
    const qreal blurRadiusMultiplier = log10( image.width() ) / 3.0 * 10.0;
    for ( int i = maxStackedNormalMaps; i >= 0; -- i )
    {
        const qreal blurRadius = blurRadiusMultiplier * ( i + 1 );
        const QImage blurredNormalMap = ImageUtilities::blurredImage( normalMap, blurRadius );
        if ( enhancedNormalMap.isNull() )
        {
            if ( ! blurredNormalMap.isNull() )
            {
                enhancedNormalMap = blurredNormalMap;
#ifdef USE_QT_INTEGRATED_OVERLAY
                p.begin( & enhancedNormalMap );
                p.setCompositionMode( QPainter::CompositionMode_Overlay );
#endif
            }
        }
        else
        {
#ifdef USE_QT_INTEGRATED_OVERLAY
            p.drawImage( QPoint(), blurredNormalMap );
#else
            enhancedNormalMap = ImageUtilities::overlayImages( blurredNormalMap, enhancedNormalMap );
#endif
        }
    }

#ifdef USE_QT_INTEGRATED_OVERLAY
    if ( p.isActive() )
    {
        p.drawImage( QPoint(), normalMap );
    }
#else
    if ( ! enhancedNormalMap.isNull() )
    {
        enhancedNormalMap = ImageUtilities::overlayImages( normalMap, enhancedNormalMap );
    }
#endif

    return enhancedNormalMap;
}

glm::vec3 ImageUtilities::normalFromColor( const QColor& color )
{
    return glm::vec3( color.red() );
}

QColor ImageUtilities::colorFromNormal( const glm::vec3& normal )
{
    const glm::vec3 norm = glm::normalize( normal );

    return QColor( ImageUtilities::colorComponentFromGeometricComponent( norm.x ),
                   ImageUtilities::colorComponentFromGeometricComponent( norm.y ),
                   ImageUtilities::colorComponentFromGeometricComponent( norm.z ) );
}

int ImageUtilities::colorComponentFromGeometricComponent( const glm::vec3::value_type& geometricComponent )
{
    static const int minColorComponentValue = ImageUtilities::intColorComponentMin();
    static const int maxColorComponentValue = ImageUtilities::intColorComponentMax();

    return qBound( minColorComponentValue,
                   minColorComponentValue
                   + qRound( ( ( geometricComponent + 1.0 ) / 2.0 )
                             * ( maxColorComponentValue - minColorComponentValue ) ),
                   maxColorComponentValue );
}

glm::vec3::value_type ImageUtilities::geometricComponentFromColorComponent( const int colorComponent )
{
    static const int minColorComponentValue = ImageUtilities::intColorComponentMin();
    static const int maxColorComponentValue = ImageUtilities::intColorComponentMax();

    return qBound(
                -1.0,
                2.0
                * ( ( qBound( minColorComponentValue,
                              colorComponent,
                              maxColorComponentValue )
                      - minColorComponentValue )
                    / qreal( maxColorComponentValue - minColorComponentValue ) )
                - 1.0,
                1.0 );
}

QImage ImageUtilities::blurredImage(
        const QImage& image,
        const qreal radius,
        const bool quality,
        const bool alphaOnly,
        const int transposed )
{
    QImage originalImg = image;
    const QSize imageSize = image.size();
    QImage blurredImg( imageSize.width(), imageSize.height(), image.format() );
    blurredImg.fill( Qt::transparent );
    {
        QPainter painter( &blurredImg );
        qt_blurImage( &painter, originalImg, radius, quality, alphaOnly, transposed );
    }

    return blurredImg;
}

// http://www.mediamacros.com/
QImage ImageUtilities::overlayImages( const QImage& foreImage, const QImage& backImage )
{
    if ( foreImage.isNull() || backImage.isNull() )
    {
        return QImage();
    }

    const QSize foreImageSize = foreImage.size();
    const QSize backImageSize = backImage.size();
    const QSize leastCommonSize( qMin( foreImageSize.width(), backImageSize.width() ),
                                 qMin( foreImageSize.height(), backImageSize.height() ) );
    if ( leastCommonSize.isEmpty() )
    {
        return QImage();
    }

    const qreal divisor = 127.0;
    QImage overlayedImage( leastCommonSize, QImage::Format_RGB32 );

    const int width = leastCommonSize.width();
    const int height = leastCommonSize.height();
    for ( int x = 0; x < width; ++ x )
    {
        for ( int y = 0; y < height; ++ y )
        {
            QColor overlayedImagePixelColor;
            const QColor foreImageColor( ( foreImage.pixel( x, y ) ) );
            const QColor backImageColor( ( backImage.pixel( x, y ) ) );

            static const int minColorComponentValue = ImageUtilities::intColorComponentMin();
            static const int maxColorComponentValue = ImageUtilities::intColorComponentMax();

            static const ColorComponentType allColorComponentTypes[] =
            {
                ColorComponentTypeRed,
                ColorComponentTypeGreen,
                ColorComponentTypeBlue
            };

            static const int allColorComponentTypesCount = sizeof( allColorComponentTypes ) / sizeof( ColorComponentType );
            for ( int cc = 0; cc < allColorComponentTypesCount; ++ cc )
            {
                int foreImageColorComponentValue = minColorComponentValue;
                int backImageColorComponentValue = minColorComponentValue;
                const ColorComponentType colorComponentType = allColorComponentTypes[ cc ];
                switch ( colorComponentType )
                {
                    case ColorComponentTypeRed:
                        foreImageColorComponentValue = foreImageColor.red();
                        backImageColorComponentValue = backImageColor.red();
                        break;
                    case ColorComponentTypeGreen:
                        foreImageColorComponentValue = foreImageColor.green();
                        backImageColorComponentValue = backImageColor.green();
                        break;
                    case ColorComponentTypeBlue:
                        foreImageColorComponentValue = foreImageColor.blue();
                        backImageColorComponentValue = backImageColor.blue();
                        break;
                    default:
                        break;
                }

                int overlayedImageColorComponentValue = 0;
                if ( backImageColor.red() < 128 )
                {
                    overlayedImageColorComponentValue = qBound( minColorComponentValue,
                                                                qRound( ( foreImageColorComponentValue * backImageColorComponentValue ) / divisor ),
                                                                maxColorComponentValue );
                }
                else
                {
                    overlayedImageColorComponentValue = maxColorComponentValue
                                                        - ( qBound( minColorComponentValue,
                                                                    qRound( ( maxColorComponentValue - backImageColorComponentValue ) * ( maxColorComponentValue - foreImageColorComponentValue ) / divisor ),
                                                                    maxColorComponentValue ) );
                }

                switch ( colorComponentType )
                {
                    case ColorComponentTypeRed:
                        overlayedImagePixelColor.setRed( overlayedImageColorComponentValue );
                        break;
                    case ColorComponentTypeGreen:
                        overlayedImagePixelColor.setGreen( overlayedImageColorComponentValue );
                        break;
                    case ColorComponentTypeBlue:
                        overlayedImagePixelColor.setBlue( overlayedImageColorComponentValue );
                        break;
                    default:
                        break;
                }
            }

            overlayedImage.setPixel( x, y, overlayedImagePixelColor.rgba() );
        }
    }

    return overlayedImage;
}
