#ifndef UTILITIES_GENERAL_UTILITIES_H
#define UTILITIES_GENERAL_UTILITIES_H

#include <QRect>

namespace Utilities {

class GeneralUtilities
{
public:
    static QRect primaryDesktopRect();

private:
    GeneralUtilities();
};

}

#endif // UTILITIES_GENERAL_UTILITIES_H
