#include "MathUtilities.h"

#include "GLTools.h"

#include <glm/gtx/epsilon.hpp>
#include <glm/gtx/projection.hpp>

#include <QPoint>
#include <QMatrix>

using namespace Utilities;

// don't define
// MathUtilities::MathUtilities()
// {
// }

QRect MathUtilities::scaledConcentricRect(
        const QRect& rect,
        const qreal scaleFactor )
{
    if ( ! rect.isValid()
         || ( scaleFactor < 0.0
              || scaleFactor > 1.0 ) )
    {
        return QRect();
    }

    const QPoint center = rect.center();
    QMatrix m;
    m.translate( center.x(), center.y() );
    m.scale( scaleFactor,
             scaleFactor );
    m.translate( -center.x(), -center.y() );

    return m.mapRect( rect );
}

qreal MathUtilities::to360DegreesAngle( const qreal angleDegrees )
{
    qreal angle360Degrees = angleDegrees;
    if ( qFuzzyCompare( qAbs( angleDegrees ), 360.0 )
         || qAbs( angleDegrees ) > 360.0 )
    {
        angle360Degrees = int( qAbs( angleDegrees ) ) % 360 * angle360Degrees / qAbs( angleDegrees );
    }

    return angle360Degrees;
}

int MathUtilities::boundedRandom( const int min, const int max )
{
    int randomNumber = 0;
    if ( min < max )
    {
        return qrand() % ( max - min + 1 ) + min;
    }
    else if ( min == max )
    {
        randomNumber = min;
    }

    return randomNumber;
}

void MathUtilities::convertDoubleToSinglePrecisionV( float* const singlePrecision, const double* const doublePrecision, const int length )
{
    for ( int i = 0; i < length; ++ i )
    {
        singlePrecision[ i ] = static_cast< float > ( doublePrecision[ i ] );
    }
}

QList< glm::vec3 > MathUtilities::gramSchmidtOrthogonalization( const QList< glm::vec3 >& linearlyIndependentVectors )
{
    const int linearlyIndependentVectorsCount = linearlyIndependentVectors.size();
    if ( linearlyIndependentVectorsCount == 0 )
    {
        return QList< glm::vec3 >();
    }

    QList< glm::vec3 > orthogonalizedVectors;
    orthogonalizedVectors.reserve( linearlyIndependentVectorsCount );
    orthogonalizedVectors.append( linearlyIndependentVectors.first() );
    for ( int i = 1; i < linearlyIndependentVectorsCount; ++ i )
    {
        const glm::vec3& linearlyIndependentVector = linearlyIndependentVectors[ i ];
        glm::vec3 orthogonalizedVector = linearlyIndependentVector;
        for ( int j = 0; j < i; ++ j )
        {
            const glm::vec3 projectionVector = glm::proj( linearlyIndependentVector, orthogonalizedVectors[ j ] );
            if ( ! glm::any( glm::isnan( projectionVector ) ) && ! glm::any( glm::isinf( projectionVector ) ) )
            {
                orthogonalizedVector -= projectionVector;
            }
        }

        orthogonalizedVectors.append( orthogonalizedVector );
    }

    return orthogonalizedVectors;
}

glm::vec2 MathUtilities::safeNormalize( const glm::vec2& v )
{
    return MathUtilities::hasZeroLength( v )
           || glm::any( glm::isinf( v ) )
           || glm::any( glm::isnan( v ) )
           ? v
           : glm::normalize( v );
}

glm::vec3 MathUtilities::safeNormalize( const glm::vec3& v )
{
    return MathUtilities::hasZeroLength( v )
           || glm::any( glm::isinf( v ) )
           || glm::any( glm::isnan( v ) )
           ? v
           : glm::normalize( v );
}

glm::vec4 MathUtilities::safeNormalize( const glm::vec4& v )
{
    return MathUtilities::hasZeroLength( v )
           || glm::any( glm::isinf( v ) )
           || glm::any( glm::isnan( v ) )
           ? v
           : glm::normalize( v );
}

bool MathUtilities::epsilonEqual( const glm::vec2& v1, const glm::vec2& v2 )
{
    return glm::all( glm::epsilonEqual( v1, v2, glm::epsilon< glm::vec2::value_type >() ) );
}

bool MathUtilities::epsilonEqual( const glm::vec3& v1, const glm::vec3& v2 )
{
    return glm::all( glm::epsilonEqual( v1, v2, glm::epsilon< glm::vec3::value_type >() ) );
}

bool MathUtilities::epsilonEqual( const glm::vec4& v1, const glm::vec4& v2 )
{
    return glm::all( glm::epsilonEqual( v1, v2, glm::epsilon< glm::vec4::value_type >() ) );
}

bool MathUtilities::hasZeroLength( const glm::vec2& v )
{
    return MathUtilities::epsilonEqual( v, glm::vec2( 0.0f ) );
}

bool MathUtilities::hasZeroLength( const glm::vec3& v )
{
    return MathUtilities::epsilonEqual( v, glm::vec3( 0.0f ) );
}

bool MathUtilities::hasZeroLength( const glm::vec4& v )
{
    return MathUtilities::epsilonEqual( v, glm::vec4( 0.0f ) );
}

bool MathUtilities::isUnit( const glm::vec2& v )
{
    return glm::length( v ) <= 1.0f;
}

bool MathUtilities::isUnit( const glm::vec3& v )
{
    return glm::length( v ) <= 1.0f;
}

bool MathUtilities::isUnit( const glm::vec4& v )
{
    return glm::length( v ) <= 1.0f;
}

int MathUtilities::vertexIndex( const QList< glm::vec2 >& vertices, const glm::vec2& v )
{
    return vertices.indexOf( v );
}

int MathUtilities::vertexIndex( const QList< glm::vec3 >& vertices, const glm::vec3& v )
{
    return vertices.indexOf( v );
}

int MathUtilities::vertexIndex( const QList< glm::vec4 >& vertices, const glm::vec4& v )
{
    return vertices.indexOf( v );
}

bool MathUtilities::fuzzyContainedInRegion(
        const qreal minValue,
        const qreal value,
        const qreal maxValue )
{
    return qFuzzyCompare( value, minValue )
           || ( minValue <= value && value <= maxValue )
           || qFuzzyCompare( value, maxValue );
}

bool operator ==( const glm::vec2& v1, const glm::vec2& v2 )
{
    return MathUtilities::epsilonEqual( v1, v2 );
}

bool operator !=( const glm::vec2& v1, const glm::vec2& v2 )
{
    return ! ::operator ==( v1, v2 );
}

bool operator ==( const glm::vec3& v1, const glm::vec3& v2 )
{
    return MathUtilities::epsilonEqual( v1, v2 );
}

bool operator !=( const glm::vec3& v1, const glm::vec3& v2 )
{
    return ! ::operator ==( v1, v2 );
}

bool operator ==( const glm::vec4& v1, const glm::vec4& v2 )
{
    return MathUtilities::epsilonEqual( v1, v2 );
}

bool operator !=( const glm::vec4& v1, const glm::vec4& v2 )
{
    return ! ::operator ==( v1, v2 );
}
