#ifndef UTILITIES_IMAGEUTILITIES_H
#define UTILITIES_IMAGEUTILITIES_H

#include <QColor>
#include <QImage>
#include <glm/vec3.hpp>

namespace Utilities {

class ImageUtilities
{
public:
    enum ColorComponentType
    {
        ColorComponentTypeRed,
        ColorComponentTypeGreen,
        ColorComponentTypeBlue
    };

public:
    static int intColorComponentMin();
    static int intColorComponentMax();

    static glm::vec3::value_type realFromIntColorComponent( const int intColorComponent );
    static int intFromRealColorComponent( const glm::vec3::value_type realColorComponent );

    static glm::vec3 realFromIntColor( const QColor& intColor );
    static QColor intFromRealColor( const glm::vec3& realColor );

    static QImage toGrayscale( const QImage& image );
    static QImage toNormalMap( const QImage& image, const qreal scaleFactor );
    static QImage toEnhancedNormalMap( const QImage& image, const qreal scaleFactor );

    static glm::vec3 normalFromColor( const QColor& color );
    static QColor colorFromNormal( const glm::vec3& point );
    static int colorComponentFromGeometricComponent( const glm::vec3::value_type& geometricComponent );
    static glm::vec3::value_type geometricComponentFromColorComponent( const int colorComponent );

    static QImage blurredImage( const QImage& image,
                                const qreal radius,
                                const bool quality = true,
                                const bool alphaOnly = false,
                                const int transposed = 0 );
    static QImage overlayImages( const QImage& imageFore,
                                 const QImage& imageBack );

private:
    ImageUtilities();
};

}

#endif // UTILITIES_IMAGEUTILITIES_H
