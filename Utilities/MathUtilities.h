#ifndef UTILITIES_MATHUTILITIES_H
#define UTILITIES_MATHUTILITIES_H

#include <glm/vec2.hpp>
#include <glm/vec3.hpp>
#include <glm/vec4.hpp>

#include <QtGlobal>
#include <QRect>
#include <QList>

namespace Utilities {

class MathUtilities
{
public:
    static QRect scaledConcentricRect( const QRect& rect, const qreal scaleFactor );

    static qreal to360DegreesAngle( const qreal angleDegrees );
    static int boundedRandom( const int min, const int max );

    static void convertDoubleToSinglePrecisionV( float* const singlePrecision, const double* const doublePrecision, const int length );

    static QList< glm::vec3 > gramSchmidtOrthogonalization( const QList< glm::vec3 >& linearlyIndependentVectors );

    static glm::vec2 safeNormalize( const glm::vec2& v );
    static glm::vec3 safeNormalize( const glm::vec3& v );
    static glm::vec4 safeNormalize( const glm::vec4& v );

    static bool epsilonEqual( const glm::vec2& v1, const glm::vec2& v2 );
    static bool epsilonEqual( const glm::vec3& v1, const glm::vec3& v2 );
    static bool epsilonEqual( const glm::vec4& v1, const glm::vec4& v2 );

    static bool hasZeroLength( const glm::vec2& v );
    static bool hasZeroLength( const glm::vec3& v );
    static bool hasZeroLength( const glm::vec4& v );

    static bool isUnit( const glm::vec2& v );
    static bool isUnit( const glm::vec3& v );
    static bool isUnit( const glm::vec4& v );

    static int vertexIndex( const QList< glm::vec2 >& vertices, const glm::vec2& v );
    static int vertexIndex( const QList< glm::vec3 >& vertices, const glm::vec3& v );
    static int vertexIndex( const QList< glm::vec4 >& vertices, const glm::vec4& v );

    static bool fuzzyContainedInRegion( const qreal minValue, const qreal value, const qreal maxValue );

private:
    MathUtilities();
};

}

bool operator ==( const glm::vec2& v1, const glm::vec2& v2 );
bool operator !=( const glm::vec2& v1, const glm::vec2& v2 );

bool operator ==( const glm::vec3& v1, const glm::vec3& v2 );
bool operator !=( const glm::vec3& v1, const glm::vec3& v2 );

bool operator ==( const glm::vec4& v1, const glm::vec4& v2 );
bool operator !=( const glm::vec4& v1, const glm::vec4& v2 );

#endif // UTILITIES_MATHUTILITIES_H
