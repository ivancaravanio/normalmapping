#ifndef VEC3EDITOR_H
#define VEC3EDITOR_H

#include <glm/vec3.hpp>

#include <QWidget>

QT_BEGIN_NAMESPACE
class QDoubleSpinBox;
class QLabel;
QT_END_NAMESPACE

QT_USE_NAMESPACE

class Vec3Editor : public QWidget
{
    Q_OBJECT
public:
    explicit Vec3Editor( QWidget* parent = 0 );
    virtual ~Vec3Editor();

    void setMinimum( const double aMinimum );
    double minimum() const;

    void setMaximum( const double aMaximum );
    double maximum() const;

    void setRange( const double aMinimum, const double aMaximum );

    void setDecimalsCount( const int aDecimalsCount );
    int decimalsCount() const;

    void setVec3( const glm::vec3& aVec3 );
    glm::vec3 vec3() const;

    void retranslateUi();

signals:
    void valueChanged();
    void valueEdited();

private:
    enum AxisIndex
    {
        AxisIndexX = 0,
        AxisIndexY = 1,
        AxisIndexZ = 2,
        AxesCount  = 3
    };

private:
    QLabel*         m_axisLabels[ AxesCount ];
    QDoubleSpinBox* m_axisEditors[ AxesCount ];
};

#endif // VEC3EDITOR_H
