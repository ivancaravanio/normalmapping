#include "MainControl.h"

#include "Utilities/GeneralUtilities.h"
#include "Utilities/ImageUtilities.h"
#include "Utilities/MathUtilities.h"

#define UNIT_TESTS_ENABLED

#ifdef UNIT_TESTS_ENABLED
#include "CommonEntities.h"
#include "Mesh3.h"
#include "Triangle2.h"
#include "Triangle3.h"
#endif

#include <QApplication>
#include <QDir>
#include <QImage>

#ifdef UNIT_TESTS_ENABLED
#include <QDebug>
#include <QStandardPaths>
#include <QStringBuilder>
#endif

#ifdef UNIT_TESTS_ENABLED
void testTangentBitangentCalculator();
void performUnitTests();
void testNormalMapGenerator();
#endif

using namespace Utilities;

int main( int argc, char* argv[] )
{
    QApplication a( argc, argv );

#ifdef UNIT_TESTS_ENABLED
    // testTangentBitangentCalculator();
    // performUnitTests();
    // testNormalMapGenerator();
#endif

    MainControl w;
    w.show();

    const QRect partialDesktopRect = MathUtilities::scaledConcentricRect( GeneralUtilities::primaryDesktopRect(), 0.5 );
    w.setGeometry( partialDesktopRect );

    return a.exec();
}

#ifdef UNIT_TESTS_ENABLED
void performUnitTests()
{
    const Triangle3 tri1(
                glm::vec3( 1.0f, 0.0f, 1.0f ),
                glm::vec3( 0.0f, 0.0f, 1.0f ),
                glm::vec3( 0.0f ) );

    qDebug() << "angle 90 expected:" << tri1.angleAtVertex( glm::vec3( 0.0f, 0.0f, 1.0f ) ) << endl
             << "angle 45 expected:" << tri1.angleAtVertex( glm::vec3( 0.0f, 0.0f, 0.0f ) );

    const Triangle3 tri2(
                glm::vec3( 0.0f ),
                glm::vec3( 0.0f, 0.0f, 1.0f ),
                glm::vec3( 0.0f, 1.0f, 0.0f ) );

    const Triangle3 tri3(
                glm::vec3( 0.0f, 0.0f, 1.0f ),
                glm::vec3( 0.0f, 1.0f, 1.0f ),
                glm::vec3( 0.0f, 1.0f, 0.0f ) );

    const Mesh3 mesh( QList< QPair< Triangle3, Triangle2 > >()
                      << QPair< Triangle3, Triangle2 >( tri1, Triangle2() )
                      << QPair< Triangle3, Triangle2 >( tri2, Triangle2() )
                      << QPair< Triangle3, Triangle2 >( tri3, Triangle2() ) );

    const QList< QPair< Triangle3, Triangle2 > >& tris = mesh.triangles();
    const int trisCount = tris.size();
    for ( int i = 0; i < trisCount; ++ i )
    {
        const QPair< Triangle3, Triangle2 >& tri = tris[ i ];
        qDebug() << ( i + 1 ) << ':' << tri.first;
    }

    const glm::vec3 n = mesh.normalAtVertex( glm::vec3( 0.0f, 0.0f, 1.0f ) );

    qDebug() << n;
}

void testTangentBitangentCalculator()
{
    const glm::vec3 v0( 0.0f, 0.0f, 0.0f );
    const glm::vec3 v1( 1.0f, 0.0f, 0.0f );
    const glm::vec3 v2( 1.0f, 1.0f, 0.0f );
    const glm::vec3 v3( 0.0f, 1.0f, 0.0f );

    const glm::vec2 vt0( 0.0f, 0.0f );
    const glm::vec2 vt1( 1.0f, 0.0f );
    const glm::vec2 vt2( 1.0f, 1.0f );
    const glm::vec2 vt3( 0.0f, 1.0f );

    QList< glm::vec3 > allVertices;
    allVertices << v0
                << v1
                << v2
                << v3;

    const Triangle3 tri1(
                v0,
                v1,
                v3 );
    const Triangle2 tri1Tex(
                vt0,
                vt1,
                vt3 );

    const Triangle3 tri2(
                v1,
                v2,
                v3 );
    const Triangle2 tri2Tex(
                vt1,
                vt2,
                vt3 );

    const Mesh3 mesh( QList< QPair< Triangle3, Triangle2 > >()
                      << QPair< Triangle3, Triangle2 >( tri1, tri1Tex )
                      << QPair< Triangle3, Triangle2 >( tri2, tri2Tex ) );
    const int allVerticesCount = allVertices.size();
    for ( int i = 0; i < allVerticesCount; ++ i )
    {
        const glm::vec3& v         = allVertices[ i ];
        const glm::vec3  normal    = mesh.normalAtVertex( v );
        const glm::vec4  tangent   = mesh.tangentAtVertex( v );
        const glm::vec4  bitangent = mesh.bitangentAtVertex( v );

        qDebug() << i << "===========================" << endl
                 << "v         =" << v << endl
                 << "normal    =" << normal << endl
                 << "tangent   =" << tangent << endl
                 << "bitangent =" << bitangent;
    }
}

void testNormalMapGenerator()
{
    QApplication* const app = qApp;
    const QString textureFilePath = ( app == 0
                                      ? QDir::currentPath()
                                      : app->applicationDirPath() ) % QDir::separator()
                                    % QString( "textures" ) % QDir::separator() % "brick_wall.jpg";
    const QImage textureImage( textureFilePath, "JPG" );

    const QString imagesDirPath = QStandardPaths::writableLocation( QStandardPaths::DesktopLocation ) + QDir::separator() + "NormalMapping";
    bool shouldCreateTestImages = true;
    if ( ! QDir( imagesDirPath ).exists() )
    {
        if ( QDir().mkpath( imagesDirPath ) )
        {
            shouldCreateTestImages = false;
        }
    }

    if ( shouldCreateTestImages )
    {
//        const QImage blurredImage = ImageUtilities::blurredImage( textureImage, 20.0 );
//        blurredImage.save(
//                    imagesDirPath + QDir::separator() + "brick_wall_blurred.jpg",
//                    "JPG" );

//        const QImage grayscaleImage = ImageUtilities::toGrayscale( textureImage );
//        grayscaleImage.save( imagesDirPath + QDir::separator() + "brick_wall_gs.jpg",
//                             "JPG" );

//        const QImage normalMap = ImageUtilities::toNormalMap( textureImage, 3.0 );
//        normalMap.save( imagesDirPath + QDir::separator() + "brick_wall_nm.jpg",
//                        "JPG" );

        const QImage normalMapEnhanced1 = ImageUtilities::toEnhancedNormalMap( textureImage, 3.0 );
        normalMapEnhanced1.save( imagesDirPath + QDir::separator() + "brick_wall_nme_pos3.jpg",
                                "JPG" );
        const QImage normalMapEnhanced2 = ImageUtilities::toEnhancedNormalMap( textureImage, -3.0 );
        normalMapEnhanced2.save( imagesDirPath + QDir::separator() + "brick_wall_nme_neg3.jpg",
                                "JPG" );
    }
}
#endif
