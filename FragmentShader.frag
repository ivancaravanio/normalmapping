#version 330 core

/* 0: in
 * 1: out
 * 2: uniform
 */

// 0: in
in vec3 vfVertexPos;
in vec2 vfVertexTexCoord;

in vec3 vfVertexNormalOrg;
in vec3 vfVertexTangentOrg;
in vec3 vfVertexBitangentOrg;

in vec3 vfVertexNormal;
in vec3 vfVertexTangent;
in vec3 vfVertexBitangent;

in vec3 vfEyePos;

in vec3 vfLightDirTangentSpace;
in vec3 vfEyeVectorTangentSpace;

// 1: out
layout (location = 0) out vec4 fColor;

// 2: uniform
uniform sampler2D colorTextureSampler;
uniform sampler2D normalMapTextureSampler;

uniform vec3 ambientColor;
uniform vec3 lightColor;
uniform vec3 lightPos;
uniform vec3 eyePos;
uniform float materialShininessLevel;
uniform float lightIntensityLevel;
uniform int effectType;

const float kc = 2.0;
const float kl = 0.5;
const float kq = 1.0;

vec3 geometryToColor( vec3 geometry )
{
    return ( geometry + 1.0f ) / 2.0f;
}

vec4 geometryToColor( vec4 geometry )
{
    return ( geometry + 1.0f ) / 2.0f;
}

vec3 colorToGeometry( vec3 color )
{
    return color * 2.0f - 1.0f;
}

vec4 colorToGeometry( vec4 color )
{
    return color * 2.0f - 1.0f;
}

struct Material
{
    vec3 emissiveReflectance;
    vec3 ambientReflectance;
    vec3 diffuseReflectance;
    vec3 specularReflectance;
    float shininessLevel;
};

struct PointLight
{
    vec3  position;
    vec3  color;
    float constantAttenuation;
    float linearAttenuation;
    float quadraticAttenuation;
    float intensityLevel;
};

vec4 phongIlluminationModel(
        vec3       surfaceNormalVector,  // object space
        vec4       surfaceMaterialColor,
        Material   material,
        PointLight light,
        vec3       globalAmbientColor,
        vec3       eyeVector )         // world space
{
    vec4 emissiveColor = vec4( 0.0 ); // if the object being illuminated acts itself as a light source
    vec3 ambientColor  = material.ambientReflectance * globalAmbientColor; // depends on position: light(0), eye(0)

    vec3 lightVector = light.position;
    float lightSurfaceDistance = length( lightVector );
    float attenuation = 1.0f / ( light.constantAttenuation
                                 + light.linearAttenuation * lightSurfaceDistance
                                 + light.quadraticAttenuation * lightSurfaceDistance * lightSurfaceDistance );
    vec3 finalLightColor = light.intensityLevel * attenuation * light.color;

    vec3 normalizedLightVector = normalize( lightVector );
    vec3 normal = normalize( surfaceNormalVector );
    float diffuseColorIntensityLevel = max( 0.0f, dot( normalizedLightVector, normal ) );

    vec3 diffuseColor = material.diffuseReflectance * diffuseColorIntensityLevel * finalLightColor; // depends on position: light(1), eye(0)

    vec3 specularColor = vec3( 0.0 ); // depends on position: light(1), eye(1)
    if ( abs( diffuseColorIntensityLevel ) >= 1E-6f )
    {
        vec3 normalizedEyeVector          = normalize( eyeVector );
        vec3 halfVector                   = normalize( normalizedLightVector + normalizedEyeVector );
        float specularColorIntensityLevel = max( 0.0f, dot( normal, halfVector ) );
        float specularColorIntensityPower = pow( specularColorIntensityLevel, material.shininessLevel );
        specularColor                     = material.specularReflectance * specularColorIntensityPower * finalLightColor;
    }

    return vec4( clamp( emissiveColor.rgb
                        + ( ambientColor + diffuseColor ) * surfaceMaterialColor.rgb
                        + specularColor,
                        vec3( 0.0f ),
                        vec3( 1.0f ) ),
                 surfaceMaterialColor.a );
}

vec4 phongIlluminationModel(
    vec3 aPos,
    vec4 aColor,
    vec3 aNormal,
    vec3 aEyePos,
    vec3 aLightPos,
    vec3 aLightColor,
    float aLightIntensityLevel,
    float aKc,
    float aKl,
    float aKq )
{
    vec3 lightDir = aLightPos - aPos;
    float lightVertexDistance = length( lightDir );
    float attenuation = 1.0f / (   aKc
                                 + aKl * lightVertexDistance
                                 + aKq * lightVertexDistance * lightVertexDistance );

    vec3 finalLightColor = aLightIntensityLevel * attenuation * aLightColor;

    vec3 normalizedLightDir = normalize( lightDir );
    vec3 normalizedNormalVector = normalize( aNormal );
    float diffuseColorIntensityLevel = max( dot( normalizedLightDir, normalizedNormalVector ), 0.0f );

    vec3 normalizedeyeVector = normalize( vfEyePos - aPos );
    float specularColorIntensityLevel = abs( diffuseColorIntensityLevel ) >= 1E-6f
                                        ? max( dot( normalize( normalizedLightDir + normalizedeyeVector ), normalizedNormalVector ), 0.0f )
                                        : 0.0f;

    vec3 diffuseColor = diffuseColorIntensityLevel * finalLightColor;
    vec3 specularColor = diffuseColorIntensityLevel > 1E-6f
                           ? pow( specularColorIntensityLevel, materialShininessLevel ) * finalLightColor
                           : vec3( 0.0f );

    return vec4( clamp( aColor.rgb * ( ambientColor + diffuseColor )
                        + specularColor,
                        vec3( 0.0f ),
                        vec3( 1.0f ) ),
                 aColor.a );
}

void main()
{
    PointLight light;
    light.color                = lightColor;
    light.constantAttenuation  = kc;
    light.linearAttenuation    = kl;
    light.quadraticAttenuation = kq;
    light.intensityLevel       = lightIntensityLevel;

    Material material;
    material.emissiveReflectance = vec3( 1.0f );
    material.ambientReflectance  = vec3( 1.0f );
    material.diffuseReflectance  = vec3( 1.0f );
    material.specularReflectance = vec3( 1.0f );
    material.shininessLevel      = materialShininessLevel;

    vec3 vertexNormal = vec3( 0.0 );
    light.position    = vec3( 0.0 );
    vec3 eyeVector    = vec3( 0.0 );
    switch ( effectType )
    {
        case 0:
            // normal - world space
            // light  - world space
            // eye    - world space
            vertexNormal   = vfVertexNormal;
            light.position = lightPos - vfVertexPos;
            eyeVector      = vfEyePos - vfVertexPos;
            break;
        case 1:
            // normal - tangent space
            // light  - tangent space
            // eye    - tangent space
            vertexNormal   = vec3( normalize( colorToGeometry( texture2D( normalMapTextureSampler, vfVertexTexCoord ) ) ) );
            light.position = vfLightDirTangentSpace;
            eyeVector      = vfEyeVectorTangentSpace;
            break;
        case 2:
            // normal - tangent space
            // light  - world space
            // eye    - world space
            vertexNormal   = vec3( normalize( colorToGeometry( texture2D( normalMapTextureSampler, vfVertexTexCoord ) ) ) );
            light.position = lightPos - vfVertexPos;
            eyeVector      = vfEyePos - vfVertexPos;
            break;
    }

    fColor = phongIlluminationModel( vertexNormal,
                                     texture2D( colorTextureSampler, vfVertexTexCoord ),
                                     material,
                                     light,
                                     ambientColor,
                                     eyeVector );

    // fColor = vec4( geometryToColor( vfVertexTangent ), 1.0 );
    // fColor = vec4( geometryToColor( vfVertexBitangent ), 1.0 );
    // fColor = vec4( geometryToColor( vfVertexNormal ), 1.0 );

    // fColor = vec4( geometryToColor( any( equal( vfVertexTangentOrg, vec3( 0.0, 0.0, 0.0 ) ) )
    //                                 ? vfVertexTangentOrg
    //                                 : normalize( vfVertexTangentOrg ) ), 1.0 );
}
