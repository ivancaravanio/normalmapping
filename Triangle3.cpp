#include "Triangle3.h"

#include "CommonEntities.h"
#include "Utilities/MathUtilities.h"

#include <glm/glm.hpp>
#include <glm/gtx/normal.hpp>
#include <glm/gtx/vector_angle.hpp>

#include <QTextStream>
#include <QtGlobal>

using namespace Utilities;

const int Triangle3::VERTICES_COUNT = 3;

Triangle3::Triangle3( const glm::vec3& aV0,
                      const glm::vec3& aV1,
                      const glm::vec3& aV2 )
{
    m_vertices.reserve( VERTICES_COUNT );
    m_vertices << aV0
               << aV1
               << aV2;
}

Triangle3::~Triangle3()
{
}

void Triangle3::setVertices(
        const glm::vec3& aV0,
        const glm::vec3& aV1,
        const glm::vec3& aV2 )
{
    this->setV0( aV0 );
    this->setV1( aV1 );
    this->setV2( aV2 );
}

const QList< glm::vec3 >& Triangle3::vertices() const
{
    return m_vertices;
}

void Triangle3::setVertexAtIndex( const int index, const glm::vec3& v )
{
    m_vertices[ Triangle3::cyclicVertexIndex( index ) ] = v;
}

glm::vec3 Triangle3::vertexAtIndex( const int index ) const
{
    return m_vertices[ Triangle3::cyclicVertexIndex( index ) ];
}

void Triangle3::setV0( const glm::vec3& aV0 )
{
    this->setVertexAtIndex( 0, aV0 );
}

glm::vec3 Triangle3::v0() const
{
    return this->vertexAtIndex( 0 );
}

void Triangle3::setV1( const glm::vec3& aV1 )
{
    this->setVertexAtIndex( 1, aV1 );
}

glm::vec3 Triangle3::v1() const
{
    return this->vertexAtIndex( 1 );
}

void Triangle3::setV2( const glm::vec3& aV2 )
{
    this->setVertexAtIndex( 2, aV2 );
}

glm::vec3 Triangle3::v2() const
{
    return this->vertexAtIndex( 2 );
}

void Triangle3::reverse()
{
    qSwap( m_vertices[ 0 ], m_vertices[ 2 ] );
}

glm::vec3 Triangle3::normal() const
{
    return glm::triangleNormal( this->v0(),
                                this->v1(),
                                this->v2() );
}

int Triangle3::indexOfVertex( const glm::vec3& v ) const
{
    return MathUtilities::vertexIndex( m_vertices, v );
}

bool Triangle3::isVertex( const glm::vec3& v ) const
{
    return this->indexOfVertex( v ) >= 0;
}

bool Triangle3::contains( const glm::vec3& v ) const
{
    const glm::vec3 v0 = this->v0();
    const glm::vec3 v1 = this->v1();
    const glm::vec3 v2 = this->v2();

    const bool b0 = Triangle3::sign( v, v0, v1 ) < 0.0f;
    const bool b1 = Triangle3::sign( v, v1, v2 ) < 0.0f;
    const bool b2 = Triangle3::sign( v, v2, v0 ) < 0.0f;

    return ( b0 == b1 ) && ( b1 == b2 );
}

bool Triangle3::contains( const Triangle3& t ) const
{
    const int otherTriangleVerticesCount = t.vertices().size();
    for ( int i = 0; i < otherTriangleVerticesCount; ++ i )
    {
        if ( ! this->contains( t.vertexAtIndex( i ) ) )
        {
            return false;
        }
    }

    return true;
}

glm::vec3::value_type Triangle3::angleAtVertex( const glm::vec3& v ) const
{
    const int vIndex = this->indexOfVertex( v );
    if ( vIndex < 0 )
    {
        return 0.0f;
    }

    return glm::angle( glm::normalize( this->vertexAtIndex( vIndex - 1 ) - v ),
                       glm::normalize( this->vertexAtIndex( vIndex + 1 ) - v ) );
}

glm::vec3::value_type Triangle3::area() const
{
    const glm::vec3 v0 = this->v0();
    const glm::vec3 v1 = this->v1();
    const glm::vec3 v2 = this->v2();

    return glm::length( glm::cross( v1 - v0, v2 - v0 ) ) / 2.0f;
}

bool Triangle3::operator==( const Triangle3& other ) const
{
    const int verticesCount = m_vertices.size();
    if ( verticesCount != other.vertices().size() )
    {
        return false;
    }

    for ( int i = 0; i < verticesCount; ++ i )
    {
        if ( ! MathUtilities::epsilonEqual(
                 this->vertexAtIndex( i ),
                 other.vertexAtIndex( i ) ) )
        {
            return false;
        }
    }

    return true;
}

bool Triangle3::operator!=( const Triangle3& other ) const
{
    return ! ( *this == other );
}

QString Triangle3::toString() const
{
    QString s;

    QTextStream ts( &s );

    ts << QT_STRINGIFY2( Triangle3 ) << '(' << endl
       << "\tv0: " << CommonEntities::vecToString( this->v0() ) << endl
       << "\tv1: " << CommonEntities::vecToString( this->v1() ) << endl
       << "\tv2: " << CommonEntities::vecToString( this->v2() ) << " )";

    return s;
}

int Triangle3::cyclicVertexIndex( const int vertexIndex ) const
{
    const int verticesCount = m_vertices.size();
    return vertexIndex >= 0
           ? vertexIndex % verticesCount
           : ( verticesCount - qAbs( vertexIndex ) % verticesCount );
}

glm::vec3::value_type Triangle3::sign(
        const glm::vec3& v0,
        const glm::vec3& v1,
        const glm::vec3& v2 )
{
    return   ( v0.x - v2.x ) * ( v1.y - v2.y )
           - ( v1.x - v2.x ) * ( v0.y - v2.y );
}

#ifndef QT_NO_DEBUG_STREAM
QDebug operator<<( QDebug qdebug, const Triangle3& t )
{
    return qdebug << t.toString();
}
#endif
