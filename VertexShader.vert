#version 330 core

/* 0: in
 * 1: out
 * 2: uniform
 */

// 0: in
layout (location = 0) in vec3 vVertexPos;      // The object-space vertex position.
layout (location = 1) in vec3 vVertexNormal;   // The object-space vertex normal.
layout (location = 2) in vec4 vVertexTangent;  // The object-space vertex tangent. w-coordinate contains the handedness.
layout (location = 3) in vec2 vVertexTexCoord;

// 1: out
out vec3 vfVertexPos;
out vec2 vfVertexTexCoord;

out vec3 vfVertexNormalOrg;
out vec3 vfVertexTangentOrg;
out vec3 vfVertexBitangentOrg;

out vec3 vfVertexNormal;
out vec3 vfVertexTangent;
out vec3 vfVertexBitangent;

out vec3 vfEyePos;

out vec3 vfLightDirTangentSpace;
out vec3 vfEyeDirTangentSpace;

// 2: uniform
uniform mat4 modelMatrix      = mat4( 1.0f ); // object -> world  space ( only rotation and reverse scaling )
uniform mat3 normalMatrix     = mat3( 1.0f ); // object -> world  space without scaling
uniform mat4 viewMatrix       = mat4( 1.0f ); // world  -> eye    space
uniform mat4 projectionMatrix = mat4( 1.0f ); // eye    -> screen space

uniform vec3 lightPos;
uniform vec3 eyePos;

void main()
{
    vfVertexNormalOrg    = vVertexNormal;
    vfVertexTangentOrg   = vVertexTangent.xyz;
    vfVertexBitangentOrg = cross( vfVertexNormalOrg, vfVertexTangentOrg ) * vVertexTangent.w;

    vec3 vertexPos       = vec3( modelMatrix * vec4( vVertexPos, 1.0f ) );
    vec3 vertexNormal    = normalMatrix * vVertexNormal;
    vec3 vertexTangent   = normalMatrix * vVertexTangent.xyz;
    vec3 vertexBitangent = cross( vertexNormal, vertexTangent ) * vVertexTangent.w;

    vfVertexPos          = vertexPos;
    vfVertexNormal       = vertexNormal;
    vfVertexTexCoord     = vVertexTexCoord;

    vfVertexTangent      = vertexTangent;
    vfVertexBitangent    = vertexBitangent;

    mat3 tangentToObjectSpaceMatrix = mat3(
                vertexTangent,
                vertexBitangent,
                vertexNormal );

    // transpose = cheap inverse in the case of orthogonal matrix
    mat3 objectToTangentSpaceMatrix = transpose( tangentToObjectSpaceMatrix );

    vfLightDirTangentSpace = objectToTangentSpaceMatrix * ( lightPos - vertexPos );

    vfEyePos             = vec3( viewMatrix * vec4( eyePos, 1.0f ) );
    vfEyeDirTangentSpace = objectToTangentSpaceMatrix * ( vfEyePos - vertexPos );

    gl_Position = projectionMatrix * viewMatrix * vec4( vertexPos, 1.0f );
}
