#ifndef GLWIDGET_H
#define GLWIDGET_H

#include "GraphicsData.h"
#include "TextureUnit.h"
#include "Triangle2.h"
#include "Triangle3.h"

#include "ogl.h"
#ifdef USE_GLM
    #include <glm/mat4x4.hpp>
    #include <glm/vec3.hpp>
    #include <glm/vec4.hpp>
#else
    #include <QMatrix4x4>
    #include <QVector3D>
#endif

#include <QGLWidget>
#include <QHash>
#include <QList>
#include <QMap>
#include <QPair>
#include <QString>
#include <QVector>

QT_BEGIN_NAMESPACE
class QKeyEvent;
class QMouseEvent;
#ifndef QT_NO_WHEELEVENT
class QWheelEvent;
#endif
QT_END_NAMESPACE

QT_USE_NAMESPACE

class GLWidget : public QGLWidget
{
    Q_OBJECT

public:
    explicit GLWidget( QWidget* parent = 0 );
    explicit GLWidget( const QGLFormat& format, QWidget* parent = 0 );
    virtual ~GLWidget();

    void setGraphicsData( const GraphicsData& aGraphicsData );
    const GraphicsData& graphicsData() const;

    GraphicsData::EffectType effectType() const;
    QString textureFilePath( const GraphicsData::PartTypeIndex partTypeIndex ) const;
    qreal bumpSize() const;

public slots:
    void setEffectType( const GraphicsData::EffectType aEffectType );
    void setTextureFilePath(
            const GraphicsData::PartTypeIndex partTypeIndex,
            const QString& textureFilePath );
    void setBumpSize( const qreal aBumpSize );

protected:
    virtual void initializeGL();
    virtual void resizeGL( int w, int h );
    virtual void paintGL();

    virtual void keyPressEvent( QKeyEvent* event );
    virtual void keyReleaseEvent( QKeyEvent* event );
    virtual void mousePressEvent( QMouseEvent* event );
    virtual void mouseMoveEvent( QMouseEvent* event );
    virtual void mouseReleaseEvent( QMouseEvent* event );
#ifndef QT_NO_WHEELEVENT
    virtual void wheelEvent( QWheelEvent* event );
#endif

private:
    enum CubeSide
    {
        CubeSideFront  = 0,
        CubeSideBack   = 1,
        CubeSideLeft   = 2,
        CubeSideRight  = 3,
        CubeSideTop    = 4,
        CubeSideBottom = 5,
        CubeSidesCount = 6
    };

    enum GenericVertexAttributeIndex
    {
        GenericVertexAttributeIndexInvalid        = -1,
        GenericVertexAttributeIndexVertexPos      = 0,
        GenericVertexAttributeIndexVertexNormal   = 1,
        GenericVertexAttributeIndexVertexTangent  = 2,
        GenericVertexAttributeIndexVertexTexCoord = 3,
        GenericVertexAttributeIndicesCount        = 4
    };

    enum FragmentOutputVariableIndex
    {
        FragmentOutputVariableIndexInvalid = -1,
        FragmentOutputVariableIndexColor   = 0,
        FragmentOutputVariableIndicesCount = 1
    };

    typedef QMap< GLuint, QString > ShaderVariableInfoContainer;

    enum ShaderType
    {
        ShaderTypeInvalid,
        ShaderTypeVertex,
        ShaderTypeFragment,
        ShaderTypeGeometry,
        ShaderTypeTessellation,
        ShaderTypeCompute
    };

    struct ShaderInfo
    {
        ShaderType type;
        QString filePath;
        ShaderVariableInfoContainer variables;
    };

    // uniforms
    enum UniformVarIndex
    {
        UniformVarIndexInvalid                = -1,
        UniformVarIndexModelMatrix            = 0,
        UniformVarIndexNormalMatrix           = 1,
        UniformVarIndexViewMatrix             = 2,
        UniformVarIndexProjectionMatrix       = 3,
        UniformVarIndexAmbientColor           = 4,
        UniformVarIndexLightColor             = 5,
        UniformVarIndexLightPos               = 6,
        UniformVarIndexEyePos                 = 7,
        UniformVarIndexMaterialShininessLevel = 8,
        UniformVarIndexLightIntensityLevel    = 9,
        UniformVarIndexEffectType             = 10,
        UniformVarsCount                      = 11
    };

    enum MatrixDataIndex
    {
        MatrixDataIndexModel = 0,
        MatrixDataIndexView  = 1,
        MatricesDataCount    = 2
    };

    struct MatrixData
    {
        glm::mat4 completeMatrix() const;

        glm::mat4 keyMatrix;
        glm::mat4 mouseMatrix;
        glm::mat4 cacheMatrix;
    };

private:
    static const glm::vec3 s_xAxisDir;
    static const glm::vec3 s_yAxisDir;
    static const GLuint s_invalidShaderProgramHandle;
    static const QList< GenericVertexAttributeIndex > ALL_ORDERED_GENERIC_VERTEX_ATTRIBUTE_INDICES;

private:
    static QString genericVertexAttributeName( const GenericVertexAttributeIndex genericVertexAttributeIndex );

    void init();
    bool loadShaderPairWithAttributes( const ShaderInfo& vertexShaderInfo,
                                       const ShaderInfo& fragmentShaderInfo );

    static void printGLInfo();
    void printPostLinkGLInfo() const;
    void clearShaderProgram();
    bool isOpenGLVersion330CoreProfile() const;
    static QString shaderVariableTypeDescription( const GLenum shaderVariableType );

    static void logGlError( const QString& filePath, const int lineNumber );
    void bindTexturesForVaoIndex( const CommonEntities::VAOIndex vaoIndex ) const;
    bool bindTextureForTextureIndex( const CommonEntities::TextureIndex textureIndex ) const;

    static void axesFromTriangles( QVector< GLfloat >& verticesAxes,
                                   QVector< GLfloat >& vertexNormalsAxes,
                                   QVector< GLfloat >& vertexTangentsAxes,
                                   QVector< GLfloat >& vertexTextureCoordsAxes,
                                   const QList< QPair< Triangle3, Triangle2 > >& triangles,
                                   const QList< glm::vec3 >& uniqueVertices,
                                   const QList< glm::vec3 >& uniqueVertexNormals,
                                   const QList< glm::vec4 >& uniqueVertexTangents );
    void applyGraphicsData(
            const GraphicsData& aGraphicsData,
            const bool force,
            const bool shouldUpdateGL = true );
    bool applyEffectType( const GraphicsData::EffectType aEffectType,
                          const bool force,
                          const bool shouldUpdateGL = true );
    void applyMatrices( const bool shouldUpdateGL = true );

    static QString uniformVarName( const UniformVarIndex uniformVarIndex );

    static void enableGenericVertexAttributeArrays( const bool enable );

    static GLuint activeShaderProgramHandle();
    const QHash< Qt::Key, QPair< glm::vec3, GLfloat > >& arrowKeysMatrixDeltaChanges();

    void setTexture(
            const CommonEntities::TextureIndex textureIndex,
            const QImage& textureImage );

private:
    MatrixData m_matricesData[ MatricesDataCount ];

    GLuint m_vbos[ CommonEntities::VBOIndicesCount ];
    GLuint m_vaos[ CommonEntities::VAOIndicesCount ];

    QPair< GLint, QString > m_uniformVars[ UniformVarsCount ];

    GLuint m_shaderProgramHandle;

    QPoint m_mousePressPos;

    QList< TextureUnit > m_textureUnits;
    bool m_isOpenGLVersion330CoreProfileUsed;

    GraphicsData m_graphicsData;
};

#endif // GLWIDGET_H
