#ifndef PARALLAXMAPPING_PRECOMPILED_H
#define PARALLAXMAPPING_PRECOMPILED_H

#include "ogl.h"

#include <qmath.h>

#ifdef __cplusplus
#ifdef USE_GLM
    #include <glm/glm.hpp>
    #include <glm/gtc/matrix_transform.hpp>
    #include <glm/gtc/type_ptr.hpp>
    #include <glm/gtx/epsilon.hpp>
    #include <glm/gtx/normal.hpp>
    #include <glm/gtx/normal.hpp>
    #include <glm/gtx/vector_angle.hpp>
    #include <glm/vec3.hpp>
#else
    #include <QMatrix>
    #include <QMatrix4x4>
#endif

#include <QApplication>
#include <QByteArray>
#include <QColorDialog>
#include <QDebug>
#include <QDesktopWidget>
#include <QDir>
#include <QDoubleSpinBox>
#include <QFile>
#include <QFileDialog>
#include <QFileInfo>
#include <QGLWidget>
#include <QGridLayout>
#include <QGroupBox>
#include <QHBoxLayout>
#include <QImage>
#include <QLabel>
#include <QLineEdit>
#include <QList>
#include <QMap>
#include <QPoint>
#include <QPushButton>
#include <QRadioButton>
#include <QRect>
#include <QRegExpValidator>
#include <QSlider>
#include <QSpinBox>
#include <QString>
#include <QStringBuilder>
#include <QtGlobal>
#include <QVector>

#include <algorithm>
#include <cmath>
#endif

#endif // PARALLAXMAPPING_PRECOMPILED_H
