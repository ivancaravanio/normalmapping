#ifndef FILEPICKER_H
#define FILEPICKER_H

#include <QString>
#include <QWidget>

QT_BEGIN_NAMESPACE
class QLabel;
class QLineEdit;
class QPushButton;
QT_END_NAMESPACE

QT_USE_NAMESPACE

class FilePathPicker : public QWidget
{
    Q_OBJECT
public:
    explicit FilePathPicker( QWidget* parent = 0 );
    virtual ~FilePathPicker();

    void setInformativeText( const QString& aInformativeText );
    QString informativeText() const;

    void setFileTypesFilter( const QString& aFileTypesFilter );
    QString fileTypesFilter() const;

    void setFilePath( const QString& aFilePath );
    QString filePath() const;

    void retranslateUi();

signals:
    void filePathChanged( const QString& newFilePath );
    void filePathPicked( const QString& newFilePath );
    void valueEdited();

private slots:
    void pickFilePath();

private:
    QLabel*      m_informativeLabel;
    QLineEdit*   m_filePathDisplay;
    QPushButton* m_pickFilePathButton;
    QString      m_fileTypesFilter;
    QString      m_filePath;
};

#endif // FILEPICKER_H
