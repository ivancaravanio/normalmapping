#ifndef TEXTUREUNIT_H
#define TEXTUREUNIT_H

#include "CommonEntities.h"
#include "Texture.h"

#include "ogl.h"

struct TextureUnit
{
public:
    TextureUnit();
    ~TextureUnit();

    void clear();

public:
    CommonEntities::TextureUnitIndex index;
    GLint                            samplerLocation;
    QList< Texture >                 textures;
};

#endif // TEXTUREUNIT_H
