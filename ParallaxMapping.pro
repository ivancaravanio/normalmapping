QT *= core gui opengl

greaterThan( QT_MAJOR_VERSION, 4 ): QT += widgets

CONFIG *= precompile_header

PRECOMPILED_HEADER = ParallaxMapping_precompiled.h

# DEFINES *= USE_GLCOREARB
DEFINES *= USE_GLM
DEFINES *= _CRT_SECURE_NO_WARNINGS
DEFINES *= THOROUGH_LOGGING_ENABLED
# DEFINES *= USE_SINGLE_UNWRAPPED_TEXTURE

TARGET = ParallaxMapping
TEMPLATE = app

SOURCES += \
    ColorPicker.cpp \
    CommonEntities.cpp \
    FilePathPicker.cpp \
    GLWidget.cpp \
    GraphicsData.cpp \
    GraphicsDataEditor.cpp \
    main.cpp \
    MainControl.cpp \
    Mesh3.cpp \
    Texture.cpp \
    TextureUnit.cpp \
    Triangle2.cpp \
    Triangle3.cpp \
    Utilities/GeneralUtilities.cpp \
    Utilities/ImageUtilities.cpp \
    Utilities/MathUtilities.cpp \
    Vec3Editor.cpp

HEADERS += \
    ColorPicker.h \
    CommonEntities.h \
    FilePathPicker.h \
    GLWidget.h \
    GraphicsData.h \
    GraphicsDataEditor.h \
    MainControl.h \
    Mesh3.h \
    ogl.h \
    ParallaxMapping_precompiled.h \
    Texture.h \
    TextureUnit.h \
    Triangle2.h \
    Triangle3.h \
    Utilities/GeneralUtilities.h \
    Utilities/ImageUtilities.h \
    Utilities/MathUtilities.h \
    Vec3Editor.h

RESOURCES += \
    Resources.qrc

include( Build/QmakeHelper/QmakeHelper.pri )

isDebugBuild() {
    BUILD = Debug
} else {
    BUILD = Release
    DEFINES += QT_NO_DEBUG_OUTPUT QT_NO_WARNING_OUTPUT
}

# QMAKE_DIR_SEP         = $$ENV_DIR_SEP
QMAKE_COPY              = $$ENV_COPY
QMAKE_COPY_FILE         = $$ENV_COPY_FILE
QMAKE_COPY_DIR          = $$ENV_COPY_DIR
QMAKE_MOVE              = $$ENV_MOVE
QMAKE_DEL_FILE          = $$ENV_DEL_FILE
QMAKE_DEL_DIR           = $$ENV_DEL_DIR
QMAKE_MKDIR             = $$ENV_MKDIR
QMAKE_CHK_DIR_EXISTS    = $$ENV_CHK_DIR_EXISTS

QMAKE_SYMBOLIC_LINK     = $$ENV_SYMBOLIC_LINK
QMAKE_INSTALL_FILE      = $$ENV_INSTALL_FILE
QMAKE_INSTALL_PROGRAM   = $$ENV_INSTALL_PROGRAM

VER_MAJ = 1
VER_MIN = 0
VER_PAT = 0
VERSION = $$VER_MAJ $$VER_MIN $$VER_PAT
VERSION = $$versionString( VERSION )
DEFINES *= VERSION_STRING=\\\"$$VERSION\\\"

DESTDIR = $$BUILD
OBJECTS_DIR = $${DESTDIR}/obj
MOC_DIR = $${DESTDIR}/moc
RCC_DIR = $${DESTDIR}/rcc
UI_DIR = $${DESTDIR}/ui

QMAKE_CLEAN += $$fixedPath( $${OUT_PWD}/$${BUILD}/obj/* ) \
               $$fixedPath( $${OUT_PWD}/$${BUILD}/moc/* ) \
               $$fixedPath( $${OUT_PWD}/$${BUILD}/rcc/* ) \
               $$fixedPath( $${OUT_PWD}/$${BUILD}/ui/* ) \
               $$fixedPath( $${OUT_PWD}/$${BUILD}/$${TARGET}* )

defineTest(is64BitBuild) {
    return (true)
}

# glew
GLEW_BASE_DIR_PATH = $${PWD}/ThirdParty/glew

INCLUDEPATH += $${GLEW_BASE_DIR_PATH}/include

BITNESS_DIR_NAME =
is64BitBuild() {
    BITNESS_DIR_NAME = x64
} else {
    BITNESS_DIR_NAME = Win32
}

LIBS += -L$${GLEW_BASE_DIR_PATH}/lib/Release/$${BITNESS_DIR_NAME} -lglew32

# glm
GLM_BASE_DIR_PATH = $${PWD}/ThirdParty/glm

INCLUDEPATH += $$GLM_BASE_DIR_PATH

# gltools
include( ThirdParty/GLTools/GLTools.pri )

defineTest(deployLocalData) {
    DIR_PATH = $$ARGS

    # glew
    QMAKE_POST_LINK += $$copyFile( $${GLEW_BASE_DIR_PATH}/bin/Release/$${BITNESS_DIR_NAME}/glew32.dll, $$DIR_PATH ) $$CRLF
    QMAKE_CLEAN     += $$fixedPath( $${OUT_PWD}/$${BUILD}/glew32.dll )

    # textures
    TEXTURE_FILE_PATHS = *.jpg *.png
    TEXTURES_FILES_SOURCE_DIR_PATH      = $${PWD}/Resources/textures/mod
    TEXTURES_FILES_DESTINATION_DIR_PATH = $${DIR_PATH}/textures
    QMAKE_POST_LINK += $$makeDirNotExists( $${DIR_PATH}/textures ) $$CRLF

    for( TEXTURE_FILE_PATH, TEXTURE_FILE_PATHS ) {
        QMAKE_POST_LINK += $$copyFile( $${TEXTURES_FILES_SOURCE_DIR_PATH}/$${TEXTURE_FILE_PATH}, $$TEXTURES_FILES_DESTINATION_DIR_PATH ) $$CRLF
        QMAKE_CLEAN     += $$fixedPath( $${TEXTURES_FILES_DESTINATION_DIR_PATH}/$${TEXTURE_FILE_PATH} )
    }

    QMAKE_CLEAN += $$delDir( $$TEXTURES_FILES_DESTINATION_DIR_PATH )

    export( QMAKE_POST_LINK )
    export( QMAKE_CLEAN )
}

OTHER_FILES += \
    VertexShader.vert \
    FragmentShader.frag \
    ThirdParty/gl/glcorearb.h \
    info.txt

defineTest(shouldDeployModule) {
    return (false)
}

defineReplace(moduleFileExtension) {
    win32 {
        return( exe )
    } else : macx {
        return( app )
    }
}

defineReplace(moduleFilePath) {
    FILE_PATH_WITHOUT_EXTENSION = $$ARGS

    isEmpty( FILE_PATH_WITHOUT_EXTENSION ) {
        return()
    }

    MODULE_FILE_EXTENSION = $$moduleFileExtension()
    isEmpty( MODULE_FILE_EXTENSION ) {
        return( $$FILE_PATH_WITHOUT_EXTENSION )
    }

    return( $${FILE_PATH_WITHOUT_EXTENSION}.$${MODULE_FILE_EXTENSION} )
}

defineTest(deployModule) {
    DEPLOY_DIR_NAME = Deploy
    DEPLOY_DIR_PATH = $${OUT_PWD}/$${BUILD}/$${DEPLOY_DIR_NAME}

    MODULE_FILE_PATH = $$moduleFilePath( $${OUT_PWD}/$${BUILD}/$${TARGET} )

    QMAKE_POST_LINK += $$makeDirNotExists( $$DEPLOY_DIR_PATH ) $$CRLF \
                       $$copyFile( $$MODULE_FILE_PATH, $$DEPLOY_DIR_PATH ) $$CRLF

    DEPLOYED_MODULE_FILE_PATH = $$moduleFilePath( $${DEPLOY_DIR_PATH}/$${TARGET} )
    QMAKE_CLEAN += $$DEPLOYED_MODULE_FILE_PATH

    deployLocalData( $$DEPLOY_DIR_PATH )

    DEPLOY_TOOL_FILE_PATH =

    win32 {
        DEPLOY_TOOL_FILE_PATH = $$[QT_INSTALL_BINS]/windeployqt.exe
    }

    exists( $$DEPLOY_TOOL_FILE_PATH ) {
        QMAKE_POST_LINK += $$DEPLOY_TOOL_FILE_PATH $$DEPLOYED_MODULE_FILE_PATH $$CRLF
        QMAKE_CLEAN     += $$DEPLOYED_MODULE_FILE_PATH \
                           $$delDir( $$DEPLOY_DIR_PATH )
    }

    export( QMAKE_POST_LINK )
    export( QMAKE_CLEAN )
}

deployLocalData( $${OUT_PWD}/$${BUILD} )
DEPLOYED_MODULE_FILE_PATH = $$moduleFilePath( $${OUT_PWD}/$${BUILD}/$${TARGET} )
shouldDeployModule() {
    deployModule( $$DEPLOYED_MODULE_FILE_PATH )
}
