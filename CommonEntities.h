#ifndef COMMONENTITIES_H
#define COMMONENTITIES_H

#include "GraphicsData.h"
#include "ogl.h"

#include <glm/vec2.hpp>
#include <glm/vec3.hpp>

#include <QDebug>
#include <QList>
#include <QString>

class CommonEntities
{
public:
    static const GLint INVALID_UNIFORM_VAR_LOCATION;

public:
    enum VBOIndex
    {
        VBOIndexInvalid                = -1,
        VBOIndexFloorVertex            = 0,
        VBOIndexFloorVertexNormal      = 1,
        VBOIndexFloorVertexTangent     = 2,
        VBOIndexFloorTexCoord          = 3,
        VBOIndexWallsVertex            = 4,
        VBOIndexWallsVertexNormal      = 5,
        VBOIndexWallsVertexTangent     = 6,
        VBOIndexWallsTexCoord          = 7,
        VBOIndexRoofSidesVertex        = 8,
        VBOIndexRoofSidesVertexNormal  = 9,
        VBOIndexRoofSidesVertexTangent = 10,
        VBOIndexRoofSidesTexCoord      = 11,
        VBOIndicesCount                = 12
    };

    enum VAOIndex
    {
        VAOIndexInvalid   = -1,
        VAOIndexFloor     = 0,
        VAOIndexWalls     = 1,
        VAOIndexRoofSides = 2,
        VAOIndicesCount   = 3
    };

    enum TextureIndex
    {
        TextureIndexInvalid            = -1,
        TextureIndexWallsColor         = 0,
        TextureIndexWallsNormalMap     = 1,
        TextureIndexFloorColor         = 2,
        TextureIndexFloorNormalMap     = 3,
        TextureIndexRoofSidesColor     = 4,
        TextureIndexRoofSidesNormalMap = 5,
        TextureIndicesCount            = 6
    };

    enum TextureUnitIndex
    {
        TextureUnitIndexInvalid   = -1,
        TextureUnitIndexColor     = 0,
        TextureUnitIndexNormalMap = 1,

        TextureUnitIndexFirst     = TextureUnitIndexColor,
        TextureUnitIndexLast      = TextureUnitIndexNormalMap,
        TextureUnitsCount         = 2
    };

public:
    static bool isTextureIndexValid( const TextureIndex textureIndex );
    static bool isTextureUnitIndexValid( const TextureUnitIndex textureUnitIndex );

    static QString defaultTextureFileNameFromTextureIndex( const TextureIndex textureIndex );
    static QString defaultTextureFilePathFromTextureIndex( const TextureIndex textureIndex );

    static bool isUniformVarLocationValid( const GLint uniformVarLocation );

    static TextureUnitIndex textureUnitIndexFromTextureIndex( const TextureIndex textureIndex );
    static GLenum textureUnitDescriptorFromTextureUnitIndex( const TextureUnitIndex textureUnitIndex );
    static QString textureUnitSamplerNameFromTextureUnitIndex( const TextureUnitIndex textureUnitIndex );
    static QList< TextureIndex > textureIndicesFromTextureUnitIndex( const TextureUnitIndex textureUnitIndex );
    static QList< TextureIndex > textureIndicesFromVaoIndex( const VAOIndex vaoIndex );

    static QString vecToString( const glm::vec2& v );
    static QString vecToString( const glm::vec3& v );
    static QString vecToString( const glm::vec4& v );

    static const GraphicsData DEFAULT_GRAPHICS_DATA;
};

#ifndef QT_NO_DEBUG_STREAM
QDebug operator<<( QDebug qdebug, const glm::vec2& v );
QDebug operator<<( QDebug qdebug, const glm::vec3& v );
QDebug operator<<( QDebug qdebug, const glm::vec4& v );
#endif

#endif // COMMONENTITIES_H
