#include "GraphicsData.h"

#include "Utilities/MathUtilities.h"

#include <glm/glm.hpp>

#include <QApplication>
#include <QDir>
#include <qmath.h>

using namespace Utilities;

const double GraphicsData::MIN_MATERIAL_SHININESS_LEVEL = 1.0;
const double GraphicsData::MAX_MATERIAL_SHININESS_LEVEL = 128.0;

const double GraphicsData::MIN_LIGHT_INTENSITY_LEVEL = 0.0;

const qreal GraphicsData::DEFAULT_BUMP_SIZE = 1.0;

GraphicsData::GraphicsData(
        const EffectType aEffectType,
        const QString&   aFloorTextureFilePath,
        const QString&   aWallsTextureFilePath,
        const QString&   aRoofSidesTextureFilePath,
        const qreal      aBumpSize,
        const QColor&    aAmbientColor,
        const QColor&    aLightColor,
        const glm::vec3& aLightPosition,
        const glm::vec3& aEyePosition,
        const double&    aMaterialShininessLevel,
        const double&    aLightIntensityLevel )
    : effectType( aEffectType ),
      bumpSize( aBumpSize ),
      ambientColor( aAmbientColor ),
      lightColor( aLightColor ),
      lightPosition( aLightPosition ),
      eyePosition( aEyePosition ),
      materialShininessLevel( aMaterialShininessLevel ),
      lightIntensityLevel( aLightIntensityLevel )
{
    textureFilePaths[ PartTypeIndexFloor     ] = aFloorTextureFilePath;
    textureFilePaths[ PartTypeIndexWalls     ] = aWallsTextureFilePath;
    textureFilePaths[ PartTypeIndexRoofSides ] = aRoofSidesTextureFilePath;
}

GraphicsData::~GraphicsData()
{
}

bool GraphicsData::operator==( const GraphicsData& other ) const
{
    if ( ! ( effectType == other.effectType
             && qFuzzyCompare( bumpSize, other.bumpSize )
             && ambientColor == other.ambientColor
             && lightColor == other.lightColor
             && MathUtilities::epsilonEqual( lightPosition, other.lightPosition )
             && MathUtilities::epsilonEqual( eyePosition, other.eyePosition )
             && qFuzzyCompare( materialShininessLevel, other.materialShininessLevel )
             && qFuzzyCompare( lightIntensityLevel, other.lightIntensityLevel ) ) )
    {
        return false;
    }

    for ( int i = 0; i < PartTypeIndicesCount; ++ i )
    {
        if ( textureFilePaths[ i ] != other.textureFilePaths[ i ] )
        {
            return false;
        }
    }

    return true;
}

bool GraphicsData::operator!=( const GraphicsData& other ) const
{
    return ! ( *this == other );
}

bool GraphicsData::isMaterialShininessLevelValid( const double materialShininessLevel )
{
    return MathUtilities::fuzzyContainedInRegion(
                MIN_MATERIAL_SHININESS_LEVEL,
                materialShininessLevel,
                MAX_MATERIAL_SHININESS_LEVEL );
}

bool GraphicsData::isLightIntensityLevelValid( const double lightIntensityLevel )
{
    return lightIntensityLevel >= MIN_LIGHT_INTENSITY_LEVEL;
}

bool GraphicsData::isEffectTypeValid( const GraphicsData::EffectType effectType )
{
    return effectType == EffectTypePhongIlluminationAveragedSurfaceNormals
            || effectType == EffectTypePhongIlluminationNormalMapping;
}

bool GraphicsData::isPartTypeIndexValid( const GraphicsData::PartTypeIndex partTypeIndex )
{
    return 0 <= partTypeIndex && partTypeIndex < PartTypeIndicesCount;
}

QString GraphicsData::textureDefaultFilePath( const GraphicsData::PartTypeIndex partTypeIndex )
{
    if ( ! GraphicsData::isPartTypeIndexValid( partTypeIndex ) )
    {
        return QString();
    }

    QString textureFileName;
    switch ( partTypeIndex )
    {
        case PartTypeIndexFloor:
            textureFileName = "stone_wall_mosaic.jpg";
            break;
        case PartTypeIndexWalls:
            textureFileName = "brick_wall.jpg";
            break;
        case PartTypeIndexRoofSides:
            textureFileName = "roof.jpg";
            break;
        default:
            break;
    }

    QApplication* const app = qApp;
    return QDir::toNativeSeparators( app == 0
                                     ? QDir::currentPath()
                                     : app->applicationDirPath() ) % QDir::separator()
           % "textures" % QDir::separator()
           % textureFileName;
}
