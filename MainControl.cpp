#include "MainControl.h"

#include "CommonEntities.h"
#include "GLWidget.h"
#include "GraphicsDataEditor.h"

#include <QGridLayout>
#include <QPushButton>
#include <QSplitter>

MainControl::MainControl( QWidget* parent )
    : QWidget( parent ),
      m_graphicsDataEditor( 0 ),
      m_applyGraphicsDataButton( 0 ),
      m_glWidget( 0 )
{
    m_graphicsDataEditor = new GraphicsDataEditor;
    m_graphicsDataEditor->setGraphicsData( CommonEntities::DEFAULT_GRAPHICS_DATA );

    m_applyGraphicsDataButton = new QPushButton;
    connect( m_applyGraphicsDataButton, SIGNAL(clicked()), SLOT(applyGraphicsData()) );

    QGLFormat glFormat;
    glFormat.setVersion( 3, 3 );
    glFormat.setProfile( QGLFormat::CoreProfile );

    m_glWidget = new GLWidget( glFormat );
    connect( m_graphicsDataEditor,
             SIGNAL(textureFilePathEdited(GraphicsData::PartTypeIndex,QString)),
             m_glWidget,
             SLOT(setTextureFilePath(GraphicsData::PartTypeIndex,QString)) );
    connect( m_graphicsDataEditor,
             SIGNAL(bumpSizeEdited(qreal)),
             m_glWidget,
             SLOT(setBumpSize(qreal)) );
    connect( m_graphicsDataEditor,
             SIGNAL(effectTypeEdited(GraphicsData::EffectType)),
             m_glWidget,
             SLOT(setEffectType(GraphicsData::EffectType)) );

    QWidget* const controlsContainer = new QWidget;
    QVBoxLayout* const controlsLayout = new QVBoxLayout( controlsContainer );
    controlsLayout->setMargin( 0 );
    controlsLayout->addWidget( m_graphicsDataEditor );
    controlsLayout->addWidget( m_applyGraphicsDataButton );

    QWidget* const controlsAlignmentHelper = new QWidget;
    QVBoxLayout* const controlsAlignmentHelperLayout = new QVBoxLayout( controlsAlignmentHelper );
    controlsAlignmentHelperLayout->setMargin( 0 );
    controlsAlignmentHelperLayout->addWidget( controlsContainer, 0, Qt::AlignTop );

    QSplitter* const splitter = new QSplitter;
    splitter->addWidget( controlsAlignmentHelper );
    splitter->addWidget( m_glWidget );

    QHBoxLayout* const mainLayout = new QHBoxLayout( this );
    mainLayout->addWidget( splitter );

    this->retranslateUi();
}

MainControl::~MainControl()
{
}

void MainControl::retranslateUi()
{
    m_applyGraphicsDataButton->setText( tr( "Apply" ) );
}

void MainControl::applyGraphicsData()
{
    m_glWidget->setGraphicsData( m_graphicsDataEditor->graphicsData() );
}
