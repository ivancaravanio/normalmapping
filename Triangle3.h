#ifndef TRIANGLE3_H
#define TRIANGLE3_H

#include <glm/vec3.hpp>

#include <QList>

class Triangle3
{
public:
    static const int VERTICES_COUNT;

public:
    explicit Triangle3(
            const glm::vec3& aV0 = glm::vec3(),
            const glm::vec3& aV1 = glm::vec3(),
            const glm::vec3& aV2 = glm::vec3() );
    ~Triangle3();

    void setVertices( const glm::vec3& aV0,
                      const glm::vec3& aV1,
                      const glm::vec3& aV2 );
    const QList< glm::vec3 >& vertices() const;

    void setVertexAtIndex( const int index, const glm::vec3& v );
    glm::vec3 vertexAtIndex( const int index ) const;

    void setV0( const glm::vec3& aV0 );
    glm::vec3 v0() const;

    void setV1( const glm::vec3& aV1 );
    glm::vec3 v1() const;

    void setV2( const glm::vec3& aV2 );
    glm::vec3 v2() const;

    void reverse();

    glm::vec3 normal() const;

    int indexOfVertex( const glm::vec3& v ) const;
    bool isVertex( const glm::vec3& v ) const;
    bool contains( const glm::vec3& v ) const;
    bool contains( const Triangle3& t ) const;

    glm::vec3::value_type angleAtVertex( const glm::vec3& v ) const;

    glm::vec3::value_type area() const;

    bool operator==( const Triangle3& other ) const;
    bool operator!=( const Triangle3& other ) const;

    QString toString() const;

private:
    int cyclicVertexIndex( const int vertexIndex ) const;

    static glm::vec3::value_type sign(
            const glm::vec3& v0,
            const glm::vec3& v1,
            const glm::vec3& v2 );

private:
    QList< glm::vec3 > m_vertices;
};

#ifndef QT_NO_DEBUG_STREAM
QDebug operator<<( QDebug qdebug, const Triangle3& t );
#endif

#endif // TRIANGLE3_H
