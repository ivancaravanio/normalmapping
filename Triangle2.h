#ifndef TRIANGLE2_H
#define TRIANGLE2_H

#include <glm/vec2.hpp>

#include <QList>

class Triangle2
{
public:
    static const int VERTICES_COUNT;

public:
    explicit Triangle2(
            const glm::vec2& aV0 = glm::vec2(),
            const glm::vec2& aV1 = glm::vec2(),
            const glm::vec2& aV2 = glm::vec2() );
    ~Triangle2();

    void setVertices( const glm::vec2& aV0,
                      const glm::vec2& aV1,
                      const glm::vec2& aV2 );
    const QList< glm::vec2 >& vertices() const;

    void setVertexAtIndex( const int index, const glm::vec2& v );
    glm::vec2 vertexAtIndex( const int index ) const;

    void setV0( const glm::vec2& aV0 );
    glm::vec2 v0() const;

    void setV1( const glm::vec2& aV1 );
    glm::vec2 v1() const;

    void setV2( const glm::vec2& aV2 );
    glm::vec2 v2() const;

    void reverse();

    int indexOfVertex( const glm::vec2& v ) const;
    bool isVertex( const glm::vec2& v ) const;
    bool contains( const glm::vec2& v ) const;
    bool contains( const Triangle2& t ) const;

    glm::vec2::value_type angleAtVertex( const glm::vec2& v ) const;

    glm::vec2::value_type area() const;

    bool operator==( const Triangle2& other ) const;
    bool operator!=( const Triangle2& other ) const;

    QString toString() const;

private:
    int cyclicVertexIndex( const int vertexIndex ) const;

    static glm::vec2::value_type sign(
            const glm::vec2& v0,
            const glm::vec2& v1,
            const glm::vec2& v2 );

private:
    QList< glm::vec2 > m_vertices;
};

#ifndef QT_NO_DEBUG_STREAM
QDebug operator<<( QDebug qdebug, const Triangle2& t );
#endif

#endif // TRIANGLE2_H
