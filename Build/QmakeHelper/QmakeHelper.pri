# Needed for function calls. The following article - http://qt-project.org/doc/qt-4.8/qmake-variable-reference.html#pwd, says:
# "Note: Function calls have no effect on the value of PWD. PWD will refer to the path of the calling file."
# Calling file could differ from the current file.
QMAKEHELPER_PRI_PWD = $$PWD

# argument #1 (required) - number
# argument #2 (optional) - field width (default = "0"; if the field width is less or equal to the count of the number's digits, the digit will be printed as it is)
# argument #3 (optional) - fill char (default = "")
defineReplace(numberToString) {
    NUMBER_TO_STRING_RESULT =
    count( ARGS, 1, >= ) : count( ARGS, 3, <= ) {
        NUMBER = $$1

        FIELD_WIDTH = 0
        count( ARGS, 2, >= ) {
            FIELD_WIDTH = $$2
        }

        greaterThan( FIELD_WIDTH, 0 ) : !lessThan( NUMBER, 0 ) {
            # Contains all possible digit counts for 32-bit signed/unsigned integer
            # 32-bit signed/unsigned long integers are not taken into consideration,
            # since lessThan, greaterThan, equals use 32-bit signed/unsigned integer.
            # It is not easy to deduce the target executable/library architecture type -
            # 32-bit or 64-bit, just by examining some of qmake's variables.
            # If the passed number exceeds the maximum unsigned 32-bit integer =>
            # 64-bit architecture is used
            POWERS_OF_TEN = 1 \ # 10 ^ 0 - added since the field width is 1-based but the POWERS_OF_TEN list is 0-based
                            1 \ # 10 ^ 0
                            10 \ # 10 ^ 1
                            100 \ # 10 ^ 2
                            1000 \ # 10 ^ 3
                            10000 \ # 10 ^ 4
                            100000 \ # 10 ^ 5
                            1000000 \ # 10 ^ 6
                            10000000 \ # 10 ^ 7
                            100000000 \ # 10 ^ 8
                            1000000000   # 10 ^ 9
            MAX_UNSIGNED_INT = 4294967295
            greaterThan(NUMBER, $$MAX_UNSIGNED_INT) {
                # the signed/unsigned integer is 64-bit
                POWERS_OF_TEN += 10000000000 \ # 10 ^ 10
                                 100000000000 \ # 10 ^ 11
                                 1000000000000 \ # 10 ^ 12
                                 10000000000000 \ # 10 ^ 13
                                 100000000000000 \ # 10 ^ 14
                                 1000000000000000 \ # 10 ^ 15
                                 10000000000000000 \ # 10 ^ 16
                                 100000000000000000 \ # 10 ^ 17
                                 1000000000000000000 \ # 10 ^ 18
                                 10000000000000000000   # 10 ^ 19
            }

            POWERS_OF_TEN_COUNT = $$size(POWERS_OF_TEN)
            lessThan( FIELD_WIDTH, $$POWERS_OF_TEN_COUNT ) {
                FILL_CHAR =
                count( ARGS, 3 ) {
                    FILL_CHAR = $$3
                }
                TARGET_POWER_OF_TEN = $$member( POWERS_OF_TEN, $$FIELD_WIDTH )
                NUMBER_TO_STRING_RESULT = $$NUMBER
                for( POWER_OF_TEN, POWERS_OF_TEN ) {
                    lessThan( POWER_OF_TEN, $$TARGET_POWER_OF_TEN ) | equals(POWER_OF_TEN, $$TARGET_POWER_OF_TEN ) {
                        greaterThan( POWER_OF_TEN, $$NUMBER ) {
                            DONT_FILL =
                            equals( POWER_OF_TEN, 1 ) {
                                equals( NUMBER, 0 ) {
                                    DONT_FILL = true
                                }
                            }
                            isEmpty( DONT_FILL ) {
                                NUMBER_TO_STRING_RESULT = $${FILL_CHAR}$${NUMBER_TO_STRING_RESULT}
                            }
                        }
                    }
                }
            }
            else {
                NUMBER_TO_STRING_RESULT = $$NUMBER
            }
        }
        else {
            NUMBER_TO_STRING_RESULT = $$NUMBER
        }
    }

    return ( $$NUMBER_TO_STRING_RESULT )
}

# argument #1 (required) - version parts (major|major minor|major minor patch)
# argument #2 (optional) - separator (default = ".")
# argument #3 (optional) - field width (default = "1")
# argument #4 (optional) - fill char (default = "")
defineReplace(versionString) {
    VERSION_STRING_RESULT =
    count( ARGS, 1, >= ) {
        VERSION_PARTS = $$eval( $$1 )

        count( VERSION_PARTS, 1, >= ) {
            SEPARATOR = .
            count( ARGS, 2, >= ) {
                SEPARATOR = $$2
            }

            FIELD_WIDTH = 1
            count( ARGS, 3, >= ) {
                FIELD_WIDTH = $$3
            }

            FILL_CHAR =
            count( ARGS, 4, >= ) {
                FILL_CHAR = $$4
            }

            VERSION_STRING_PARTS =
            for( VERSION_PART, VERSION_PARTS ) {
                VERSION_STRING_PARTS += $$numberToString( $$VERSION_PART, $$FIELD_WIDTH, $$FILL_CHAR )
            }

            VERSION_STRING_RESULT = $$join( VERSION_STRING_PARTS, $$SEPARATOR )
        }
    }

    return ( $$VERSION_STRING_RESULT )
}

# major
# minor
# patch
# fieldWidth
# fillChar - equals "0" here
# separator
defineReplace(pyVersionString) {
    str =
    count(ARGS, 6) {
        major       = $$1
        minor       = $$2
        patch       = $$3
        fieldWidth  = $$4
        # fillChar    = $$5
        separator   = $$6
        str         = $$system($${QMAKEHELPER_PRI_PWD}/QmakeHelper.py -versionString $$major $$minor $$patch $$fieldWidth $$separator)
    }

    return ($$str)
}

defineReplace(incremented) {
    str =
    count(ARGS, 1) {
        str = $$system($${QMAKEHELPER_PRI_PWD}/QmakeHelper.py -increment $$1)
    }

    return ($$str)
}

defineReplace(decremented) {
    str =
    count(ARGS, 1) {
        str = $$system($${QMAKEHELPER_PRI_PWD}/QmakeHelper.py -decrement $$1)
    }

    return ($$str)
}

defineTest(hasUnixShell) {
    win32 : isEmpty(QMAKE_SH) {
        return (false)
    } else: unix | win32 : !isEmpty(QMAKE_SH) {
        return (true)
    }

    return (false)
}

# argument #1 (required): path
defineReplace(whitespacesEscapedPath) {
    WHITESPACES_ESCAPED_PATH = $$1
    WHITESPACES_ESCAPED_PATH = $$quote($$join(WHITESPACES_ESCAPED_PATH, " "))
    hasUnixShell() {
        WHITESPACES_ESCAPED_PATH = $$replace(WHITESPACES_ESCAPED_PATH, "\\ ", " ")
        WHITESPACES_ESCAPED_PATH = $$replace(WHITESPACES_ESCAPED_PATH, " ", "\\ ")
    } else {
        WHITESPACES_ESCAPED_PATH = $$replace(WHITESPACES_ESCAPED_PATH, "\"", "")
        WHITESPACES_ESCAPED_PATH = $$quote(\"$${WHITESPACES_ESCAPED_PATH}\")
    }
    return ( $$WHITESPACES_ESCAPED_PATH )
}

defineTest(isDebugBuild) {
    CONFIG(debug, debug|release) {
        return(true)
    } else {
        return(false)
    }
}

# argument #1 (required): library name
# argument #2 (optional): the formatted version of the library (example: "1.0.0", "1", "")
# argument #3 (optional): if empty the name of the dynamic library will be composed, otherwise the name of the static one
defineReplace(libraryFullBinaryFileName) {
    LIBRARY_FULL_BINARY_FILE_NAME_RESULT =
    count( ARGS, 1, >= ) : count( ARGS, 3, <= )  {
        LIBRARY_VERSION =
        count( ARGS, 2, >= ) {
            LIBRARY_VERSION = $$2
        }

        LIBRARY_VERSION_SUFFIX =
        !isEmpty( LIBRARY_VERSION ) {
            win32 {
                LIBRARY_VERSION_SUFFIX = $$LIBRARY_VERSION
            } else {
                LIBRARY_VERSION_SUFFIX = .$$LIBRARY_VERSION
            }
        }

        NAME_TEMPLATE =
        # %1 - library name
        # %2 - library version
        win32 {
            IS_STATIC = $$3
            isEmpty(IS_STATIC) {
                NAME_TEMPLATE = %1%2.dll
            } else {
                *g++* | *gcc* {
                    NAME_TEMPLATE = lib%1%2.a
                } else {
                    NAME_TEMPLATE = %1%2.lib
                }
            }
        } else: macx {
            NAME_TEMPLATE = $${QMAKE_PREFIX_SHLIB}%1%2.$${QMAKE_EXTENSION_SHLIB}
        } else: unix : !macx {
            NAME_TEMPLATE = $${QMAKE_PREFIX_SHLIB}%1.so%2
        }
        LIBRARY_FULL_BINARY_FILE_NAME_RESULT = $$sprintf( $$NAME_TEMPLATE, $$1, $$LIBRARY_VERSION_SUFFIX )
    }

    return ( $$LIBRARY_FULL_BINARY_FILE_NAME_RESULT )
}

CRLF                    = $$escape_expand(\\n\\t)
WIN_DIR_SEPARATOR       = $$escape_expand(\\)
UNIX_DIR_SEPARATOR      = /

hasUnixShell() {
    ENV_DIR_SEP         = $$UNIX_DIR_SEPARATOR
    ENV_COPY            = cp -f
    ENV_COPY_FILE       = $$ENV_COPY
    ENV_COPY_DIR        = $$ENV_COPY -R
    ENV_MOVE            = mv -f
    ENV_DEL_FILE        = rm -f
    ENV_DEL_DIR         = rmdir
    ENV_MKDIR           = mkdir -p
    ENV_CHK_DIR_EXISTS  = test -d

    ENV_SYMBOLIC_LINK   = ln -f -s
    ENV_INSTALL_FILE    = install -m 644 -p
    ENV_INSTALL_PROGRAM = install -m 755 -p
} else {
    ENV_DIR_SEP         = $$WIN_DIR_SEPARATOR
    ENV_COPY            = copy /y
    ENV_COPY_FILE       = $$ENV_COPY
    ENV_COPY_DIR        = xcopy /s /q /y /i
    ENV_MOVE            = move
    ENV_DEL_FILE        = del /q
    ENV_DEL_DIR         = rmdir /s /q
    ENV_MKDIR           = mkdir
    ENV_CHK_DIR_EXISTS  = IF NOT EXIST

    ENV_SYMBOLIC_LINK   =
    ENV_INSTALL_FILE    = $$ENV_COPY_FILE
    ENV_INSTALL_PROGRAM = $$ENV_COPY_FILE
}

# argument #1 (required, value): a variable containing the path to convert
defineReplace(toNativeSeparators) {
    PATH = $$1
    !isEmpty( PATH ) {
        DIR_SEP_TO_REPLACE =
        hasUnixShell() {
            DIR_SEP_TO_REPLACE = $${WIN_DIR_SEPARATOR}$${WIN_DIR_SEPARATOR}
        } else {
            DIR_SEP_TO_REPLACE = $$UNIX_DIR_SEPARATOR
        }
        PATH = $$replace(PATH, $$DIR_SEP_TO_REPLACE, $$ENV_DIR_SEP)
    }

    return ( $$PATH )
}

#argument #1 (required, value): path
defineReplace(fixedPath) {
    return ( $$whitespacesEscapedPath( $$toNativeSeparators( $$1 ) ) )
}

defineReplace(shellWrapper) {
    SHELL_WRAPPER =
    hasUnixShell() : win32 {
        SHELL_WRAPPER = $$QMAKE_SH -c \"%1\"
    } else {
        SHELL_WRAPPER = %1
    }

    return ( $$SHELL_WRAPPER )
}

#argument #1 (required, value): command and its arguments
defineReplace(formattedCommand) {
    return( $$sprintf( $$shellWrapper(), $$1 ) )
}

# argument #1 (required, value): source file/files path
# argument #2 (required, value): destination file/folder path
defineReplace(copyFile) {
    return ( $$formattedCommand( $$ENV_COPY_FILE $$fixedPath($$1) $$fixedPath($$2) ) )
}

# argument #1 (required, value): source folder path
# argument #2 (required, value): destination folder path
defineReplace(formattedCopyDirCommand) {
    COPY_DIR_FORMAT = %1
    hasUnixShell() {
        COPY_DIR_FORMAT += %2/*
    } else {
        COPY_DIR_FORMAT += %2
    }
    COPY_DIR_FORMAT += %3

    return ( $$sprintf( $$COPY_DIR_FORMAT, $$ENV_COPY_DIR, $$1, $$2 ) )
}

# argument #1 (required, value): source folder path
# argument #2 (required, value): destination folder path
defineReplace(copyDir) {
    return ( $$formattedCommand( $$formattedCopyDirCommand( $$fixedPath($$1), $$fixedPath($$2) ) ) )
}

# argument #1 (required, value): source file path
# argument #2 (required, value): destination file path
defineReplace(copyFileExists) {
    return ( $$formattedCommand( $$sprintf( $$fileFolderExists( $$fixedPath($$1), file, exist ), $$ENV_COPY_FILE $$fixedPath($$1) $$fixedPath($$2) ) ) )
}

# argument #1 (required): file path
defineReplace(delFile) {
    return ( $$formattedCommand( $$ENV_DEL_FILE $$fixedPath($$1) ) )
}

# argument #1 (required, value): source folder path
# argument #2 (required, value): destination folder path
defineReplace(copyDirExists) {
    return ( $$formattedCommand( $$sprintf( $$fileFolderExists( $$fixedPath($$1), directory, exist ), $$formattedCopyDirCommand( $$fixedPath($$1), $$fixedPath($$2) ) ) ) )
}

# argument #1 (required, value): folder path
defineReplace(makeDir) {
    return ( $$formattedCommand( $$ENV_MKDIR $$fixedPath($$1) ) )
}

# argument #1 (required, value): folder path
defineReplace(makeDirNotExists) {
    return ( $$formattedCommand( $$sprintf( $$fileFolderExists( $$fixedPath($$1), directory, notexist ), $$ENV_MKDIR $$fixedPath($$1) ) ) )
}

# argument #1 (required): folder path
defineReplace(delDir) {
    return ( $$formattedCommand( $$ENV_DEL_DIR $$fixedPath($$1) ) )
}

# argument #1 (required): source file path
# argument #2 (required): destination symbolic link file path
defineReplace(symbolicLink) {
    return ( $$formattedCommand( $$ENV_SYMBOLIC_LINK $$fixedPath($$1) $$fixedPath($$2) ) )
}

#argument #1 (required, value): file path
#argument #2 (required, value): "file" or "directory"
#argument #3 (required, value): "exist" or "notexist"
defineReplace(fileFolderExists) {
    FILE_EXISTS_RESULT =
    ARGUMENT_2_POSSIBLE_VALUES = file directory
    ARGUMENT_3_POSSIBLE_VALUES = exist notexist

    count(ARGS, 3) : contains(ARGUMENT_2_POSSIBLE_VALUES, $$2) : contains(ARGUMENT_3_POSSIBLE_VALUES, $$3) {
        FILE_EXISTS_FORMAT =
        EXIST = $$3
        hasUnixShell() {
            FILE_EXISTS_FORMAT = if [
            equals(EXIST, exist) {
                FILE_EXISTS_FORMAT = -e %1 -a

                ENTITY_TYPE = $$2
                equals(ENTITY_TYPE, file) {
                    FILE_EXISTS_FORMAT += -f
                } else: equals(ENTITY_TYPE, directory) {
                    FILE_EXISTS_FORMAT += -d
                }
            } else: equals(EXIST, notexist) {
                FILE_EXISTS_FORMAT = if [ ! -e
            }
            FILE_EXISTS_FORMAT += %1 ]; then %2; fi
        } else {
            equals(EXIST, exist) {
                FILE_EXISTS_FORMAT = IF
            } else: equals(EXIST, notexist) {
                FILE_EXISTS_FORMAT = IF NOT
            }
            FILE_EXISTS_FORMAT += EXIST %1 %2
        }

        FILE_EXISTS_RESULT = $$sprintf($$FILE_EXISTS_FORMAT, $$1)
    }

    return ($$FILE_EXISTS_RESULT)
}

#argument #1 (required, value): string, which whitespaces will be replaced by \\x20
defineReplace(quotedWhitespaceEscapedValue) {
    QUOTED_WHITESPACE_ESCAPED_VALUE = $$1
    !isEmpty( QUOTED_WHITESPACE_ESCAPED_VALUE ) {
        WHITESPACE_CHAR_CODE = $$escape_expand(\\x20)
        QUOTED_WHITESPACE_ESCAPED_VALUE = $$replace( QUOTED_WHITESPACE_ESCAPED_VALUE, " ", $$WHITESPACE_CHAR_CODE )
        QUOTED_WHITESPACE_ESCAPED_VALUE = $$join( QUOTED_WHITESPACE_ESCAPED_VALUE, $$WHITESPACE_CHAR_CODE )
        QUOTED_WHITESPACE_ESCAPED_VALUE = $$quote( $$QUOTED_WHITESPACE_ESCAPED_VALUE )
    }

    return ($$QUOTED_WHITESPACE_ESCAPED_VALUE)
}

#argument #1 (required, variable): array of elements
#argument #2 (required, value): the value that should be prepended
#                               to each element from the array
defineReplace(prependToEachArrayElement) {
    PREPEND_TO_EACH_ARRAY_ELEMENT_RESULT =
    for( ITER, $$1 ) {
        PREPEND_TO_EACH_ARRAY_ELEMENT_RESULT += $${2}$${ITER}
    }

    return ($$PREPEND_TO_EACH_ARRAY_ELEMENT_RESULT)
}

#argument #1 (required, variable): array of elements
#argument #2 (required, value): the value that should be appended
#                               to each element from the array
defineReplace(appendToEachArrayElement) {
    APPEND_TO_EACH_ARRAY_ELEMENT_RESULT =
    for( ITER, $$1 ) {
        APPEND_TO_EACH_ARRAY_ELEMENT_RESULT += $${ITER}$${2}
    }

    return ($$APPEND_TO_EACH_ARRAY_ELEMENT_RESULT)
}

QT_BINARY_TRANSLATION_FILES_FILTER = qt_*.qm
defineReplace(qtBinaryTranslationFilesPaths) {
    qtAndQtHelpTranslationFiles = $$files($$[QT_INSTALL_TRANSLATIONS]/$${QT_BINARY_TRANSLATION_FILES_FILTER})
    qtTranslationFilesOnly =
    ! isEmpty(qtAndQtHelpTranslationFiles) {
        for(translationFile, qtAndQtHelpTranslationFiles) {
            containsHelp = $$find(translationFile, _help_)
            isEmpty( containsHelp ) {
                qtTranslationFilesOnly += $$translationFile
            }
        }
    }
    return ($$qtTranslationFilesOnly)
}

OTHER_FILES += \
    $${PWD}/QmakeHelper.py
