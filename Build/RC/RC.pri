# Define the following variables prior to including this file:
#
# VER_MAJ (required)
# VER_MIN (required)
# VER_PAT (required)
# TARGET (required)
# COMPANY (required)
# ICON_COMPLETE_BASE_FILE_PATH (optional, applications only)
# MAC_APP_AGENT (optional) - http://stackoverflow.com/questions/2226749/gui-less-app-bundle
#
# If the project is an Windows or Mac non-console
# application and the application requires an icon,
# the icon's file should be put in the .pro file's
# directory and be named after the COMPANY:
# COMPANY.(ico|icns)

win32 | macx {
    LEGAL_TRADEMARKS = All Rights Reserved
    COPYRIGHT_SYMBOL =
    win32 {
        COPYRIGHT_SYMBOL = \\xA9
    }
    macx {
        COPYRIGHT_SYMBOL = $$system( printf \'\\302\\251\' )
    }

    LEGAL_COPYRIGHT = Copyright $$COPYRIGHT_SYMBOL 2013
    SHORT_COMPANY_NAME = $$COMPANY
    FULL_COMPANY_NAME =
    contains(DEFINES,LEGRAND) {
        FULL_COMPANY_NAME = $$SHORT_COMPANY_NAME SA
    }
    else {
        FULL_COMPANY_NAME = $$SHORT_COMPANY_NAME SPA
    }

    win32 {
        # 1/3 define variables
        contains(TEMPLATE,app) {
            WINDOWS_RC_FILE_ICON          =
        }

        WINDOWS_RC_FILE_VERSION_MAJOR     =
        WINDOWS_RC_FILE_VERSION_MINOR     =
        WINDOWS_RC_FILE_VERSION_PATCH     =

        WINDOWS_RC_FILE_COMPANY_NAME      =
        WINDOWS_RC_FILE_FILE_DESCRIPTION  =
        WINDOWS_RC_FILE_INTERNAL_NAME     =
        WINDOWS_RC_FILE_LEGAL_COPYRIGHT   =
        WINDOWS_RC_FILE_LEGAL_TRADEMARKS  =
        WINDOWS_RC_FILE_ORIGINAL_FILENAME =
        WINDOWS_RC_FILE_PRODUCT_NAME      =

        # 2/3 assign values to the variables
        WINDOWS_RC_FILE_VERSION_MAJOR       = $$VER_MAJ
        WINDOWS_RC_FILE_VERSION_MINOR       = $$VER_MIN
        WINDOWS_RC_FILE_VERSION_PATCH       = $$VER_PAT
        WINDOWS_RC_FILE_FILE_DESCRIPTION    = $$TARGET

        contains(TEMPLATE,app) {
             WINDOWS_RC_FILE_FILE_DESCRIPTION += application
        }
        else {
            contains(TEMPLATE,lib) {
                WINDOWS_RC_FILE_FILE_DESCRIPTION += library
            }
        }

        WINDOWS_RC_FILE_INTERNAL_NAME       = $$TARGET
        WINDOWS_RC_FILE_LEGAL_TRADEMARKS    = $$LEGAL_TRADEMARKS

        TARGET_EXTENSION =
        contains(TEMPLATE,app) {
             TARGET_EXTENSION = exe
        }
        else {
            contains(TEMPLATE,lib) {
                TARGET_EXTENSION = dll
            }
        }

        WINDOWS_RC_FILE_ORIGINAL_FILENAME   = $${TARGET}.$${TARGET_EXTENSION}
        WINDOWS_RC_FILE_PRODUCT_NAME        = $$TARGET
        WINDOWS_RC_FILE_COMPANY_NAME        = $$FULL_COMPANY_NAME
        WINDOWS_RC_FILE_LEGAL_COPYRIGHT     = $$LEGAL_COPYRIGHT $$FULL_COMPANY_NAME

        contains(TEMPLATE,app) : ! isEmpty(ICON_COMPLETE_BASE_FILE_PATH) {
            ICON_FILE_PATH = $${ICON_COMPLETE_BASE_FILE_PATH}.ico
            ICON_FILE_NAME = $$basename(ICON_FILE_PATH)
            exists( $$ICON_FILE_PATH ) {
                # needed by the rc tool to find the *.ico files
                QMAKE_RC += -I\"$$dirname(ICON_FILE_PATH)\"
                WINDOWS_RC_FILE_ICON = $$ICON_FILE_NAME
            }
        }

        include(../QmakeHelper/QmakeHelper.pri)

        # 3/3 add the variables' values as preprocessor definitions
        contains(TEMPLATE,app) {
            !isEmpty(WINDOWS_RC_FILE_ICON) {
                DEFINES*=WINDOWS_RC_FILE_ICON=$$quotedWhitespaceEscapedValue($$WINDOWS_RC_FILE_ICON)
            }
        }

        DEFINES*=WINDOWS_RC_FILE_VERSION_MAJOR=$$WINDOWS_RC_FILE_VERSION_MAJOR
        DEFINES*=WINDOWS_RC_FILE_VERSION_MINOR=$$WINDOWS_RC_FILE_VERSION_MINOR
        DEFINES*=WINDOWS_RC_FILE_VERSION_PATCH=$$WINDOWS_RC_FILE_VERSION_PATCH

        DEFINES*=WINDOWS_RC_FILE_COMPANY_NAME=$$quotedWhitespaceEscapedValue($$WINDOWS_RC_FILE_COMPANY_NAME)
        DEFINES*=WINDOWS_RC_FILE_FILE_DESCRIPTION=$$quotedWhitespaceEscapedValue($$WINDOWS_RC_FILE_FILE_DESCRIPTION)
        DEFINES*=WINDOWS_RC_FILE_INTERNAL_NAME=$$quotedWhitespaceEscapedValue($$WINDOWS_RC_FILE_INTERNAL_NAME)
        DEFINES*=WINDOWS_RC_FILE_LEGAL_COPYRIGHT=$$quotedWhitespaceEscapedValue($$WINDOWS_RC_FILE_LEGAL_COPYRIGHT)
        DEFINES*=WINDOWS_RC_FILE_LEGAL_TRADEMARKS=$$quotedWhitespaceEscapedValue($$WINDOWS_RC_FILE_LEGAL_TRADEMARKS)
        DEFINES*=WINDOWS_RC_FILE_ORIGINAL_FILENAME=$$quotedWhitespaceEscapedValue($$WINDOWS_RC_FILE_ORIGINAL_FILENAME)
        DEFINES*=WINDOWS_RC_FILE_PRODUCT_NAME=$$quotedWhitespaceEscapedValue($$WINDOWS_RC_FILE_PRODUCT_NAME)

        RC_FILE = $${PWD}/RCFileTemplate.rc
    }
    macx {
        contains(TEMPLATE,app) : !contains(CONFIG,console) {
            !isEmpty(ICON_COMPLETE_BASE_FILE_PATH) {
                ICON_FILE_PATH = $${ICON_COMPLETE_BASE_FILE_PATH}.icns
                ICON_FILE_NAME = $$basename(ICON_FILE_PATH)
                exists( $$ICON_FILE_PATH ) {
                    # needed by the rc tool to find the *.ico files
                    QMAKE_RC += -I\"$$dirname(ICON_FILE_PATH)\"
                    ICON = $$ICON_FILE_PATH
                }
            }

            include(../QmakeHelper/QmakeHelper.pri)

            MAC_PLIST.EXECUTABLE = $$TARGET
            MAC_PLIST.VERSION = $$VERSION
            MAC_PLIST.COPYRIGHT = $$quote($${LEGAL_COPYRIGHT} $${FULL_COMPANY_NAME}. $${LEGAL_TRADEMARKS}.)
            MAC_PLIST.GET_INFO_STRING = $$quote($${VERSION}"," $${LEGAL_COPYRIGHT} $${FULL_COMPANY_NAME}. $${LEGAL_TRADEMARKS}.)
            !isEmpty(ICON) {
                MAC_PLIST.ICON_FILE = $$basename(ICON)
            }

            MAC_PLIST.COMPANY_NAME = $$lower($$SHORT_COMPANY_NAME)
            MAC_PLIST.PACKAGE_TYPE = APPL

            # Each project fills in the properties listed below if needed:
            # MAC_PLIST.TYPES - collection of MAC_PLIST_TYPE instances

            # The MAC_PLIST_TYPE structure has the following properties:
            # MAC_PLIST_TYPE.EXTENSIONS - collection of string instances
            # MAC_PLIST_TYPE.ICON_FILE
            # MAC_PLIST_TYPE.MIME_TYPES - collection of string instances
            # MAC_PLIST_TYPE.NAME
            # MAC_PLIST_TYPE.ROLE
            # MAC_PLIST_TYPE.DEFAULT_FOR_TYPE

            include(../Global.pri)
            PLIST_DIR_PATH = $${OUT_PWD}/$${BUILD}

            PLIST_FILE_PATH = $${PLIST_DIR_PATH}/$${MAC_PLIST.EXECUTABLE}.plist
            PLIST_EDITOR = /usr/libexec/PlistBuddy
            QMAKE_POST_LINK += $${PLIST_EDITOR} $${PLIST_FILE_PATH} -c \"Save\" $$CRLF
            !isEmpty(MAC_PLIST.TYPES) {
                QMAKE_POST_LINK += $${PLIST_EDITOR} $${PLIST_FILE_PATH} -c \"Add :CFBundleDocumentTypes array\" $$CRLF
                MAC_PLIST_TYPES_ITERATOR = 0
                for(MAC_PLIST_TYPE, MAC_PLIST.TYPES) {
                    QMAKE_POST_LINK += $${PLIST_EDITOR} $${PLIST_FILE_PATH} -c \"Add :CFBundleDocumentTypes:$${MAC_PLIST_TYPES_ITERATOR} dict\" $$CRLF
                    QMAKE_POST_LINK += $${PLIST_EDITOR} $${PLIST_FILE_PATH} -c \"Add :CFBundleDocumentTypes:$${MAC_PLIST_TYPES_ITERATOR}:CFBundleTypeExtensions array\" $$CRLF

                    MAC_PLIST_TYPE_EXTENSIONS_ITERATOR = 0
                    for(MAC_PLIST_TYPE_EXTENSION, MAC_PLIST_TYPE.EXTENSIONS) {
                        QMAKE_POST_LINK += $${PLIST_EDITOR} $${PLIST_FILE_PATH} -c \"Add :CFBundleDocumentTypes:$${MAC_PLIST_TYPES_ITERATOR}:CFBundleTypeExtensions:$${MAC_PLIST_TYPE_EXTENSIONS_ITERATOR} string $${MAC_PLIST_TYPE_EXTENSION}\" $$CRLF
                        MAC_PLIST_TYPE_EXTENSIONS_ITERATOR = $$incremented( $$MAC_PLIST_TYPE_EXTENSIONS_ITERATOR )
                    }

                    QMAKE_POST_LINK += $${PLIST_EDITOR} $${PLIST_FILE_PATH} -c \"Add :CFBundleDocumentTypes:$${MAC_PLIST_TYPES_ITERATOR}:CFBundleTypeIconFile string $${MAC_PLIST_TYPE.ICON_FILE}\" $$CRLF
                    QMAKE_POST_LINK += $${PLIST_EDITOR} $${PLIST_FILE_PATH} -c \"Add :CFBundleDocumentTypes:$${MAC_PLIST_TYPES_ITERATOR}:CFBundleTypeMIMETypes array\" $$CRLF

                    MAC_PLIST_TYPE_MIME_TYPES_ITERATOR = 0
                    for(MAC_PLIST_TYPE_MIME_TYPE, MAC_PLIST_TYPE.MIME_TYPES) {
                        QMAKE_POST_LINK += $${PLIST_EDITOR} $${PLIST_FILE_PATH} -c \"Add :CFBundleDocumentTypes:$${MAC_PLIST_TYPES_ITERATOR}:CFBundleTypeMIMETypes:$${MAC_PLIST_TYPE_MIME_TYPES_ITERATOR} string $${MAC_PLIST_TYPE_MIME_TYPE}\" $$CRLF
                        MAC_PLIST_TYPE_MIME_TYPES_ITERATOR = $$incremented( $$MAC_PLIST_TYPE_MIME_TYPES_ITERATOR )
                    }

                    QMAKE_POST_LINK += $${PLIST_EDITOR} $${PLIST_FILE_PATH} -c \"Add :CFBundleDocumentTypes:$${MAC_PLIST_TYPES_ITERATOR}:CFBundleTypeName string $${MAC_PLIST_TYPE.NAME}\" $$CRLF
                    QMAKE_POST_LINK += $${PLIST_EDITOR} $${PLIST_FILE_PATH} -c \"Add :CFBundleDocumentTypes:$${MAC_PLIST_TYPES_ITERATOR}:CFBundleTypeRole string $${MAC_PLIST_TYPE.ROLE}\" $$CRLF
                    QMAKE_POST_LINK += $${PLIST_EDITOR} $${PLIST_FILE_PATH} -c \"Add :CFBundleDocumentTypes:$${MAC_PLIST_TYPES_ITERATOR}:LSHandlerRank string Owner\" $$CRLF
                    QMAKE_POST_LINK += $${PLIST_EDITOR} $${PLIST_FILE_PATH} -c \"Add :CFBundleDocumentTypes:$${MAC_PLIST_TYPES_ITERATOR}:LSIsAppleDefaultForType bool $${MAC_PLIST_TYPE.DEFAULT_FOR_TYPE}\" $$CRLF
                }

                MAC_PLIST_TYPES_ITERATOR = $$incremented($$MAC_PLIST_TYPES_ITERATOR)
            }

            QMAKE_POST_LINK += $${PLIST_EDITOR} $${PLIST_FILE_PATH} -c \"Add :CFBundleExecutable string $${MAC_PLIST.EXECUTABLE}\" $$CRLF
            QMAKE_POST_LINK += $${PLIST_EDITOR} $${PLIST_FILE_PATH} -c \"Add :CFBundleGetInfoString string $${MAC_PLIST.GET_INFO_STRING}\" $$CRLF
            !isEmpty(MAC_PLIST.ICON_FILE) {
                QMAKE_POST_LINK += $${PLIST_EDITOR} $${PLIST_FILE_PATH} -c \"Add :CFBundleIconFile string $${MAC_PLIST.ICON_FILE}\" $$CRLF
            }

            QMAKE_POST_LINK += $${PLIST_EDITOR} $${PLIST_FILE_PATH} -c \"Add :CFBundleIdentifier string com.$${MAC_PLIST.COMPANY_NAME}.$${MAC_PLIST.EXECUTABLE}\" $$CRLF
            QMAKE_POST_LINK += $${PLIST_EDITOR} $${PLIST_FILE_PATH} -c \"Add :CFBundleShortVersionString string $${MAC_PLIST.VERSION}\" $$CRLF
            QMAKE_POST_LINK += $${PLIST_EDITOR} $${PLIST_FILE_PATH} -c \"Add :CFBundleVersion string $${MAC_PLIST.VERSION}\" $$CRLF
            QMAKE_POST_LINK += $${PLIST_EDITOR} $${PLIST_FILE_PATH} -c \"Add :NSHumanReadableCopyright string $${MAC_PLIST.COPYRIGHT}\" $$CRLF
            QMAKE_POST_LINK += $${PLIST_EDITOR} $${PLIST_FILE_PATH} -c \"Add :CFBundlePackageType string $${MAC_PLIST.PACKAGE_TYPE}\" $$CRLF
            QMAKE_POST_LINK += $${PLIST_EDITOR} $${PLIST_FILE_PATH} -c \"Add :CFBundleSignature string\" $$CRLF
            !isEmpty(MAC_APP_AGENT) {
                QMAKE_POST_LINK += $${PLIST_EDITOR} $${PLIST_FILE_PATH} -c \"Add :LSUIElement string 1\" $$CRLF
            }

            # QMAKE_INFO_PLIST variable is taken into consideration by qmake
            # neither at PRE_LINK and therefore nor at POST_LINK build stage
            # but presumably before all these stage, since the file is not
            # copied to the Contents folder of the application's bundle.
            # Copy the file manually afterwards.
            # Do not provide the path to the temporarily created plist file
            # since the ICON and probably other resources, won't get copied
            # in the Resources folder of the application's bundle.
            # QMAKE_INFO_PLIST = $${PLIST_FILE_PATH}

            QMAKE_POST_LINK += $$copyFile($${PLIST_FILE_PATH}, $${OUT_PWD}/$${BUILD}/$${TARGET}.app/Contents/Info.plist) $$CRLF

            QMAKE_CLEAN += $$fixedPath( $${PLIST_FILE_PATH} )
        }
    }
}
