#ifndef MAINCONTROL_H
#define MAINCONTROL_H

#include "GraphicsData.h"

#include <QWidget>

class GLWidget;
class GraphicsDataEditor;

QT_BEGIN_NAMESPACE
class QPushButton;
QT_END_NAMESPACE

QT_USE_NAMESPACE

class MainControl : public QWidget
{
    Q_OBJECT

public:
    explicit MainControl( QWidget* parent = 0 );
    virtual ~MainControl();

    void retranslateUi();

private slots:
    void applyGraphicsData();

private:
    GraphicsDataEditor* m_graphicsDataEditor;
    QPushButton*        m_applyGraphicsDataButton;

    GLWidget*           m_glWidget;
};

#endif // MAINCONTROL_H
