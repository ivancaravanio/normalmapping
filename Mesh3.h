#ifndef MESH3_H
#define MESH3_H

#include "Triangle2.h"
#include "Triangle3.h"

#include <glm/vec3.hpp>
#include <glm/vec4.hpp>

#include <QList>
#include <QPair>

class Mesh3
{
public:
    explicit Mesh3( const QList< QPair< Triangle3, Triangle2 > >& aTriangles = QList< QPair< Triangle3, Triangle2 > >() );
    ~Mesh3();

    void setTriangles( const QList< QPair< Triangle3, Triangle2 > >& aTriangles );
    const QList< QPair< Triangle3, Triangle2 > >& triangles() const;

    QList< QPair< Triangle3, Triangle2 > > trianglesSharingVertex( const glm::vec3& v ) const;
    glm::vec3 normalAtVertex( const glm::vec3& v ) const;
    glm::vec4 tangentAtVertex( const glm::vec3& v ) const;
    glm::vec4 bitangentAtVertex( const glm::vec3& v ) const;

    bool isVertex( const glm::vec3 v ) const;
    bool contains( const Triangle3& t ) const;

    glm::vec3::value_type area() const;

    bool operator==( const Mesh3& other ) const;
    bool operator!=( const Mesh3& other ) const;

private:
    glm::vec4 tangentOrBitangentAtVertex(
            const glm::vec3& v,
            const bool requestTangent,
            const bool shouldCalculateManually ) const;

private:
    QList< QPair< Triangle3, Triangle2 > > m_triangles;
};

#endif // MESH3_H
