#include "FilePathPicker.h"

#include <QDir>
#include <QFileDialog>
#include <QFileInfo>
#include <QHBoxLayout>
#include <QLabel>
#include <QLineEdit>
#include <QPushButton>

FilePathPicker::FilePathPicker( QWidget* parent )
    : QWidget( parent ),
      m_informativeLabel( 0 ),
      m_filePathDisplay( 0 ),
      m_pickFilePathButton( 0 )
{
    m_informativeLabel = new QLabel;
    m_filePathDisplay = new QLineEdit;
    m_filePathDisplay->setReadOnly( true );
    m_pickFilePathButton = new QPushButton;
    connect( m_pickFilePathButton, SIGNAL(clicked()), SLOT(pickFilePath()) );

    QHBoxLayout* const mainLayout = new QHBoxLayout( this );
    mainLayout->addWidget( m_informativeLabel, 0, Qt::AlignLeft );
    mainLayout->addWidget( m_filePathDisplay, 1 );
    mainLayout->addWidget( m_pickFilePathButton, 0, Qt::AlignRight );

    this->retranslateUi();
}

FilePathPicker::~FilePathPicker()
{
}

void FilePathPicker::setInformativeText( const QString& aInformativeText )
{
    m_informativeLabel->setText( aInformativeText );
}

QString FilePathPicker::informativeText() const
{
    return m_informativeLabel->text();
}

void FilePathPicker::setFileTypesFilter( const QString& aFileTypesFilter )
{
    m_fileTypesFilter = aFileTypesFilter;
}

QString FilePathPicker::fileTypesFilter() const
{
    return m_fileTypesFilter;
}

void FilePathPicker::setFilePath( const QString& aFilePath )
{
    if ( aFilePath.isEmpty() )
    {
        return;
    }

    const QFileInfo newFileInfo( aFilePath );
    if ( ! newFileInfo.exists() )
    {
        return;
    }

    const QString newFilePath = QDir::toNativeSeparators( newFileInfo.canonicalFilePath() );
    if ( newFilePath == m_filePath )
    {
        return;
    }

    m_filePath = newFilePath;
    m_filePathDisplay->setText( newFilePath );

    emit filePathChanged( aFilePath );
}

QString FilePathPicker::filePath() const
{
    return m_filePath;
}

void FilePathPicker::pickFilePath()
{
    const QFileInfo fileInfo( m_filePath );

    QString newFilePath = QFileDialog::getOpenFileName( this,
                                                        QString(),
                                                        fileInfo.exists()
                                                        ? fileInfo.canonicalPath()
                                                        : QString(),
                                                        m_fileTypesFilter );
    if ( newFilePath.isEmpty() )
    {
        return;
    }

    const QString currentFilePath = this->filePath();
    this->setFilePath( newFilePath );
    if ( currentFilePath == newFilePath )
    {
        return;
    }

    emit filePathPicked( newFilePath );
}

void FilePathPicker::retranslateUi()
{
    m_pickFilePathButton->setText( tr( "Choose ..." ) );
}
