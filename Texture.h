#ifndef TEXTURE_H
#define TEXTURE_H

#include "CommonEntities.h"

#include "ogl.h"

struct Texture
{
public:
    Texture();
    ~Texture();

    void clear();
    bool isValid() const;

public:
    CommonEntities::TextureIndex index;
    GLuint                       name;
};

#endif // TEXTURE_H
