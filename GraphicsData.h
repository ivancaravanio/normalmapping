#ifndef GRAPHICSDATA_H
#define GRAPHICSDATA_H

#include <glm/vec3.hpp>

#include <QColor>
#include <QString>

struct GraphicsData
{
public:
    enum EffectType
    {
        EffectTypeInvalid                                 = -1,
        EffectTypePhongIlluminationAveragedSurfaceNormals = 0,
        EffectTypePhongIlluminationNormalMapping          = 1
    };

    enum PartTypeIndex
    {
        PartTypeIndexInvalid   = -1,
        PartTypeIndexFloor     = 0,
        PartTypeIndexWalls     = 1,
        PartTypeIndexRoofSides = 2,
        PartTypeIndicesCount   = 3
    };

public:
    static const double MIN_MATERIAL_SHININESS_LEVEL;
    static const double MAX_MATERIAL_SHININESS_LEVEL;

    static const double MIN_LIGHT_INTENSITY_LEVEL;

    static const qreal DEFAULT_BUMP_SIZE;

public:
    explicit GraphicsData(
            const EffectType aEffectType = EffectTypePhongIlluminationAveragedSurfaceNormals,
            const QString&   aFloorTextureFilePath = GraphicsData::textureDefaultFilePath( GraphicsData::PartTypeIndexFloor ),
            const QString&   aWallsTextureFilePath = GraphicsData::textureDefaultFilePath( GraphicsData::PartTypeIndexWalls ),
            const QString&   aRoofSidesTextureFilePath = GraphicsData::textureDefaultFilePath( GraphicsData::PartTypeIndexRoofSides ),
            const qreal      aBumpSize = DEFAULT_BUMP_SIZE,
            const QColor&    aAmbientColor = QColor(),
            const QColor&    aLightColor = QColor(),
            const glm::vec3& aLightPosition = glm::vec3(),
            const glm::vec3& aEyePosition = glm::vec3(),
            const double&    aMaterialShininessLevel = MIN_MATERIAL_SHININESS_LEVEL,
            const double&    aLightIntensityLevel = 1.0 );
    ~GraphicsData();

    bool operator==( const GraphicsData& other ) const;
    bool operator!=( const GraphicsData& other ) const;

    static bool isMaterialShininessLevelValid( const double materialShininessLevel );
    static bool isLightIntensityLevelValid( const double lightIntensityLevel );
    static bool isEffectTypeValid( const EffectType effectType );
    static bool isPartTypeIndexValid( const PartTypeIndex partTypeIndex );
    static QString textureDefaultFilePath( const PartTypeIndex partTypeIndex );

public:
    EffectType effectType;
    QString    textureFilePaths[ PartTypeIndicesCount ];
    qreal      bumpSize;
    QColor     ambientColor;
    QColor     lightColor;
    glm::vec3  lightPosition;
    glm::vec3  eyePosition;
    double     materialShininessLevel;
    double     lightIntensityLevel;
};

#endif // GRAPHICSDATA_H
