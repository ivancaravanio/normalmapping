#ifndef GRAPHICSDATAEDITOR_H
#define GRAPHICSDATAEDITOR_H

#include "GraphicsData.h"

#include <QWidget>

class ColorPicker;
class FilePathPicker;
class Vec3Editor;

QT_BEGIN_NAMESPACE
class QButtonGroup;
class QColorDialog;
class QDoubleSpinBox;
class QGroupBox;
class QLabel;
class QRadioButton;
class QSlider;
QT_END_NAMESPACE

QT_USE_NAMESPACE

class GraphicsDataEditor : public QWidget
{
    Q_OBJECT

public:
    explicit GraphicsDataEditor( QWidget* parent = 0 );
    virtual ~GraphicsDataEditor();

    void retranslateUi();

    void setGraphicsData( const GraphicsData& aGraphicsData );
    GraphicsData graphicsData() const;

signals:
    void textureFilePathEdited( const GraphicsData::PartTypeIndex partTypeIndex, const QString& textureFilePath );

    void bumpSizeChanged( const qreal bumpSize );
    void bumpSizeEdited( const qreal bumpSize );

    void effectTypeChanged( const GraphicsData::EffectType effectType );
    void effectTypeEdited( const GraphicsData::EffectType effectType );

private slots:
    void notifyEffectTypeEdited();
    void onTextureFilePathChosen( const QString& aNewTextureFilePath );
    void onBumpSizeChanged( const int aBumpSize );
    void onBumpSizeEdited();

private:
    void configurePositionEditor( Vec3Editor* const positionEditor );

    void setUiEffectType( const GraphicsData::EffectType aUiEffectType );
    GraphicsData::EffectType uiEffectType() const;
    GraphicsData::EffectType effectTypeFromEffectTypeSelector( QRadioButton* const aEffectTypeSelector ) const;
    QRadioButton* effectTypeSelectorFromEffectType( const GraphicsData::EffectType aEffectType ) const;

private:
    QLabel*         m_ambientColorLabel;
    ColorPicker*    m_ambientColorPicker;

    QLabel*         m_lightColorLabel;
    ColorPicker*    m_lightColorPicker;

    QLabel*         m_lightPositionLabel;
    Vec3Editor*     m_lightPositionEditor;

    QLabel*         m_materialShininessLabel;
    QDoubleSpinBox* m_materialShininessLevelEditor;

    QLabel*         m_lightIntensityLevelLabel;
    QDoubleSpinBox* m_lightIntensityLevelEditor;

    QLabel*         m_eyePositionLabel;
    Vec3Editor*     m_eyePositionEditor;

    QList< FilePathPicker* > m_textureFilePathPickers;

    QLabel*         m_bumpSizeLabel;
    QSlider*        m_bumpSizeEditor;

    QGroupBox*      m_effectTypeSelectionGroupBox;
    QButtonGroup*   m_effectTypeSelectorsGroup;
    QRadioButton*   m_phongAveragedSurfaceNormalsSelector;
    QRadioButton*   m_phongNormalMappingSelector;
};

#endif // GRAPHICSDATAEDITOR_H
