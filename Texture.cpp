#include "Texture.h"

Texture::Texture()
    : index( CommonEntities::TextureIndexInvalid ),
      name( 0 )
{
}

Texture::~Texture()
{
}

void Texture::clear()
{
    index = CommonEntities::TextureIndexInvalid;
    name = 0;
}

bool Texture::isValid() const
{
    return CommonEntities::isTextureIndexValid( index )
           && name != 0;
}
